package com.sjkj.main.module.ui.activity

import android.os.Bundle
import android.transition.Transition
import android.util.SparseArray
import android.widget.ImageView
import com.blankj.utilcode.util.ToastUtils
import com.qmuiteam.qmui.widget.dialog.QMUIBottomSheet
import com.qmuiteam.qmui.widget.dialog.QMUIDialog
import com.qmuiteam.qmui.widget.grouplist.QMUICommonListItemView
import com.qmuiteam.qmui.widget.grouplist.QMUICommonListItemView.ACCESSORY_TYPE_CUSTOM
import com.qmuiteam.qmui.widget.grouplist.QMUIGroupListView
import com.qmuiteam.qmui.widget.grouplist.QMUIGroupListView.SEPARATOR_STYLE_NORMAL
import com.sjkj.lib_common.base.BaseMvpActivity
import com.sjkj.lib_common.common.HttpStore
import com.sjkj.lib_common.extensions.getStringArray
import com.sjkj.lib_common.extensions.injectBaseComponet
import com.sjkj.lib_common.utils.GlideApp
import com.sjkj.lib_common.utils.TransitionUtils
import com.sjkj.main.R
import com.sjkj.main.module.data.UserCenterBean
import com.sjkj.main.module.data.UserSchoolBean
import com.sjkj.main.module.di.DaggerMainComponent
import com.sjkj.main.module.domain.repertory.UserUtils
import com.sjkj.main.module.domain.repertory.UserUtils.getHeadImg
import com.sjkj.main.module.presenter.UserCenterPresenter
import com.sjkj.main.module.presenter.UserCenterView
import kotlinx.android.synthetic.main.activity_user_info.*
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 * @author by dingl on 2018/3/16.
 * @desc StudentUserIoActivity
 */
class StudentUserInfoActivity : BaseMvpActivity<UserCenterPresenter>(), UserCenterView {

    override fun getUserSchoolSuccess(userSchoolBean: UserSchoolBean) {
        QMUIBottomSheet.BottomListSheetBuilder(this, true).apply {
            setTitle(R.string.choose_user_school)
            userSchoolBean.SysSchoolInfoDto.forEachIndexed { index, (Name, SchoolID) ->
                if (userBean.SchoolID == SchoolID) {
                    setCheckedIndex(index)
                }
                addItem(Name, SchoolID.toString())
            }
            setOnSheetItemClickListener { dialog, _, position, _ ->
                dialog.dismiss()
                userInfoViewArray[6].detailText = userSchoolBean.SysSchoolInfoDto[position].Name
            }

        }.build().show()
    }

    override fun getUserSchoolFailed() {
        ToastUtils.showShort(R.string.user_school_failed)
    }

    override fun getUserCenterSuccess(userCenterData: UserCenterBean.Data) {
        setupUserInfoGroup(userCenterData)
        setupContactWayGroup(userCenterData)
        setupContactWayParentGroup(userCenterData)
    }

    override fun getUserCenterFailed() {
        ToastUtils.showShort(R.string.user_center_failed)
    }

    private val userInfoDatas = getStringArray(R.array.user_info)
    private val userContactWayDatas = getStringArray(R.array.contact_way)
    private val userContantWayParentDatas = getStringArray(R.array.contact_way_parent)
    private val userBean by lazy { UserUtils.userBean }
    private val userInfoViewArray = SparseArray<QMUICommonListItemView>()
    private val contactWayViewArray = SparseArray<QMUICommonListItemView>()
    private val contactWayParentViewArray = SparseArray<QMUICommonListItemView>()
    override fun getLayoutId() = R.layout.activity_user_info

    override fun injectComponent() {
        DaggerMainComponent.builder()
                .baseComponent(injectBaseComponet())
                .build()
                .inject(this)
        mPresenter.mView = this
    }

    override fun initView(savedInstanceState: Bundle?) {
        setupTopBar()
        setupTransition()
        checkNotNull(userInfoDatas) { "userInfoDatas is null" }
        checkNotNull(userContactWayDatas) { "userContactWayDatas is null" }
        checkNotNull(userContantWayParentDatas) { "userContantWayParentDatas is null" }
        userInfoGroupListView.separatorStyle = SEPARATOR_STYLE_NORMAL
    }

    private fun setupTransition() {
        window.enterTransition.addListener(object : TransitionUtils.TransitionListenerAdapter() {
            override fun onTransitionEnd(transition: Transition?) {
                super.onTransitionEnd(transition)
                transition?.removeListener(this)
                mPresenter.getUserCenterData()
            }
        })
    }

    private fun setupTopBar() {
        topbar.setTitle(R.string.user_msg)
        topbar.addLeftBackImageButton().onClick { finish() }
    }

    private fun setupContactWayGroup(userCenterData: UserCenterBean.Data) {
        val contactWaySection = QMUIGroupListView.newSection(this)
        contactWaySection.setTitle(getString(R.string.contact_way))
        userContactWayDatas.forEachIndexed { index, data ->
            val itemView = userInfoGroupListView.createItemView(data)
            when (index) {
                0 -> itemView.detailText = userCenterData.QQ
                1 -> itemView.detailText = userCenterData.Email
            }
            contactWaySection.addItemView(itemView, {})
            contactWayViewArray.put(index, itemView)
        }
        contactWaySection.addTo(userInfoGroupListView)
    }

    private fun setupContactWayParentGroup(userCenterData: UserCenterBean.Data) {
        val contactWayParentSection = QMUIGroupListView.newSection(this)
        contactWayParentSection.setTitle(getString(R.string.contact_way_parent))
        userContantWayParentDatas.forEachIndexed { index, data ->
            val itemView = userInfoGroupListView.createItemView(data)
            with(itemView) {
                when (index) {
                    0 -> detailText = userCenterData.ParentPhone
                    1 -> detailText = userCenterData.ParentEmail
                    2 -> detailText = userCenterData.MyInvitationCode
                    3 -> detailText = userCenterData.MyRefererUserName
                    4 -> detailText = "${userCenterData.AuditionLength / 60}分钟"
                }
            }
            contactWayParentSection.addItemView(itemView, {})
            contactWayParentViewArray.put(index, itemView)
        }
        contactWayParentSection.addTo(userInfoGroupListView)
    }

    private fun setupUserInfoGroup(userCenterData: UserCenterBean.Data) {
        val userInfoSection = QMUIGroupListView.newSection(this)
        userInfoDatas.forEachIndexed { index, data ->
            val itemView = userInfoGroupListView.createItemView(data)
            with(itemView) {
                when (index) {
                    0 -> {
                        accessoryType = ACCESSORY_TYPE_CUSTOM
                        val headView = ImageView(this@StudentUserInfoActivity)
                        GlideApp.with(this)
                                .load(HttpStore.Base_Img_Url + getHeadImg())
                                .into(headView)
                        addAccessoryCustomView(headView)
                    }
                    1 -> detailText = userBean.UserName
                    2 -> detailText = userBean.Name
                    3 -> detailText = userCenterData.Sex
                    4 -> detailText = "${userBean.Province}-${userBean.City}-${userBean.District}"
                    5 -> detailText = userBean.Stage
                    6 -> detailText = userCenterData.School
                    7 -> detailText = userCenterData.Birthday
                }
            }
            userInfoSection.addItemView(itemView, {
                when (index) {
                    2 -> {
                        showEditTextDialog(index)
                    }
                    3 -> {
                        QMUIDialog.CheckableDialogBuilder(this)
                                .setTitle(R.string.choose_user_sex)
                                .setCheckedIndex(0)
                                .addItems(arrayOf("男", "女"), { dialog, which -> })
                                .show()
                    }
                    6 -> {
                        mPresenter.getUserSchoolData(userBean.CityID, userBean.DistrictID)
                    }

                    else -> {
                    }
                }
            })
            userInfoViewArray.put(index, itemView)
        }
        userInfoSection.addTo(userInfoGroupListView)
    }

    private fun showEditTextDialog(index: Int) {
        QMUIDialog.EditTextDialogBuilder(this)
                .show()
    }
}