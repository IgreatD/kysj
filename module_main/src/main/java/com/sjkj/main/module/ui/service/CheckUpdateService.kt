package com.sjkj.main.module.ui.service

import android.app.IntentService
import android.content.Intent
import com.blankj.utilcode.util.ToastUtils
import com.sjkj.main.R
import com.sjkj.main.module.data.UpdateBean
import com.sjkj.main.module.presenter.CheckUpdatePresenter
import com.sjkj.main.module.presenter.UpdateView
import com.sjkj.main.module.ui.activity.CheckUpdateActivity
import com.taobao.sophix.SophixManager

/**
 * @author by dingl on 2018/3/15.
 * @desc CheckUpdateService
 */
class CheckUpdateService : IntentService("CheckUpdateService"), UpdateView {

    private val mCheckUpdatePresenter by lazy { CheckUpdatePresenter() }

    private var caption = false

    companion object {
        const val CAPTION = "caption"
        const val CHANNEDID = "update"
    }

    override fun onHandleIntent(intent: Intent?) {
        //查询是否有新补丁
        SophixManager.getInstance().queryAndLoadNewPatch()
        mCheckUpdatePresenter.mView = this
        mCheckUpdatePresenter.update()
        caption = intent?.extras?.getBoolean(CAPTION) ?: false
    }

    override fun update(data: UpdateBean.Data) {
        val intent = Intent(this, CheckUpdateActivity::class.java)
        intent.putExtra(CheckUpdateActivity.UPDATE_MUST, data.UpgradeFlag != 1)
        intent.putExtra(CheckUpdateActivity.UPDATE_PATH, data.AppPath)
        intent.putExtra(CheckUpdateActivity.UPDATE_REMARK, data.Remark)
        intent.putExtra(CheckUpdateActivity.UPDATE_VERSION, data.AppVersion)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    override fun updateFailed() {
        if (caption)
            ToastUtils.showShort(R.string.update_failed)
    }

    override fun noUpdate() {
        if (caption)
            ToastUtils.showShort(R.string.update_no)
    }

    override fun showToast(toast: String) {
        if (caption)
            ToastUtils.showShort(toast)
    }

    override fun showLoading() {

    }

    override fun hideLoading() {
    }

}