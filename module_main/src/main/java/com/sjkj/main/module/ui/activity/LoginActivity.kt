package com.sjkj.main.module.ui.activity

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.alibaba.android.arouter.launcher.ARouter
import com.sjkj.lib_common.base.BaseMvpActivity
import com.sjkj.lib_common.common.UserConstant
import com.sjkj.lib_common.extensions.injectBaseComponet
import com.sjkj.lib_common.utils.PreferenceUtils
import com.sjkj.main.R
import com.sjkj.main.module.di.DaggerMainComponent
import com.sjkj.main.module.presenter.LoginPresenter
import com.sjkj.main.module.presenter.LoginView
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 * @author by dingl on 2018/3/24.
 * @desc LoginActivity
 */
class LoginActivity : BaseMvpActivity<LoginPresenter>(), LoginView {

    private var userName by PreferenceUtils(UserConstant.ACCOUNT_USERNAME, "")

    override fun getLayoutId() = R.layout.activity_login

    override fun injectComponent() {
        DaggerMainComponent
                .builder()
                .baseComponent(injectBaseComponet())
                .build()
                .inject(this)
        mPresenter.mView = this
    }

    override fun initView(savedInstanceState: Bundle?) {
        setqueryData()
        setlistener()
    }

    private fun setlistener() {
        password.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE && isLoginValid()) {
                login.performClick()
                return@OnEditorActionListener true
            }
            false
        })

        username.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                password.requestFocus()
                return@OnEditorActionListener true
            }
            false
        })

        sign.onClick {
            ARouter.getInstance().build("/main/sign").navigation()
        }

        login.onClick {
            if (checkContent()) {
                mPresenter.login(username.text.toString(), password.text.toString())
            }
        }

        forgetPd.onClick {
            ARouter.getInstance().build("/main/forgetPwd").navigation()
        }

        username.addTextChangedListener(loginFieldWatcher)

        password.addTextChangedListener(loginFieldWatcher)
    }

    private fun setqueryData() {
        if (userName.isNotEmpty()) {
            username.setText(userName)
        }
    }

    override fun loginSuccess() {
        ARouter.getInstance().build("/main/main").navigation()
        finish()
    }


    override fun loginFailed(userName: String, password: String) {

    }

    private fun isLoginValid(): Boolean {
        return username.length() >= 11 && password.length() >= 6
    }

    private val loginFieldWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable) {
            login.isEnabled = isLoginValid()
        }
    }

    private fun checkContent(): Boolean {
        username.error = null
        password.error = null
        var cancel = false
        var focusView: View? = null
        val pwdText = password.text.toString()
        val usernameText = username.text.toString()
        if (TextUtils.isEmpty(pwdText)) {
            password.error = getString(R.string.password_not_empty)
            focusView = password
            cancel = true
        } else if (password.text.length < 6) {
            password.error = getString(R.string.password_length_short)
            focusView = password
            cancel = true
        }

        if (TextUtils.isEmpty(usernameText)) {
            username.error = getString(R.string.username_not_empty)
            focusView = username
            cancel = true
        }

        return if (cancel) {
            if (focusView != null) {
                focusView.requestFocus()
            }
            false
        } else {
            true
        }
    }
}