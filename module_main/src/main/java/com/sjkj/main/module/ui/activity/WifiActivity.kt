package com.sjkj.main.module.ui.activity

import android.net.wifi.ScanResult
import android.net.wifi.WifiConfiguration
import android.net.wifi.WifiInfo
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.transition.Slide
import android.transition.Transition
import android.view.Gravity
import com.alibaba.android.arouter.facade.annotation.Route
import com.qmuiteam.qmui.widget.dialog.QMUIDialog
import com.qmuiteam.qmui.widget.dialog.QMUITipDialog
import com.sjkj.lib_common.extensions.dismiss
import com.sjkj.lib_common.utils.TransitionUtils
import com.sjkj.main.R
import com.sjkj.main.module.ui.adapter.WifiAdapter
import com.sjkj.main.module.utils.WifiAdmin
import kotlinx.android.synthetic.main.activity_wifi.*
import me.yokeyword.fragmentation.SupportActivity
import org.jetbrains.anko.act
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.toast

/**
 * @author by dingl on 2018/3/12.
 * @desc WifiActivity
 */
@Route(path = "/main/wifi")
class WifiActivity : SupportActivity(), WifiAdmin.OnWifiScanResultsListener, WifiAdmin.OnWifiRSSIListener, WifiAdmin.OnWifiSupplicantStateChangeListener, WifiAdmin.OnWifiPWDErrorListener, WifiAdmin.OnWifiConnectSuccessListener {

    private val wifiAdmin by lazy { WifiAdmin(this) }

    private val loadingDialog by lazy { QMUITipDialog.Builder(act).setIconType(QMUITipDialog.Builder.ICON_TYPE_LOADING).create() }

    private val wifiAdapter by lazy {
        WifiAdapter {
            when (it.state) {
                WifiAdapter.STATE_SAVE -> wifiAdmin.createSaveConfig(it.wifiConfiguration)
                WifiAdapter.STATE_CONNECTING, WifiAdapter.STATE_CONNECTED, WifiAdapter.STATE_UNSAVE -> {
                    if (it.type != 3) {
                        wifiAdmin.createConfig(it.scanResult.SSID)
                        return@WifiAdapter
                    }
                    val builder = QMUIDialog.EditTextDialogBuilder(act)
                    val editText = builder.editText
                    builder.apply {
                        setTitle(it.scanResult.SSID)
                        addAction("取消", { dialog, _ -> dialog.dismiss() })
                        addAction("连接", { dialog, _ ->
                            if (editText.text.toString().isEmpty()) {
                                toast("请先输入wifi密码")
                                return@addAction
                            }
                            dialog.dismiss()
                            val configuration = wifiAdmin.createWifiInfo(it.scanResult.SSID, editText.text.toString(), 3)
                            wifiAdmin.addNetwork(configuration)
                        })
                    }.show()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wifi)
        setupLayout()
        setTransition()
    }

    private fun setTransition() {
        val slideTransition = Slide(Gravity.TOP)
        window.enterTransition = slideTransition
        slideTransition.addListener(object : TransitionUtils.TransitionListenerAdapter() {
            override fun onTransitionEnd(transition: Transition?) {
                transition?.removeListener(this)
                setListener()
                wifiAdmin.openWifi()
                scanWifi()
            }
        })
    }

    private fun setupLayout() {
        with(refresh) {
            setColorSchemeResources(R.color.colorPrimary)
            onRefresh {
                isRefreshing = true
                scanWifi()
            }
        }

        with(recyclerView) {
            adapter = wifiAdapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }

        topbar.addLeftBackImageButton().onClick { finish() }
        topbar.setTitle(R.string.wifi_title)
    }

    private fun setListener() {
        wifiAdmin.setOnScanResultsListener(this)
        wifiAdmin.setOnRSSIListener(this)
        wifiAdmin.setOnWifiSupplicantStateChangeListener(this)
        wifiAdmin.setOnWifiPWDErrorListener(this)
        wifiAdmin.setOnWifiConnectSuccessListener(this)
    }

    private fun scanWifi() {
        loadingDialog.show()
        wifiAdmin.openWifi()
        wifiAdmin.startScan()
    }

    override fun onWifiSuccess(wifiInfo: WifiInfo?, isLock: Boolean) {
        refresh.dismiss()
        loadingDialog.dismiss()
    }

    override fun onWifiPWDError(configuration: WifiConfiguration?) {
        loadingDialog.dismiss()
        refresh.dismiss()
        toast("连接失败，请重新输入密码")
    }

    override fun OnWifiSupplicantStateChange(scanResult: MutableList<ScanResult>?, configurations: MutableList<WifiConfiguration>?) {
        loadingDialog.dismiss()
        refresh.dismiss()
        wifiAdapter.setData(scanResult, configurations, wifiAdmin.connectInfo)
    }

    override fun onWifiRSSI(scanResult: MutableList<ScanResult>?, configurations: MutableList<WifiConfiguration>?) {
        loadingDialog.dismiss()
        refresh.dismiss()
        wifiAdapter.setData(scanResult, configurations, wifiAdmin.connectInfo)
    }

    override fun onWifiScanResults(scanResult: MutableList<ScanResult>?, configurations: MutableList<WifiConfiguration>?) {
        loadingDialog.dismiss()
        refresh.dismiss()
        wifiAdapter.setData(scanResult, configurations, wifiAdmin.connectInfo)
    }

    override fun onDestroy() {
        super.onDestroy()
        wifiAdmin.closeReceiver()
    }

}