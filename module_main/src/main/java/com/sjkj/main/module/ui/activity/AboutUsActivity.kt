package com.sjkj.main.module.ui.activity

import android.os.Bundle
import com.alibaba.android.arouter.facade.annotation.Route
import com.qmuiteam.qmui.widget.textview.QMUILinkTextView
import com.sjkj.main.R
import kotlinx.android.synthetic.main.activity_about.*
import me.yokeyword.fragmentation.SupportActivity
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 * @author by dingl on 2018/3/14.
 * @desc AboutUsActivity
 */
@Route(path = "/user/about")
class AboutUsActivity : SupportActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
        topbar.setTitle(R.string.my_about)
        topbar.addLeftBackImageButton().onClick { finish() }
        doAsync {
            val about = resources.assets.open("about.txt")
                    .use {
                        it.bufferedReader()
                                .readLines()
                                .joinToString("\n")
                    }
            runOnUiThread {
                aboutContent.text = about
            }
        }

        aboutContent.setOnLinkLongClickListener {
            object : QMUILinkTextView.OnLinkClickListener {
                override fun onWebUrlLinkClick(url: String?) {

                }

                override fun onTelLinkClick(phoneNumber: String) {

                }

                override fun onMailLinkClick(mailAddress: String) {

                }

            }
        }
    }

}