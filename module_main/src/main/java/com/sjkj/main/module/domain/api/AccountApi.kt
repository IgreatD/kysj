package com.sjkj.main.module.domain.api

import com.sjkj.lib_common.data.LoginResult
import com.sjkj.main.module.domain.requestbody.LoginRequestBody
import com.sjkj.main.module.domain.requestbody.ModifyPwdRequestBody
import retrofit2.http.Body
import retrofit2.http.POST
import rx.Observable

/**
 * @author by dingl on 2018/3/9.
 * @desc AccountApi
 */

interface AccountApi {

    @POST("LoginRegService.svc/Login")
    fun login(@Body loginRequestBody: LoginRequestBody): Observable<LoginResult>

    @POST("UserCenterService.svc/ModiPassword")
    fun modifyPwd(@Body modifyPwdRequestBody: ModifyPwdRequestBody): Observable<LoginResult>

}