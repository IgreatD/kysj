package com.sjkj.main.module.presenter

import com.sjkj.lib_common.base.BasePresenter
import com.sjkj.lib_common.base.BaseView
import com.sjkj.lib_common.rxhelper.RxSubscriber
import com.sjkj.lib_common.rxhelper.ioMain
import com.sjkj.main.module.data.UserCenterBean
import com.sjkj.main.module.data.UserSchoolBean
import com.sjkj.main.module.domain.repertory.UserCenterRepertory
import javax.inject.Inject

/**
 * @author by dingl on 2018/3/16.
 * @desc UserCenterPresenter
 */
interface UserCenterView : BaseView {
    fun getUserCenterSuccess(userCenterData: UserCenterBean.Data)
    fun getUserCenterFailed()
    fun getUserSchoolSuccess(userSchoolBean: UserSchoolBean)
    fun getUserSchoolFailed()
}

class UserCenterPresenter @Inject constructor() : BasePresenter<UserCenterView>() {

    @Inject
    lateinit var userCenterRepertory: UserCenterRepertory

    private var userSchoolBean: UserSchoolBean? = null

    fun getUserCenterData() {
        userCenterRepertory.getUserCenterData()
                .compose(ioMain(lifecycleProvider))
                .subscribe(object : RxSubscriber<UserCenterBean>(mView) {
                    override fun _onNext(t: UserCenterBean) {
                        if (t.GetUserInfoInfoDto != null) {
                            mView.getUserCenterSuccess(t.GetUserInfoInfoDto[0])
                        } else
                            mView.getUserCenterFailed()
                    }

                })
    }

    fun getUserSchoolData(cityId: Int, districtID: Int) {
        if (userSchoolBean == null) {
            userCenterRepertory.getUserSchoolData(cityId, districtID)
                    .compose(ioMain(lifecycleProvider))
                    .subscribe(object : RxSubscriber<UserSchoolBean>(mView) {
                        override fun _onNext(t: UserSchoolBean) {
                            userSchoolBean = t
                            mView.getUserSchoolSuccess(t)
                        }

                        override fun _onError(toast: String) {
                            super._onError(toast)
                            mView.getUserSchoolFailed()
                        }

                    })
        } else
            mView.getUserSchoolSuccess(userSchoolBean!!)
    }

}