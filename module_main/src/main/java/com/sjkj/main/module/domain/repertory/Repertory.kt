package com.sjkj.main.module.domain.repertory

import com.blankj.utilcode.util.EncryptUtils
import com.google.gson.Gson
import com.orhanobut.logger.Logger
import com.sjkj.lib_common.common.UserConstant
import com.sjkj.lib_common.data.LoginResult
import com.sjkj.lib_common.data.UserBean
import com.sjkj.lib_common.http.createService
import com.sjkj.lib_common.utils.PreferenceUtils
import com.sjkj.main.module.data.UpdateBean
import com.sjkj.main.module.data.UserCenterBean
import com.sjkj.main.module.data.UserSchoolBean
import com.sjkj.main.module.domain.api.AccountApi
import com.sjkj.main.module.domain.api.UpdateApi
import com.sjkj.main.module.domain.api.UserCenterApi
import com.sjkj.main.module.domain.requestbody.LoginRequestBody
import com.sjkj.main.module.domain.requestbody.ModifyPwdRequestBody
import com.sjkj.main.module.domain.requestbody.UpdateReqestBody
import com.sjkj.main.module.domain.requestbody.UserSchoolRequestBody
import rx.Observable
import javax.inject.Inject

/**
 * @author by dingl on 2018/3/8.
 * @desc Repertory
 */
class UpdateRepertory {

    fun update(): Observable<UpdateBean> {
        return createService(UpdateApi::class.java).updateApp(UpdateReqestBody())
    }

}

class AccountRepertory @Inject constructor() {

    fun login(userName: String, password: String): Observable<LoginResult> {
        val loginRequestBody = LoginRequestBody(
                UserName = userName,
                Password = EncryptUtils.encryptMD5ToString(password))
        Logger.json(Gson().toJson(loginRequestBody))
        return createService(AccountApi::class.java).login(loginRequestBody)
    }

    fun modifyPwd(oldPwd: String, newPwd: String): Observable<LoginResult> {
        return createService(AccountApi::class.java).modifyPwd(
                ModifyPwdRequestBody(
                        EncryptUtils.encryptMD5ToString(oldPwd),
                        EncryptUtils.encryptMD5ToString(newPwd)))
    }

}

class UserCenterRepertory @Inject constructor() {

    fun getUserCenterData(): Observable<UserCenterBean> =
            createService(UserCenterApi::class.java).getUserCenterData()

    fun getUserSchoolData(cityId: Int, districtID: Int): Observable<UserSchoolBean> =
            createService(UserCenterApi::class.java).getUserSchoolData(UserSchoolRequestBody(cityId, districtID))

}

object UserUtils {

    val userBean: UserBean by lazy {
        val userString by PreferenceUtils(UserConstant.USER, "")
        Gson().fromJson(userString, UserBean::class.javaObjectType)
    }

    fun getHeadImg() = userBean.HeadImage

    fun getName() = userBean.Name

}