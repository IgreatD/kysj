package com.sjkj.main.module.data

import com.sjkj.lib_common.base.BaseJsonResult


/**
 * @author by dingl on 2018/3/21.
 * @desc ClassCourseHaveInBean
 */
data class ClassCourseHaveInBean(val MyClassListInfoDto: List<Data>) : BaseJsonResult() {
    data class Data(
            val BeginDate: String,
            val IsColletClass: Int
    )
}