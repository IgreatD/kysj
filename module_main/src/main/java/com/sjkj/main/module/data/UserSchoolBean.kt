package com.sjkj.main.module.data

import com.sjkj.lib_common.base.BaseJsonResult


/**
 * @author by dingl on 2018/3/19.
 * @desc UserSchoolBean
 */
data class UserSchoolBean(val SysSchoolInfoDto: List<Data>) : BaseJsonResult() {
    data class Data(
            val Name: String,
            val SchoolID: Int
    )
}