package com.sjkj.main.module.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo.DetailedState;
import android.net.wifi.ScanResult;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import java.util.List;

public class WifiAdmin {

    private WifiManager mWifiManager;

    private OnWifiStateChangeListener onWifiStateChangeListener;
    private OnWifiSupplicantStateChangeListener onWifiSupplicantStateChangeListener;
    private OnWifiScanResultsListener onWifiScanResultsListener;
    private OnWifiConnectSuccessListener onWifiConnectSuccessListener;
    private OnWifiConnectintListener onWifiConnectintListener;
    private OnWifiIDLEListener onWifiIDLEListener;
    private OnWifiRSSIListener onWifiRSSIListener;
    private OnWifiPWDErrorListener onWifiPWDErrorListener;
    private Context context;// 上下文
    private WifiBroadcast mWifiReceiver;

    // 构造器
    public WifiAdmin(Context context) {
        this.context = context;
        // 取得WifiManager对象
        mWifiManager = (WifiManager) context.getApplicationContext()
                .getSystemService(Context.WIFI_SERVICE);
        // 取得WifiInfo对象
        // 注册wifi状态及接受消息的广播接收器
        registerReceiver(context);
    }

    /**
     * 注册广播
     */
    private void registerReceiver(Context context) {
        // 注册wifi信号监听器
        mWifiReceiver = new WifiBroadcast();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);// wifi信号变动广播
        intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);// wifi扫描到的结果广播
        intentFilter.addAction(WifiManager.RSSI_CHANGED_ACTION);// wifi信号变动广播
        intentFilter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);// 请求状态变动广播
        context.registerReceiver(mWifiReceiver, intentFilter);
    }

    /**
     * 关闭wifi广播(取消注册)
     */
    public void closeReceiver() {
        if (mWifiReceiver != null) {
            context.unregisterReceiver(mWifiReceiver);
        }
    }

    /**
     * 连接一个新的网络
     */
    public boolean connectNewNetwork(List<ScanResult> results, String SSID,
                                     String pwd) {
        int netId = addWifiConfig(results, SSID, pwd);
        // netId不等于-1，则说明添加成功，开始连接
        if (netId != -1) {
            return mWifiManager.enableNetwork(netId, true);
        }
        return false;
    }

    /**
     * 连接一个新的网络
     */
    public boolean connectNewNetwork(ScanResult result, String pwd) {
        int netId = addWifiConfig(result, pwd);
        // netId不等于-1，则说明添加成功，开始连接
        if (netId != -1) {
            return mWifiManager.enableNetwork(netId, true);
        }
        return false;
    }

    /**
     * 添加指定WIFI的配置信息,原列表不存在此SSID
     *
     * @param wifiList 所有列表
     * @param ssid     要连接的wifi名称
     * @param pwd      wifi密码
     * @return
     */
    private int addWifiConfig(List<ScanResult> wifiList, String ssid, String pwd) {
        int wifiId = -1;
        for (int i = 0; i < wifiList.size(); i++) {
            ScanResult wifi = wifiList.get(i);
            if (wifi.SSID.equals(ssid)) {
                WifiConfiguration wifiCong = new WifiConfiguration();
                wifiCong.allowedAuthAlgorithms.clear();
                wifiCong.allowedGroupCiphers.clear();
                wifiCong.allowedKeyManagement.clear();
                wifiCong.allowedPairwiseCiphers.clear();
                wifiCong.allowedProtocols.clear();

                wifiCong.SSID = "\"" + wifi.SSID + "\"";// \"转义字符，代表"
                // 无密码的连接
                if (pwd == null || pwd.equals("")) {
                    wifiCong.allowedKeyManagement
                            .set(WifiConfiguration.KeyMgmt.NONE);
                }
                // 有密码的连接
                else {
                    wifiCong.preSharedKey = "\"" + pwd + "\"";// WPA-PSK密码
                    wifiCong.hiddenSSID = false;
                    wifiCong.status = WifiConfiguration.Status.ENABLED;
                }
                wifiId = mWifiManager.addNetwork(wifiCong);// 将配置好的特定WIFI密码信息添加,添加完成后默认是不激活状态，成功返回ID，否则为-1
                if (wifiId != -1) {
                    return wifiId;
                }
            }
        }
        return wifiId;
    }

    public void createSaveConfig(WifiConfiguration wifiConfiguration) {
        while (mWifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLING) {
            try {
                Thread.currentThread();
                Thread.sleep(100);
            } catch (InterruptedException ie) {
                System.out.println("ie.getMessage():" + ie.getMessage());
            }
        }
        if (wifiConfiguration != null) {
            mWifiManager.enableNetwork(wifiConfiguration.networkId, true);
            mWifiManager.saveConfiguration();
        }
    }

    public void createConfig(String SSID) {
        WifiConfiguration config = new WifiConfiguration();
        config.allowedAuthAlgorithms.clear();
        config.allowedGroupCiphers.clear();
        config.allowedKeyManagement.clear();
        config.allowedPairwiseCiphers.clear();
        config.allowedProtocols.clear();
        config.SSID = "\"" + SSID + "\"";
        config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
        mWifiManager.disableNetwork(mWifiManager.getConnectionInfo()
                .getNetworkId());
        mWifiManager.enableNetwork(mWifiManager.addNetwork(config), true);
    }

    public WifiConfiguration createWifiInfo(String SSID, String Password,
                                            int Type) {
        WifiConfiguration config = new WifiConfiguration();
        config.allowedAuthAlgorithms.clear();
        config.allowedGroupCiphers.clear();
        config.allowedKeyManagement.clear();
        config.allowedPairwiseCiphers.clear();
        config.allowedProtocols.clear();
        config.SSID = "\"" + SSID + "\"";

        WifiConfiguration tempConfig = this.IsExsits(SSID);
        if (tempConfig != null) {
            mWifiManager.removeNetwork(tempConfig.networkId);
        }
        // WIFICIPHER_NOPASS
        if (Type == 1) {
            config.wepKeys[0] = "";
            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            config.wepTxKeyIndex = 0;
        }
        // WIFICIPHER_WEP
        if (Type == 2) {
            config.hiddenSSID = true;
            config.wepKeys[0] = "\"" + Password + "\"";
            config.allowedAuthAlgorithms
                    .set(WifiConfiguration.AuthAlgorithm.SHARED);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
            config.allowedGroupCiphers
                    .set(WifiConfiguration.GroupCipher.WEP104);
            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            config.wepTxKeyIndex = 0;
        }
        // WIFICIPHER_WPA
        if (Type == 3) {
            config.preSharedKey = "\"" + Password + "\"";
            config.hiddenSSID = true;
            config.allowedAuthAlgorithms
                    .set(WifiConfiguration.AuthAlgorithm.OPEN);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
            config.allowedPairwiseCiphers
                    .set(WifiConfiguration.PairwiseCipher.TKIP);
            // config.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            config.allowedPairwiseCiphers
                    .set(WifiConfiguration.PairwiseCipher.CCMP);
            config.status = WifiConfiguration.Status.ENABLED;
        }
        return config;
    }

    public WifiConfiguration IsExsits(String SSID) {
        List<WifiConfiguration> existingConfigs = mWifiManager
                .getConfiguredNetworks();
        for (WifiConfiguration existingConfig : existingConfigs) {
            if (existingConfig.SSID.equals("\"" + SSID + "\"")) {
                return existingConfig;
            }
        }
        return null;
    }

    /**
     * 添加指定WIFI的配置信息,原列表不存在此SSID
     *
     * @param result 连接的wifi对象
     * @param pwd    wifi密码
     * @return
     */
    private int addWifiConfig(ScanResult result, String pwd) {
        int wifiId = -1;
        WifiConfiguration wifiCong = new WifiConfiguration();
        wifiCong.allowedAuthAlgorithms.clear();
        wifiCong.allowedGroupCiphers.clear();
        wifiCong.allowedKeyManagement.clear();
        wifiCong.allowedPairwiseCiphers.clear();
        wifiCong.allowedProtocols.clear();

        wifiCong.SSID = "\"" + result.SSID + "\"";// \"转义字符，代表"
        // 无密码的连接
        if (pwd == null || pwd.equals("")) {
            wifiCong.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
        }
        // 有密码的连接
        else {
            wifiCong.preSharedKey = "\"" + pwd + "\"";// WPA-PSK密码
            wifiCong.hiddenSSID = false;
            wifiCong.status = WifiConfiguration.Status.ENABLED;
        }
        wifiId = mWifiManager.addNetwork(wifiCong);// 将配置好的特定WIFI密码信息添加,添加完成后默认是不激活状态，成功返回ID，否则为-1
        if (wifiId != -1) {
            return wifiId;
        }
        return wifiId;
    }

    public void addNetwork(WifiConfiguration wcg) {
        int wcgID = mWifiManager.addNetwork(wcg);
        mWifiManager.enableNetwork(wcgID, true);
    }

    /**
     * 开始扫描wifi
     */
    public void startScan() {
        if (mWifiManager != null) {
            mWifiManager.startScan();
        }
    }

    private boolean isStartScanMode = true;
    private Long sleepTime = 3000L;// 睡眠时间

    /**
     * 启动扫描模式
     */
    public void startScanMode() {
        if (isStartScanMode) {
            isStartScanMode = false;
            new ScanThread().start();
        }
    }

    /**
     * 设置扫描周期(每隔多少毫秒扫描一次，不设置默认为3秒)
     *
     * @param time 毫秒数
     */
    public void setScanCycle(Long time) {
        sleepTime = time;
    }

    /**
     * 关闭扫描模式
     */
    public void closeScanMode() {
        isStartScanMode = true;
    }

    /**
     * 连接配置好的指定ID的网络
     *
     * @param networkId
     * @return
     */
    public boolean enableNetWork(int networkId) {
        if (mWifiManager != null) {
            boolean network = mWifiManager.enableNetwork(networkId, true);
            mWifiManager.saveConfiguration();
            return network;
        }
        return false;
    }

    /**
     * 获取已连接上的wifi名称
     *
     * @return
     */
    public String getConnectWifiSSID() {
        if (mWifiManager != null) {
            return mWifiManager.getConnectionInfo().getSSID();
        }
        return "";
    }

    /**
     * 获取已连接上的wifi名称
     *
     * @return
     */
    public WifiInfo getConnectInfo() {
        if (mWifiManager != null) {
            return mWifiManager.getConnectionInfo();
        }
        return null;
    }

    public String intToIp(int paramInt) {
        return (paramInt & 0xFF) + "." + (0xFF & paramInt >> 8) + "."
                + (0xFF & paramInt >> 16) + "." + (0xFF & paramInt >> 24);
    }

    /**
     * 断开指定ID的网络
     *
     * @param netId wifi的id
     */
    public void disconnectWifi(int netId) {
        if (mWifiManager != null) {
            mWifiManager.disableNetwork(netId);
            mWifiManager.disconnect();
        }
    }

    /**
     * 取消保存
     */
    public void removeNetWork(WifiConfiguration wifiConfiguration) {
        if (mWifiManager != null && wifiConfiguration != null) {
            // 移除网络
            mWifiManager.removeNetwork(wifiConfiguration.networkId);
            // 重新保存配置
            mWifiManager.saveConfiguration();
            mWifiManager.startScan();// 重新扫描
        }
    }

    /**
     * 取消保存
     */
    public void removeNetWork(int networkId) {
        // 移除网络
        mWifiManager.removeNetwork(networkId);
        // 重新保存配置
        mWifiManager.saveConfiguration();
        mWifiManager.startScan();// 重新扫描
    }

    /**
     * 获取wifi状态，返回int值，默认为已关闭
     *
     * @return
     */
    public int getWifiState() {
        return STATE;
    }

    public void openWifi() {
        if (!mWifiManager.isWifiEnabled())
            mWifiManager.setWifiEnabled(true);
    }

    /**
     * 设置wifi扫描结果触发的监听
     *
     * @param listener
     */
    public void setOnScanResultsListener(OnWifiScanResultsListener listener) {
        onWifiScanResultsListener = listener;
    }

    /**
     * 设置wifi扫描结果触发的监听
     *
     * @param listener
     */
    public void setOnWifiSupplicantStateChangeListener(
            OnWifiSupplicantStateChangeListener listener) {
        onWifiSupplicantStateChangeListener = listener;
    }

    /**
     * 设置wifi密码连接错误的监听器
     *
     * @param listener
     */
    public void setOnWifiPWDErrorListener(OnWifiPWDErrorListener listener) {
        onWifiPWDErrorListener = listener;
    }

    /**
     * 设置wifi信号改变触发的监听
     *
     * @param listener
     */
    public void setOnRSSIListener(OnWifiRSSIListener listener) {
        onWifiRSSIListener = listener;
    }

    /**
     * 设置wifi连接成功
     *
     * @param listener
     */
    public void setOnWifiConnectSuccessListener(
            OnWifiConnectSuccessListener listener) {
        onWifiConnectSuccessListener = listener;
    }

    /**
     * 设置wifi正在连接
     *
     * @param listener
     */
    public void setOnWifiConnectingListener(OnWifiConnectintListener listener) {
        onWifiConnectintListener = listener;
    }

    /**
     * 设置wifi闲置中
     *
     * @param listener
     */
    public void setOnWifiIDLEListener(OnWifiIDLEListener listener) {
        onWifiIDLEListener = listener;
    }

    /**
     * 设置wifi信状态改变触发的监听
     *
     * @param listener
     */
    public void setOnWifiStateChangeListener(OnWifiStateChangeListener listener) {
        onWifiStateChangeListener = listener;
    }

    /**
     * wifi的状态，wifi已断开
     */
    public static final int DISABLED = 01;

    /**
     * wifi的状态，wifi正在关闭
     */
    public static final int DISABLING = 02;

    /**
     * wifi的状态，wifi已打开
     */
    public static final int ENABLE = 03;

    /**
     * wifi的状态，正在打开
     */
    public static final int ENABLING = 04;

    /**
     * wifi的状态，未找到
     */
    public static final int UNKNOWN = 05;

    /**
     * wifi的状态
     */
    private int STATE = 01;

    /**
     * wifi信号接口，实现这个接口，当wifi信号改变时会触发
     */
    public interface OnWifiRSSIListener {

        /**
         * 获取扫描接口
         */
        void onWifiRSSI(List<ScanResult> scanResult,
                        List<WifiConfiguration> configurations);
    }

    /**
     * wifi扫描接口，实现这个接口，当扫描到新的网络会触发
     */
    public interface OnWifiScanResultsListener {

        /**
         * 获取扫描到的wifi列表
         */
        void onWifiScanResults(List<ScanResult> scanResult,
                               List<WifiConfiguration> configurations);
    }

    /**
     * wifi请求改变，实现这个接口，当发生请求时触发
     */
    public interface OnWifiSupplicantStateChangeListener {

        /**
         * wifi请求改变
         */
        void OnWifiSupplicantStateChange(List<ScanResult> scanResult,
                                         List<WifiConfiguration> configurations);
    }

    /**
     * wifi连接密码错误回调监听
     */
    public interface OnWifiPWDErrorListener {

        /**
         * wifi连接密码错误回调
         *
         * @param configuration 当前连接错误的wifi对象
         */
        void onWifiPWDError(WifiConfiguration configuration);
    }

    /**
     * wifi连接成功回调
     */
    public interface OnWifiConnectSuccessListener {

        /**
         * wifi连接密码成功回调
         *
         * @param wifiInfo 当前连接成功的wifi对象
         * @param isLock   是否有密码
         */
        void onWifiSuccess(WifiInfo wifiInfo, boolean isLock);
    }

    /**
     * wifi连接成功回调
     */
    public interface OnWifiConnectintListener {

        /**
         * wifi连接中回调
         *
         * @param wifiInfo 当前连接的wifi对象
         * @param isLock   是否有密码
         */
        void onWifiConnecting(WifiInfo wifiInfo, boolean isLock);
    }

    /**
     * wifi闲置回调
     */
    public interface OnWifiIDLEListener {

        /**
         * wifi闲置回调
         */
        void onWifiIDLE();
    }

    /**
     * wifi状态改变，实现这个接口，当wifi的状态改变时会触发
     */
    public interface OnWifiStateChangeListener {
        /**
         * wifi状态改变
         */
        void onWifiStateChange(int state);

    }

    /**
     * 根据SSid找出已配置的消息
     *
     * @param SSID
     * @return
     */
    public WifiConfiguration getWifiConfiguration(String SSID) {
        WifiConfiguration wifiConfiguration = null;
        if (mWifiManager != null) {
            for (WifiConfiguration wcg : mWifiManager.getConfiguredNetworks()) {
                if (wcg.SSID.equals(SSID)) {
                    wifiConfiguration = wcg;
                    break;
                }
            }
        }
        return wifiConfiguration;
    }

    /**
     * 根据SSid找出已扫描的消息
     *
     * @param SSID
     * @return
     */
    private ScanResult getScanResult(String SSID) {
        ScanResult result = null;
        if (mWifiManager != null) {
            for (ScanResult s : mWifiManager.getScanResults()) {
                if (s.SSID.equals(SSID.replace("\"", ""))) {
                    result = s;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * 获取wifi加密类型转换成String显示
     *
     * @param capability 加密字符串
     * @param context    上下文，用来进行文字国际化
     * @return
     */
    public static String getCapability(String capability, Context context) {
        String capabilityInfo = "";
        if (capability.contains("WPA2")) {
            if (capability.contains("WPA")) {
                capabilityInfo = "通过WPA/WPA2进行保护";
            } else if (capability.contains("WPS")) {
                capabilityInfo = "通过WPA2进行保护(可使用WPS)";
            } else {
                capabilityInfo = "通过WPA2进行保护";
            }
        } else if (capability.contains("WPS")) {
            capabilityInfo = "可使用WPS";
        }

        return capabilityInfo;
    }

    /**
     * 获取wifi加密类型
     */
    public static int getCapabilityType(String capability) {
        int type = 0;
        if (capability.contains("WPA2")) {
            if (capability.contains("WPA")) {
                type = 3;
            } else if (capability.contains("WPS")) {
                type = 3;
            } else {
                type = 3;
            }
        } else if (capability.contains("WPS")) {
            type = 1;
        }

        return type;
    }

    /**
     * 扫描线程
     */
    class ScanThread extends Thread {
        @Override
        public void run() {
            super.run();
            while (!isStartScanMode) {
                try {
                    // 每隔3秒钟扫描一次，可以设置
                    Thread.sleep(sleepTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (!isStartScanMode) {
                    if (mWifiManager != null) {
                        // 如果是为连接状态，则开始扫描
                        if (getWifiState() == ENABLE) {
                            startScan();// 开始扫描
                        }
                    }
                }
            }
        }
    }

    /**
     * 接受wifi状态的广播
     */
    public class WifiBroadcast extends BroadcastReceiver {

        @SuppressWarnings("static-access")
        @Override
        public void onReceive(Context context, Intent intent) {
            // wifi状态改变
            if (WifiManager.WIFI_STATE_CHANGED_ACTION
                    .equals(intent.getAction())) {
                switch (mWifiManager.getWifiState()) {
                    case WifiManager.WIFI_STATE_DISABLED:
                        // 赋值状态为已关闭
                        STATE = DISABLED;
                        if (onWifiStateChangeListener != null) {
                            onWifiStateChangeListener.onWifiStateChange(DISABLED);
                        }
                        break;
                    case WifiManager.WIFI_STATE_DISABLING:
                        // 赋值状态为正在关闭
                        STATE = DISABLING;
                        if (onWifiStateChangeListener != null) {
                            onWifiStateChangeListener.onWifiStateChange(DISABLING);
                        }
                        break;
                    case WifiManager.WIFI_STATE_ENABLING:
                        // 赋值状态为正在打开
                        STATE = ENABLING;
                        if (onWifiStateChangeListener != null) {
                            // wifi正在打开时，开始扫描
                            mWifiManager.startScan();
                            onWifiStateChangeListener.onWifiStateChange(ENABLING);
                        }
                        break;
                    case WifiManager.WIFI_STATE_ENABLED:
                        // 赋值状态为已打开
                        STATE = ENABLE;
                        if (onWifiStateChangeListener != null) {
                            // wifi已打开时，开始扫描
                            mWifiManager.startScan();
                            onWifiStateChangeListener.onWifiStateChange(ENABLE);
                        }
                        break;
                    default:
                        // 赋值状态为未找到
                        STATE = UNKNOWN;
                        if (onWifiStateChangeListener != null) {
                            onWifiStateChangeListener.onWifiStateChange(UNKNOWN);
                        }
                        break;
                }
            }
            // wifi扫描结果
            if (WifiManager.SCAN_RESULTS_AVAILABLE_ACTION.equals(intent
                    .getAction())) {
                if (onWifiScanResultsListener != null) {
                    onWifiScanResultsListener.onWifiScanResults(
                            mWifiManager.getScanResults(),
                            mWifiManager.getConfiguredNetworks());
                }
            }
            // wifi信号改变
            if (WifiManager.RSSI_CHANGED_ACTION.equals(intent.getAction())) {
                if (onWifiRSSIListener != null) {
                    onWifiRSSIListener.onWifiRSSI(
                            mWifiManager.getScanResults(),
                            mWifiManager.getConfiguredNetworks());
                }
            }
            // wifi请求状态改变
            if (WifiManager.SUPPLICANT_STATE_CHANGED_ACTION.equals(intent
                    .getAction())) {
                SupplicantState supplicantState = intent
                        .getParcelableExtra(WifiManager.EXTRA_NEW_STATE);
                if (onWifiIDLEListener != null) {
                    // wifi闲置
                    if (mWifiManager.getConnectionInfo()
                            .getDetailedStateOf(supplicantState)
                            .equals(DetailedState.IDLE)) {
                        onWifiIDLEListener.onWifiIDLE();
                    }
                }
                if (onWifiConnectintListener != null) {
                    // wifi正在连接中
                    if (mWifiManager.getConnectionInfo()
                            .getDetailedStateOf(supplicantState)
                            .equals(DetailedState.CONNECTING)) {
                        ScanResult result = getScanResult(mWifiManager
                                .getConnectionInfo().getSSID());
                        // isLock是否需要密码
                        boolean isLock = true;
                        if (result != null) {
                            isLock = getCapabilityType(result.capabilities) != 0 && getCapabilityType(result.capabilities) != 1;
                        }
                        onWifiConnectintListener.onWifiConnecting(
                                mWifiManager.getConnectionInfo(), isLock);
                    }
                }
                if (onWifiConnectSuccessListener != null) {
                    if (mWifiManager.getConnectionInfo()
                            .getDetailedStateOf(supplicantState)
                            .equals(DetailedState.OBTAINING_IPADDR)) {
                        // wifi连接成功
                        ScanResult result = getScanResult(mWifiManager
                                .getConnectionInfo().getSSID());
                        // isLock是否需要密码
                        boolean isLock = true;
                        if (result != null) {
                            isLock = getCapabilityType(result.capabilities) != 0 && getCapabilityType(result.capabilities) != 1;
                        }
                        onWifiConnectSuccessListener.onWifiSuccess(
                                mWifiManager.getConnectionInfo(), isLock);
                    }
                }
                if (onWifiPWDErrorListener != null) {
                    int linkWifiResult = intent.getIntExtra(
                            WifiManager.EXTRA_SUPPLICANT_ERROR, 123);
                    if (linkWifiResult == WifiManager.ERROR_AUTHENTICATING) {
                        WifiConfiguration wcg = getWifiConfiguration(mWifiManager
                                .getConnectionInfo().getSSID());
                        // 移除当前网络
                        removeNetWork(wcg);
                        // wifi密码错误时触发
                        onWifiPWDErrorListener.onWifiPWDError(wcg);
                    }
                }
                if (onWifiSupplicantStateChangeListener != null) {
                    onWifiSupplicantStateChangeListener
                            .OnWifiSupplicantStateChange(
                                    mWifiManager.getScanResults(),
                                    mWifiManager.getConfiguredNetworks());
                }
            }
        }
    }

}
