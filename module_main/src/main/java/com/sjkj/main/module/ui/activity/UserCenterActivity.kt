package com.sjkj.main.module.ui.activity

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import com.alibaba.android.arouter.facade.annotation.Route
import com.alibaba.android.arouter.launcher.ARouter
import com.sjkj.lib_common.base.BaseRxActivity
import com.sjkj.lib_common.common.HttpStore
import com.sjkj.lib_common.extensions.transitionTo
import com.sjkj.lib_common.utils.GlideApp
import com.sjkj.main.R
import com.sjkj.main.module.data.MainTabBean
import com.sjkj.main.module.domain.repertory.UserUtils.userBean
import com.sjkj.main.module.ui.adapter.UserCenterAdapter
import com.sjkj.main.module.ui.service.CheckUpdateService
import kotlinx.android.synthetic.main.activity_student_center.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.startService

/**
 * @author by dingl on 2018/3/14.
 * @desc StudentCenterActivity
 */
@Route(path = "/user/user")
class UserCenterActivity : BaseRxActivity() {

    private val centerTabDatas = ArrayList<MainTabBean>()

    override fun getLayoutId(): Int = R.layout.activity_student_center

    override fun initView(savedInstanceState: Bundle?) {
        setupTopBar()
        setupHead()
        setupMainTabDatas()
        checkNotNull(centerTabDatas) { "the centerTabDatas is null" }
        setupListItem()
    }

    private fun setupTopBar() {
        topbar.setTitle(R.string.app_center)
        topbar.addLeftBackImageButton().onClick { finish() }
    }

    private fun setupMainTabDatas() {
        val centerNames = resources.obtainTypedArray(R.array.center_name)
        val centerIcons = resources.obtainTypedArray(R.array.center_icon)
        try {
            if (centerIcons.length() == centerNames.length()) {
                (0 until centerIcons.length()).mapTo(centerTabDatas) { MainTabBean(it, centerNames.getString(it), centerIcons.getDrawable(it)) }
            } else
                throw IllegalArgumentException("The number of names and pictures doesn't right")
        } finally {
            centerNames.recycle()
            centerIcons.recycle()
        }
    }

    private fun setupListItem() {
        with(centerRecyclerView) {
            adapter = UserCenterAdapter(centerTabDatas) {
                when (it) {
                    0 -> {
                    }
                    1 -> ARouter.getInstance().build("/main/modify_pwd").navigation()
                    2 -> {
                    }
                    3 -> {
                    }
                    4 -> {
                    }
                    5 -> {
                    }
                    6 -> {
                    }
                    7 -> ARouter.getInstance().build("/user/about").navigation()
                    8 -> {
                    }
                    9 -> startService<CheckUpdateService>(Pair(CheckUpdateService.CAPTION, true))
                    else -> {
                    }
                }
            }
            layoutManager = GridLayoutManager(context, 4)
        }
    }

    private fun setupHead() {
        GlideApp.with(this)
                .load(HttpStore.Base_Img_Url + userBean.HeadImage)
                .into(centerHead)
        centerName.text = userBean.Name
        var school = userBean.School
        if (school.isEmpty()) {
            school = "暂无学校"
        }
        var stage = userBean.Stage
        if (stage.isEmpty()) {
            stage = "还未设置学段"
        }
        centerSchool.text = String.format(getString(R.string.center_school), school, stage)
        centerHeadLayout.onClick {
            transitionTo<StudentUserInfoActivity>()
        }
    }
}