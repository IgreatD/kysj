package com.sjkj.main.module.domain.requestbody

import com.blankj.utilcode.util.EncryptUtils
import com.sjkj.main.module.domain.repertory.UserUtils

/**
 * @author by dingl on 2018/3/9.
 * @desc ReqestBody
 */
private val userBean by lazy { UserUtils.userBean }

data class LoginRequestBody(
        val UserName: String = UserUtils.userBean.UserName,
        val Password: String = EncryptUtils.encryptMD5ToString(UserUtils.userBean.PassWord),
        val Flat: String = "",
        val FlatSerial: String = "",
        val FlatModel: String = "",
        val TimeSpan: String = (System.currentTimeMillis() / 1000).toString()
)

data class ModifyPwdRequestBody(
        val OldPassword: String,
        val NewPassword: String
)

data class UpdateReqestBody(val APPType: Int = 1, val DeviceType: Int = 1)

data class UserSchoolRequestBody(
        val CityID: Int,
        val DistrictID: Int
)