package com.sjkj.main.module.ui.adapter

import com.blankj.utilcode.util.AppUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.sjkj.main.R
import com.sjkj.main.module.data.MainTabBean
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 * @author by dingl on 2018/3/8.
 * @desc StudentCenterAdapter
 */
class UserCenterAdapter(datas: List<MainTabBean>, private val itemClick: (Int) -> Unit)
    : BaseQuickAdapter<MainTabBean, BaseViewHolder>(R.layout.adapter_student_center, datas) {
    override fun convert(helper: BaseViewHolder, item: MainTabBean?) {
        item ?: return
        helper.setImageDrawable(R.id.mainTabView, item.icon).itemView.onClick { itemClick(item.id) }
        helper.setText(R.id.mainTabName,
                if (helper.adapterPosition == itemCount - 1)
                    String.format(item.name, AppUtils.getAppVersionName())
                else
                    item.name)
    }
}