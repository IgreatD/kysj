package com.sjkj.main.module.ui.activity

import android.content.Intent
import android.os.Bundle
import com.sjkj.lib_common.base.BaseMvpActivity
import com.sjkj.lib_common.common.Constant
import com.sjkj.lib_common.common.UserConstant
import com.sjkj.lib_common.extensions.injectBaseComponet
import com.sjkj.lib_common.utils.PreferenceUtils
import com.sjkj.main.R
import com.sjkj.main.module.di.DaggerMainComponent
import com.sjkj.main.module.presenter.LoginPresenter
import com.sjkj.main.module.presenter.LoginView
import org.jetbrains.anko.startActivity

/**
 * @author by dingl on 2018/3/9.
 * @desc SplashActivity
 */
class SplashActivity : BaseMvpActivity<LoginPresenter>(), LoginView {

    private var isFirst by PreferenceUtils(Constant.WELCOME_FIRST, true)

    override fun loginSuccess() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun loginFailed(userName: String, password: String) {
        val intent = Intent(this, LoginActivity::class.java)
        intent.putExtra(UserConstant.ACCOUNT_USERNAME, userName)
        startActivity(intent)
        finish()
    }

    override fun injectComponent() {
        DaggerMainComponent.builder()
                .baseComponent(injectBaseComponet())
                .build()
                .inject(this)
        mPresenter.mView = this
    }

    override fun initView(savedInstanceState: Bundle?) {

        if (isFirst.not()) {
            val userName by PreferenceUtils(UserConstant.ACCOUNT_USERNAME, "")
            val passWord by PreferenceUtils(UserConstant.ACCOUNT_PASSWORD, "")
            if (userName.isNotEmpty() && passWord.isNotEmpty()) {
                mPresenter.login(userName, passWord)
            } else
                loginFailed(userName, passWord)
        } else {
            startActivity<WelcomeActivity>()
            finish()
        }


    }

    override fun getLayoutId(): Int = R.layout.activity_splash

}