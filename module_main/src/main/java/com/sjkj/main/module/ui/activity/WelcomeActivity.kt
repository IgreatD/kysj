package com.sjkj.main.module.ui.activity

import android.content.res.TypedArray
import android.os.Bundle
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.*
import android.widget.ImageView
import com.sjkj.lib_common.common.Constant
import com.sjkj.lib_common.utils.PreferenceUtils
import com.sjkj.main.R
import kotlinx.android.synthetic.main.activity_welcome.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.startActivity


/**
 * 欢迎界面
 *
 * @author by dingl on 2018/3/8.
 */
class WelcomeActivity : AppCompatActivity() {

    private lateinit var welcomeIds: TypedArray

    private var isFirst by PreferenceUtils(Constant.WELCOME_FIRST, true)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_welcome)
        welcomeIds = resources.obtainTypedArray(R.array.welcom_drawable_id)
        setupPager()
        setListener()
    }

    private fun setListener() {
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                if (position == welcomeIds.length() - 1)
                    viewPager.getChildAt(position)
                            .onClick {
                                isFirst = false
                                startActivity<LoginActivity>()
                                finish()
                            }
            }
        })
    }

    private fun setupPager() {
        viewPager.adapter = object : PagerAdapter() {
            override fun getCount(): Int {
                return welcomeIds.length()
            }

            override fun isViewFromObject(view: View, `object`: Any): Boolean {
                return view === `object`
            }

            override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
                container.removeView(`object` as View)
            }

            override fun instantiateItem(container: ViewGroup, position: Int): Any {
                val view = LayoutInflater.from(container.context).inflate(R.layout.page_welcome, container, false)
                val iv = view.findViewById<ImageView>(R.id.pageView)
                iv.setImageDrawable(welcomeIds.getDrawable(position))
                container.addView(view)
                return view
            }
        }

        viewPager.pageMargin = resources.getDimensionPixelSize(R.dimen.padding_normal)

        indicator.setViewPager(viewPager)


    }
}
