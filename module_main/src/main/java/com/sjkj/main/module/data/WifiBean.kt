package com.sjkj.main.module.data

import android.net.wifi.ScanResult
import android.net.wifi.WifiConfiguration

/**
 * @author by dingl on 2018/3/12.
 * @desc WifiBean
 */
data class WifiBean(
        val type: Int,
        val state: Int,
        val scanResult: ScanResult

) {
    var wifiConfiguration: WifiConfiguration? = null
}