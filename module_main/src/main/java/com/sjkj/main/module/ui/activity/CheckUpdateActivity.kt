package com.sjkj.main.module.ui.activity

import android.Manifest
import android.os.Bundle
import com.alibaba.android.arouter.facade.annotation.Route
import com.blankj.utilcode.constant.PermissionConstants
import com.blankj.utilcode.util.AppUtils
import com.blankj.utilcode.util.PermissionUtils
import com.liulishuo.filedownloader.BaseDownloadTask
import com.liulishuo.filedownloader.FileDownloadListener
import com.liulishuo.filedownloader.FileDownloader
import com.qmuiteam.qmui.widget.dialog.QMUIDialog
import com.sjkj.lib_common.base.BaseRxActivity
import com.sjkj.lib_common.common.AppConstant
import com.sjkj.lib_common.common.Constant
import com.sjkj.lib_common.common.HttpStore
import com.sjkj.lib_common.extensions.setVisible
import com.sjkj.main.R
import com.sjkj.main.module.ui.service.UpdateService
import kotlinx.android.synthetic.main.activity_update.*
import org.jetbrains.anko.act
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.startService
import org.jetbrains.anko.toast

/**
 * @author by dingl on 2018/3/15.
 * @desc CheckUpdateActivity
 */
@Route(path = "/user/update")
class CheckUpdateActivity : BaseRxActivity() {

    companion object {
        const val UPDATE_MUST = "update_must"
        const val UPDATE_PATH = "update_path"
        const val UPDATE_REMARK = "update_remark"
        const val UPDATE_VERSION = "update_version"
    }

    private var updateMust = false
    private var updatePath = HttpStore.Base_Img_Url

    override fun getLayoutId(): Int = R.layout.activity_update

    override fun initView(savedInstanceState: Bundle?) {
        setupData()
        setupListener()
    }

    private fun setupData() {
        with(intent.extras) {
            updateMust = getBoolean(UPDATE_MUST)
            updatePath += getString(UPDATE_PATH)
            updateContent.text = getString(UPDATE_REMARK)
            updateVersion.text = String.format(getString(R.string.update_version), getString(UPDATE_VERSION))
        }
        updatePb.setTextColor(R.color.colorPrimary)
        updatePb.setQMUIProgressBarTextGenerator { _, value, maxValue ->
            "${100 * value / maxValue}%"
        }
    }

    private fun setupListener() {
        updateAfter.onClick {
            if (updateMust) {
                AppUtils.exitApp()
            } else {
                finish()
            }
        }
        updateNow.onClick {
            if (haveStroagePermission()) {
                updateApp()
            } else {
                requestStorageermission()
            }
        }
    }

    private fun updateApp() {
        if (updateMust) {
            FileDownloader.getImpl()
                    .create(updatePath)
                    .setPath(AppConstant.DIR + "kysj.apk")
                    .setListener(downloadListener)
                    .start()
        } else {
            startService<UpdateService>(Pair(UPDATE_PATH, updatePath))
            finish()
        }
    }

    private fun requestStorageermission() {
        PermissionUtils.permission(PermissionConstants.STORAGE)
                .rationale {
                    QMUIDialog.MessageDialogBuilder(act)
                            .setMessage(R.string.permission_rationale_storage)
                            .addAction(R.string.permission_dialog_cancle, { dialog, _ ->
                                it.again(false)
                                dialog.dismiss()
                            })
                            .addAction(R.string.permission_dialog_agress, { dialog, _ ->
                                it.again(true)
                                dialog.dismiss()
                            })
                            .show()
                }
                .callback(object : PermissionUtils.FullCallback {
                    override fun onGranted(permissionsGranted: MutableList<String>?) {
                        updateApp()
                    }

                    override fun onDenied(permissionsDeniedForever: MutableList<String>?, permissionsDenied: MutableList<String>?) {
                        if (permissionsDeniedForever != null && permissionsDeniedForever.isNotEmpty()) {
                            QMUIDialog.MessageDialogBuilder(act)
                                    .setMessage(R.string.permission_denied_storage)
                                    .addAction(R.string.permission_dialog_cancle, { dialog, _ ->
                                        dialog.dismiss()
                                    })
                                    .addAction(R.string.permission_dialog_agress, { dialog, _ ->
                                        PermissionUtils.launchAppDetailsSettings()
                                        dialog.dismiss()
                                    })
                                    .show()
                        }
                    }

                })
                .request()
    }

    private fun haveStroagePermission(): Boolean {
        return PermissionUtils.isGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }

    private val downloadListener by lazy {
        object : FileDownloadListener() {
            override fun warn(task: BaseDownloadTask?) {

            }

            override fun completed(task: BaseDownloadTask) {
                toast(R.string.update_success)
                AppUtils.installApp(task.path, Constant.AUTHORITY)
                AppUtils.exitApp()
            }

            override fun pending(task: BaseDownloadTask?, soFarBytes: Int, totalBytes: Int) {
                updateContent.setVisible(false)
                updatePb.setVisible(true)
            }

            override fun error(task: BaseDownloadTask?, e: Throwable?) {
                toast(R.string.update_failed)
                AppUtils.exitApp()
            }

            override fun progress(task: BaseDownloadTask?, soFarBytes: Int, totalBytes: Int) {
                updatePb.progress = ((soFarBytes.toFloat() / totalBytes.toFloat()) * 100).toInt()
            }

            override fun paused(task: BaseDownloadTask?, soFarBytes: Int, totalBytes: Int) {
            }

        }
    }

    override fun onBackPressedSupport() {

    }
}