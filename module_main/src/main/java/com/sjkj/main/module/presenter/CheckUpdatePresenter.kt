package com.sjkj.main.module.presenter

import com.blankj.utilcode.util.AppUtils
import com.sjkj.lib_common.base.BaseView
import com.sjkj.lib_common.rxhelper.RxSubscriber
import com.sjkj.lib_common.rxhelper.ioMain
import com.sjkj.main.module.data.UpdateBean
import com.sjkj.main.module.domain.repertory.UpdateRepertory

/**
 * @author by dingl on 2018/3/15.
 * @desc CheckUpdatePresenter
 */
interface UpdateView : BaseView {
    fun update(data: UpdateBean.Data)
    fun updateFailed()
    fun noUpdate()
}

class CheckUpdatePresenter {

    private val updateRepertory by lazy { UpdateRepertory() }

    lateinit var mView: UpdateView

    fun update() {
        updateRepertory.update()
                .compose(ioMain())
                .subscribe(object : RxSubscriber<UpdateBean>(mView) {
                    override fun _onNext(t: UpdateBean) {
                        if (t.GetAPPVersionInfoDto != null) {
                            checkUpdate(t.GetAPPVersionInfoDto[0])
                        } else
                            mView.updateFailed()
                    }

                    override fun _onError(toast: String) {
                        super._onError(toast)
                        mView.updateFailed()
                    }
                })
    }

    private fun checkUpdate(data: UpdateBean.Data) {
        if (data.AppCode > AppUtils.getAppVersionCode()) {
            mView.update(data)
        } else if (data.AppCode == AppUtils.getAppVersionCode()) {
            mView.noUpdate()
        }

    }

}