package com.sjkj.main.module.presenter

import com.sjkj.lib_common.base.BasePresenter
import com.sjkj.lib_common.base.BaseView
import com.sjkj.lib_common.data.LoginResult
import com.sjkj.lib_common.rxhelper.RxSubscriber
import com.sjkj.main.module.domain.repertory.AccountRepertory
import javax.inject.Inject

/**
 * @author by dingl on 2018/3/16.
 * @desc MofidyPwdPresenter
 */

interface ModifyPwdView : BaseView {
    fun modifySuccess(pwd: String)
    fun modifyFailed()
}

class MofidyPwdPresenter @Inject constructor() : BasePresenter<ModifyPwdView>() {

    @Inject
    lateinit var accountRepertory: AccountRepertory

    fun modifyPwd(oldPwd: String, newPwd: String) {
        accountRepertory.modifyPwd(oldPwd, newPwd)
                .ioMain(lifecycleProvider)
                .subscribe(object : RxSubscriber<LoginResult>(mView) {
                    override fun _onError(toast: String) {
                        mView.modifyFailed()
                    }

                    override fun _onNext(t: LoginResult) {
                        mView.modifySuccess(newPwd)
                    }
                })
    }

}