package com.sjkj.main.module.data

import com.sjkj.lib_common.base.BaseJsonResult


/**
 * @author by dingl on 2018/3/15.
 * @desc UpdateBean
 */

data class UpdateBean(
        val GetAPPVersionInfoDto: List<Data>?) : BaseJsonResult() {

    data class Data(
            val AppCode: Int,
            val AppPath: String,
            val AppVersion: String,
            val UpgradeFlag: Int,
            val Remark: String)
}