package com.sjkj.main.module.ui.service

import android.app.IntentService
import android.content.Intent
import com.blankj.utilcode.util.AppUtils
import com.blankj.utilcode.util.ToastUtils
import com.liulishuo.filedownloader.BaseDownloadTask
import com.liulishuo.filedownloader.FileDownloadListener
import com.liulishuo.filedownloader.FileDownloader
import com.sjkj.lib_common.common.AppConstant
import com.sjkj.lib_common.common.Constant
import com.sjkj.lib_common.utils.NotificationUtils
import com.sjkj.main.R
import com.sjkj.main.module.ui.activity.CheckUpdateActivity
import com.sjkj.main.module.ui.service.CheckUpdateService.Companion.CHANNEDID

/**
 * @author by dingl on 2018/3/15.
 * @desc UpdateService
 */
class UpdateService : IntentService("UpdateService") {

    private var beforeTime: Long = 0L

    override fun onHandleIntent(intent: Intent?) {
        val updatePath = intent?.extras?.getString(CheckUpdateActivity.UPDATE_PATH)
        if (updatePath != null) {
            setupNotify()
            update(updatePath)
        } else
            ToastUtils.showShort(R.string.update_failed)
    }

    private fun setupNotify() {
        NotificationUtils.updateNotify(this, CHANNEDID)
        beforeTime = System.currentTimeMillis()
    }

    private fun update(updatePath: String?) {
        FileDownloader.getImpl()
                .create(updatePath)
                .setPath(AppConstant.DIR + "kysj.apk")
                .setListener(object : FileDownloadListener() {
                    override fun warn(task: BaseDownloadTask?) {
                        NotificationUtils.cancleNotify(CHANNEDID)
                    }

                    override fun completed(task: BaseDownloadTask) {
                        ToastUtils.showShort(R.string.update_success)
                        NotificationUtils.cancleNotify(CHANNEDID)
                        AppUtils.installApp(task.path, Constant.AUTHORITY)
                    }

                    override fun pending(task: BaseDownloadTask?, soFarBytes: Int, totalBytes: Int) {
                    }

                    override fun error(task: BaseDownloadTask?, e: Throwable?) {
                        NotificationUtils.cancleNotify(CHANNEDID)
                        ToastUtils.showShort(R.string.update_failed)
                    }

                    override fun progress(task: BaseDownloadTask?, soFarBytes: Int, totalBytes: Int) {
                        if (System.currentTimeMillis() - beforeTime > 1000) {
                            beforeTime = System.currentTimeMillis()
                            val percent = ((soFarBytes.toFloat() / totalBytes.toFloat()) * 100).toInt()
                            NotificationUtils.updateProgressNotify(percent, CHANNEDID)
                        }
                    }

                    override fun paused(task: BaseDownloadTask?, soFarBytes: Int, totalBytes: Int) {
                        NotificationUtils.cancleNotify(CHANNEDID)
                    }

                })
                .start()
    }

    override fun onDestroy() {
        super.onDestroy()
        NotificationUtils.cancleNotify(CHANNEDID)
    }
}