package com.sjkj.main.module.domain.api

import com.sjkj.main.module.data.UserCenterBean
import com.sjkj.main.module.data.UserSchoolBean
import com.sjkj.main.module.domain.requestbody.UserSchoolRequestBody
import retrofit2.http.Body
import retrofit2.http.POST
import rx.Observable

/**
 * @author by dingl on 2018/3/16.
 * @desc UserCenterApi
 */
interface UserCenterApi {
    @POST("UserCenterService.svc/GetUserInfo")
    fun getUserCenterData(): Observable<UserCenterBean>

    @POST("CommonService.svc/GetSchool")
    fun getUserSchoolData(@Body userSchoolRequestBody: UserSchoolRequestBody): Observable<UserSchoolBean>
}