package com.sjkj.main.module.ui.activity

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.v7.widget.StaggeredGridLayoutManager
import android.widget.TextView
import com.alibaba.android.arouter.facade.annotation.Route
import com.alibaba.android.arouter.launcher.ARouter
import com.blankj.utilcode.util.AppUtils
import com.blankj.utilcode.util.TimeUtils
import com.blankj.utilcode.util.ToastUtils
import com.qmuiteam.qmui.widget.QMUIRadiusImageView
import com.sjkj.lib_common.base.BaseRxActivity
import com.sjkj.lib_common.common.Constant
import com.sjkj.lib_common.common.HttpStore
import com.sjkj.lib_common.extensions.startCameraActivity
import com.sjkj.lib_common.utils.GlideApp
import com.sjkj.lib_common.utils.GlideEngine
import com.sjkj.main.R
import com.sjkj.main.module.data.MainTabBean
import com.sjkj.main.module.domain.repertory.UserUtils
import com.sjkj.main.module.ui.adapter.MainAdapter
import com.sjkj.main.module.ui.service.CheckUpdateService
import com.sjkj.main.module.ui.view.showLightSettingDialog
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.find
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.startService
import org.jetbrains.anko.toast
import java.text.SimpleDateFormat
import java.util.*
import kotlin.concurrent.timerTask


/**
 * @author by dingl on 2018/3/8.
 * @desc MainActivity
 */
@Route(path = "/main/main")
class MainActivity : BaseRxActivity() {

    companion object {
        const val REQUEST_CODE_CHOOSE = 0x001
    }

    override fun getLayoutId(): Int = R.layout.activity_main

    private val mainTabDatas = ArrayList<MainTabBean>()

    private val timer = Timer()

    private val waitTime = 2000L

    private var touchTime = 0L


    override fun initView(savedInstanceState: Bundle?) {

        setupHeadData()

        setupMainTabDatas()

        checkNotNull(mainTabDatas) { "the mainTabDatas is null" }

        with(homeRecyclerView) {
            adapter = MainAdapter(mainTabDatas) {
                onAdapterItemClick(it)
            }
            layoutManager = StaggeredGridLayoutManager(Constant.SPAN_COUNT, StaggeredGridLayoutManager.VERTICAL)
        }

        setUserData()

        setUserHead()

        setListener()

        checkUpdate()

    }

    private fun onAdapterItemClick(index: Int) {
        when (index) {
            0 -> {
            }
            1 -> ARouter.getInstance()
                    .build("/course/class")
                    .navigation()
            7 -> ARouter.getInstance()
                    .build("/user/user")
                    .navigation()
            else -> {
            }
        }
    }

    private fun checkUpdate() {
        toast("我是通过热更新加载的23")
        startService<CheckUpdateService>()
    }

    private fun setUserHead() {
        setHeadImg(homeNavigationView.getHeaderView(0).find(R.id.userHead))
        setHeadImg(homeHead)
    }

    private fun setHeadImg(homeHead: QMUIRadiusImageView) {
        GlideApp.with(this)
                .load(HttpStore.Base_Img_Url + UserUtils.getHeadImg())
                .into(homeHead)
        homeHead.onClick {
            Matisse.from(this@MainActivity)
                    .choose(MimeType.allOf())
                    .countable(true)
                    .maxSelectable(1)
                    .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                    .thumbnailScale(0.85f)
                    .imageEngine(GlideEngine())
                    .forResult(REQUEST_CODE_CHOOSE)
        }
    }

    private fun setListener() {
        homeNavigationView.menu.findItem(R.id.drawerVersion).title = String.format(getString(R.string.drawer_user_version), AppUtils.getAppVersionName())
        homeNavigationView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.drawerCamera -> startCameraActivity()
                R.id.drawerApp -> ARouter.getInstance().build("/main/app").navigation()
                R.id.drawerClearFile -> {

                }
                R.id.drawerClearCache -> {

                }
                R.id.drawerWifi -> ARouter.getInstance().build("/main/wifi").navigation()
                R.id.drawerLight -> showLightSettingDialog()
                R.id.drawerNetWork -> {

                }
                R.id.drawerModifyPd -> ARouter.getInstance().build("/main/modify_pwd").navigation()
                R.id.drawerSwitchUser -> {

                }
            }

            true
        }
    }

    private fun setUserData() {
        val name = UserUtils.getName()
        homeNavigationView.getHeaderView(0).find<TextView>(R.id.username).text = name
        homeName.text = name
    }

    private fun setupHeadData() {
        setTimeData()
        timer.schedule(timerTask {
            runOnUiThread {
                setTimeData()
            }
        }, 1000L, 1000)
    }

    private fun setupMainTabDatas() {
        val mainTabNames = resources.obtainTypedArray(R.array.home_tab_name)
        val mainTabIcons = resources.obtainTypedArray(R.array.home_tab_icon)
        try {
            if (mainTabIcons.length() == mainTabNames.length()) {
                (0 until mainTabIcons.length()).mapTo(mainTabDatas) { MainTabBean(it, mainTabNames.getString(it), mainTabIcons.getDrawable(it)) }
            } else
                throw IllegalArgumentException("The number of names and pictures doesn't right")
        } finally {
            mainTabNames.recycle()
            mainTabIcons.recycle()
        }
    }

    private fun setTimeData() {
        homeTime?.text = TimeUtils.getNowString(SimpleDateFormat("HH:mm:ss", Locale.getDefault()))
        homeDate?.text = TimeUtils.getNowString(SimpleDateFormat("yyyy年MM月dd日  EEEE", Locale.getDefault()))
    }

    override fun onBackPressedSupport() {
        if (System.currentTimeMillis() - touchTime < waitTime) {
            timer.cancel()
            finish()
        } else {
            touchTime = System.currentTimeMillis()
            ToastUtils.showShort(R.string.app_exit_again)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        timer.cancel()
    }

}