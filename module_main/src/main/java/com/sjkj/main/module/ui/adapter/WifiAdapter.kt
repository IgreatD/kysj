package com.sjkj.main.module.ui.adapter


import android.net.wifi.*
import android.text.TextUtils
import android.widget.TextView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.sjkj.lib_common.extensions.getColorWithExtension
import com.sjkj.main.R
import com.sjkj.main.module.data.WifiBean
import com.sjkj.main.module.utils.WifiAdmin
import org.jetbrains.anko.sdk25.coroutines.onClick


/**
 * @author dingl on 2017/8/4
 */

class WifiAdapter(private val wifiSetting: (WifiBean) -> Unit) : BaseQuickAdapter<ScanResult, BaseViewHolder>(R.layout.adapter_wifi) {

    private var wifiInfo: WifiInfo? = null

    private var configurations: List<WifiConfiguration>? = null

    fun setData(scanResults: MutableList<ScanResult>?, configurations: List<WifiConfiguration>?, wifiInfo: WifiInfo) {
        this.wifiInfo = wifiInfo
        this.configurations = configurations
        if (this.configurations != null) {
            setNewData(scanResults)
        } else
            setNewData(null)
        notifyDataSetChanged()
    }

    override fun convert(helper: BaseViewHolder, item: ScanResult?) {
        item ?: return
        with(helper) {
            setText(R.id.wifiName, item.SSID)
            val signalLevel = WifiManager.calculateSignalLevel(item.level, 5)
            val capabilityType = WifiAdmin.getCapabilityType(item.capabilities)
            when (signalLevel) {
                0 -> {
                    if (capabilityType == 3) {
                        setImageResource(R.id.wifiSignal, R.drawable._0suo)
                        setTag(R.id.wifiSignal, R.drawable._0suo)
                    } else {
                        setImageResource(R.id.wifiSignal, R.drawable._0)
                        setTag(R.id.wifiSignal, R.drawable._0)
                    }
                }
                1 -> {
                    if (capabilityType == 3) {
                        setImageResource(R.id.wifiSignal, R.drawable._1suo)
                        setTag(R.id.wifiSignal, R.drawable._1suo)
                    } else {
                        setImageResource(R.id.wifiSignal, R.drawable._1)
                        setTag(R.id.wifiSignal, R.drawable._1)
                    }
                }
                2 -> {
                    if (capabilityType == 3) {
                        setImageResource(R.id.wifiSignal, R.drawable._2suo)
                        setTag(R.id.wifiSignal, R.drawable._2suo)
                    } else {
                        setImageResource(R.id.wifiSignal, R.drawable._2)
                        setTag(R.id.wifiSignal, R.drawable._2)
                    }
                }
                3 -> {
                    if (capabilityType == 3) {
                        setImageResource(R.id.wifiSignal, R.drawable._3suo)
                        setTag(R.id.wifiSignal, R.drawable._3suo)
                    } else {
                        setImageResource(R.id.wifiSignal, R.drawable._3)
                        setTag(R.id.wifiSignal, R.drawable._3)
                    }
                }
                4 -> {
                    if (capabilityType == 3) {
                        setImageResource(R.id.wifiSignal, R.drawable._4suo)
                        setTag(R.id.wifiSignal, R.drawable._4suo)
                    } else {
                        setImageResource(R.id.wifiSignal, R.drawable._4)
                        setTag(R.id.wifiSignal, R.drawable._4)
                    }
                }
                else -> {
                    if (capabilityType == 3) {
                        setImageResource(R.id.wifiSignal, R.drawable._0suo)
                        setTag(R.id.wifiSignal, R.drawable._0suo)
                    } else {
                        setImageResource(R.id.wifiSignal, R.drawable._0)
                        setTag(R.id.wifiSignal, R.drawable._0)
                    }
                }
            }

            if (wifiInfo != null && wifiInfo!!.ssid == "\"" + item.SSID + "\"") {
                when {
                    wifiInfo!!.supplicantState == SupplicantState.ASSOCIATING -> {
                        setText(R.id.wifiStaus, "正在连接...")
                        setTag(R.id.wifiStaus, STATE_CONNECTING)
                        setTextColor(R.id.wifiName, getColorWithExtension(R.color.colorPrimary))
                        setTextColor(R.id.wifiStaus, getColorWithExtension(R.color.colorPrimary))
                    }
                    wifiInfo!!.supplicantState == SupplicantState.COMPLETED -> {
                        setText(R.id.wifiStaus, "已连接")
                        setTag(R.id.wifiStaus, STATE_CONNECTED)
                        setTextColor(R.id.wifiName, getColorWithExtension(R.color.colorPrimary))
                        setTextColor(R.id.wifiStaus, getColorWithExtension(R.color.colorPrimary))
                        when (signalLevel) {
                            0 -> setImageResource(R.id.wifiSignal, R.drawable.wuxinhao)
                            1 -> setImageResource(R.id.wifiSignal, R.drawable.xuanzhong)
                            2 -> setImageResource(R.id.wifiSignal, R.drawable.xuanzhong_)
                            3 -> setImageResource(R.id.wifiSignal, R.drawable.xuanzhongqiang_)
                            4 -> setImageResource(R.id.wifiSignal, R.drawable.wuxian_hover)
                        }
                    }
                    wifiInfo!!.supplicantState == SupplicantState.SCANNING -> {
                        setText(R.id.wifiStaus, "正在进行验证身份...")
                        setTag(R.id.wifiStaus, STATE_CONNECTING)
                        setTextColor(R.id.wifiName, getColorWithExtension(R.color.colorPrimary))
                        setTextColor(R.id.wifiStaus, getColorWithExtension(R.color.colorPrimary))
                    }
                    else -> {
                        if (wifiInfo!!.ssid == "\"" + item.SSID + "\"") {
                            setText(R.id.wifiStaus, "已保存")
                        }
                        setTag(R.id.wifiStaus, STATE_SAVE)
                        setTextColor(R.id.wifiName, getColorWithExtension(R.color.qmui_config_color_75_pure_black))
                        setTextColor(R.id.wifiStaus, getColorWithExtension(R.color.qmui_config_color_50_pure_black))

                        when (signalLevel) {
                            0 -> setImageResource(R.id.wifiSignal, R.drawable._0)
                            1 -> setImageResource(R.id.wifiSignal, R.drawable._1)
                            2 -> setImageResource(R.id.wifiSignal, R.drawable._2)
                            3 -> setImageResource(R.id.wifiSignal, R.drawable._3)
                            4 -> setImageResource(R.id.wifiSignal, R.drawable._4)
                        }
                    }
                }
            } else {
                setText(R.id.wifiStaus, "未连接")
                setTag(R.id.wifiStaus, STATE_UNSAVE)
                setTextColor(R.id.wifiName, getColorWithExtension(R.color.qmui_config_color_75_pure_black))
                setTextColor(R.id.wifiStaus, getColorWithExtension(R.color.qmui_config_color_50_pure_black))
            }

            for (i in configurations!!.indices) {
                if (configurations!![i].SSID == "\"" + item.SSID + "\"" && configurations!![i].SSID != wifiInfo!!.ssid) {
                    setText(R.id.wifiStaus, if (TextUtils.isEmpty(WifiAdmin.getCapability(item.capabilities, mContext))) "已保存"
                    else
                        "已保存，" + WifiAdmin.getCapability(item.capabilities, mContext))
                    setTag(R.id.wifiStaus, STATE_SAVE)
                }
            }

            itemView.onClick {
                val state = getView<TextView>(R.id.wifiStaus).tag as Int
                val wifiBean = WifiBean(capabilityType, state, item)
                if (configurations != null && configurations?.isNotEmpty() == true) {
                    for (i in configurations.orEmpty().indices) {
                        if (configurations?.get(i)?.SSID == ("\"" + item.SSID + "\"")) {
                            wifiBean.wifiConfiguration = configurations?.get(i)
                        }
                    }
                }
                wifiSetting(wifiBean)
            }
        }
    }

    companion object {
        const val STATE_CONNECTED = 0
        const val STATE_CONNECTING = 1
        const val STATE_SAVE = 2
        const val STATE_UNSAVE = 3
    }

}
