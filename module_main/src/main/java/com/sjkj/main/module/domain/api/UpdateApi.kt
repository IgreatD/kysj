package com.sjkj.main.module.domain.api

import com.sjkj.main.module.data.UpdateBean
import com.sjkj.main.module.domain.requestbody.UpdateReqestBody
import retrofit2.http.Body
import retrofit2.http.POST
import rx.Observable

/**
 * @author by dingl on 2018/3/15.
 * @desc UpdateApi
 */
interface UpdateApi {
    @POST("CommonService.svc/GetAPPVersion2")
    fun updateApp(@Body reqestBody: UpdateReqestBody): Observable<UpdateBean>
}