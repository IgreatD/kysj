package com.sjkj.main.module.ui.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.sjkj.main.R
import com.sjkj.main.module.data.MainTabBean
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 * @author by dingl on 2018/3/8.
 * @desc MainAdapter
 */
class MainAdapter(datas: List<MainTabBean>, private val itemClick: (Int) -> Unit)
    : BaseQuickAdapter<MainTabBean, BaseViewHolder>(R.layout.adapter_main_list, datas) {
    override fun convert(helper: BaseViewHolder, item: MainTabBean?) {
        item ?: return
        helper.setImageDrawable(R.id.mainTabView, item.icon)
                .setText(R.id.mainTabName, item.name)
                .itemView.onClick { itemClick(item.id) }

    }
}