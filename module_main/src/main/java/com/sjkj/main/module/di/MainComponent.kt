package com.sjkj.main.module.di

import com.sjkj.lib_common.di.BaseComponent
import com.sjkj.lib_common.di.scope.PerUser
import com.sjkj.main.module.ui.activity.LoginActivity
import com.sjkj.main.module.ui.activity.ModifyPwdActivity
import com.sjkj.main.module.ui.activity.SplashActivity
import com.sjkj.main.module.ui.activity.StudentUserInfoActivity
import dagger.Component

/**
 * @author by dingl on 2018/3/8.
 * @desc Component
 */
@PerUser
@Component(dependencies = [(BaseComponent::class)])
interface MainComponent {

    fun inject(loginActivity: LoginActivity)
    fun inject(splashActivity: SplashActivity)
    fun inject(modifyPwdActivity: ModifyPwdActivity)
    fun inject(studentUserInfoActivity: StudentUserInfoActivity)
}

