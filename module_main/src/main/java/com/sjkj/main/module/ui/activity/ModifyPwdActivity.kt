package com.sjkj.main.module.ui.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.alibaba.android.arouter.facade.annotation.Route
import com.blankj.utilcode.util.ToastUtils
import com.sjkj.lib_common.base.BaseMvpActivity
import com.sjkj.lib_common.common.UserConstant
import com.sjkj.lib_common.extensions.injectBaseComponet
import com.sjkj.lib_common.utils.PreferenceUtils
import com.sjkj.main.R
import com.sjkj.main.module.di.DaggerMainComponent
import com.sjkj.main.module.domain.repertory.UserUtils
import com.sjkj.main.module.presenter.ModifyPwdView
import com.sjkj.main.module.presenter.MofidyPwdPresenter
import kotlinx.android.synthetic.main.activity_modify_pwd.*
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 * @author by dingl on 2018/3/16.
 * @desc ModifyPwdActivity
 */
@Route(path = "/main/modify_pwd")
class ModifyPwdActivity : BaseMvpActivity<MofidyPwdPresenter>(), ModifyPwdView {

    override fun getLayoutId() = R.layout.activity_modify_pwd

    private var pwd by PreferenceUtils(UserConstant.ACCOUNT_PASSWORD, "")

    override fun injectComponent() {
        DaggerMainComponent.builder()
                .baseComponent(injectBaseComponet())
                .build()
                .inject(this)
        mPresenter.mView = this
    }

    override fun initView(savedInstanceState: Bundle?) {
        topbar.setTitle(R.string.modify_pwd)
        topbar.addLeftBackImageButton().onClick { finish() }
        setupLayout()
        setupListener()
    }

    private fun setupListener() {
        oldEditText.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_NEXT && isOldPwdValid()) {
                oldInputLayout.isErrorEnabled = false
                newEditText.requestFocus()
                return@OnEditorActionListener true
            }
            oldInputLayout.isErrorEnabled = true
            oldInputLayout.error = getString(R.string.old_pwd_error)
            true
        })

        newEditText.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_NEXT && newPwdValid()) {
                newInputLayout.isErrorEnabled = false
                confirmEditText.requestFocus()
                return@OnEditorActionListener true
            }
            newInputLayout.isErrorEnabled = true
            newInputLayout.error = getString(R.string.new_pwd_length_error)
            true
        })

        confirmEditText.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE && isPwdValid()) {
                modify.performClick()
                confirmInputLayout.isErrorEnabled = false
                return@OnEditorActionListener true
            }
            confirmInputLayout.isErrorEnabled = false
            confirmInputLayout.error = getString(R.string.new_pwd_equal_confirm_pwd)
            true
        })

        confirmEditText.addTextChangedListener(pwdFieldWatcher)
        newEditText.addTextChangedListener(pwdFieldWatcher)
        oldEditText.addTextChangedListener(pwdFieldWatcher)

        modify.onClick {
            if (checkContent()) {
                mPresenter.modifyPwd(pwd, newEditText.text.toString())
            }
        }
    }

    private fun checkContent(): Boolean {
        if (isOldPwdValid()) {
            oldInputLayout.isErrorEnabled = false
            return if (newPwdValid()) {
                newInputLayout.isErrorEnabled = false
                if (isPwdValid()) {
                    confirmInputLayout.isErrorEnabled = false
                    true
                } else {
                    confirmInputLayout.isErrorEnabled = false
                    confirmInputLayout.error = getString(R.string.new_pwd_equal_confirm_pwd)
                    false
                }
            } else {
                newInputLayout.isErrorEnabled = true
                newInputLayout.error = getString(R.string.new_pwd_length_error)
                false
            }
        } else {
            oldInputLayout.isErrorEnabled = true
            oldInputLayout.error = getString(R.string.old_pwd_error)
            ToastUtils.showShort(R.string.old_pwd_error)
            return false
        }
    }

    private fun isPwdValid() = newPwdValid() && confirmValid() && pwdValid()

    private fun pwdValid() = newEditText.text.toString() == confirmEditText.text.toString()

    private fun confirmValid() = confirmEditText.length() in 6..20

    private fun newPwdValid() = newEditText.length() in 6..20

    private fun isOldPwdValid() = oldEditText.text.toString() == pwd

    private val pwdFieldWatcher = object : TextWatcher {

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable) {
            modify.isEnabled = isPwdValid()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setupLayout() {
        userName.text = "账号： ${UserUtils.userBean.UserName}"
        nickName.text = "昵称： ${UserUtils.userBean.Name}"
    }

    override fun modifySuccess(pwd: String) {
        this.pwd = pwd
        ToastUtils.showShort(R.string.modify_pwd_success)
        finish()
    }

    override fun modifyFailed() {
        ToastUtils.showShort(R.string.modify_pwd_failed)
    }

}