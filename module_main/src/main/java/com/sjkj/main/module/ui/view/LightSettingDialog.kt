package com.sjkj.main.module.ui.view

import android.app.Activity
import android.content.Context
import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.SeekBar
import com.qmuiteam.qmui.widget.dialog.QMUIDialog
import com.qmuiteam.qmui.widget.dialog.QMUIDialogBuilder
import com.sjkj.main.R
import com.sjkj.main.module.utils.ScreenManager
import org.jetbrains.anko.sdk25.coroutines.onCheckedChange
import org.jetbrains.anko.sdk25.coroutines.onSeekBarChangeListener

/**
 * @author by dingl on 2018/3/12.
 * @desc LightSettingDialog
 */
fun Activity.showLightSettingDialog() {
    val builder = CustomDialogBuilder(this)
    val dialog = builder.setLayout(R.layout.dialog_light_setting).create()
    dialog.show()
    val sk = builder.view?.findViewById<SeekBar>(R.id.sk)
    val cb = builder.view?.findViewById<CheckBox>(R.id.cb)
    sk?.progress = ScreenManager.getScreenBrightness(this)
    sk?.onSeekBarChangeListener {
        object : SeekBar.OnSeekBarChangeListener {
            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                cb?.isChecked = false
                ScreenManager.setScreenMode(this@showLightSettingDialog, 0)
                ScreenManager.changeAppBrightnessSystem(this@showLightSettingDialog, progress)
            }

        }
    }
    val screenMode = ScreenManager.getScreenMode(this)
    cb?.isChecked = screenMode != 0
    cb?.onCheckedChange { _, isChecked ->
        if (isChecked) {
            ScreenManager.setScreenMode(this@showLightSettingDialog, 1)
        } else {
            ScreenManager.setScreenMode(this@showLightSettingDialog, 0)
        }
        sk?.progress = ScreenManager.getScreenBrightness(this@showLightSettingDialog)
    }
}

class CustomDialogBuilder(context: Context) : QMUIDialogBuilder<CustomDialogBuilder>(context) {
    override fun onCreateContent(dialog: QMUIDialog?, parent: ViewGroup, context: Context?) {
        val view = LayoutInflater.from(context).inflate(mLayoutId, parent, false)
        this.view = view
        parent.addView(view)
    }

    private var mLayoutId: Int = 0

    var view: View? = null

    fun setLayout(@LayoutRes layoutResId: Int): CustomDialogBuilder {
        mLayoutId = layoutResId
        return this
    }
}