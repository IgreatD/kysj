package com.sjkj.main.module.presenter

import com.google.gson.Gson
import com.sjkj.lib_common.base.BasePresenter
import com.sjkj.lib_common.base.BaseView
import com.sjkj.lib_common.common.UserConstant
import com.sjkj.lib_common.data.LoginResult
import com.sjkj.lib_common.rxhelper.RxSubscriber
import com.sjkj.lib_common.rxhelper.ioMain
import com.sjkj.lib_common.utils.PreferenceUtils
import com.sjkj.main.module.domain.repertory.AccountRepertory
import org.jetbrains.anko.doAsync
import javax.inject.Inject

/**
 * @author by dingl on 2018/3/24.
 * @desc LoginPresenter
 */
interface LoginView : BaseView {

    fun loginSuccess()

    fun loginFailed(userName: String, password: String)

}

class LoginPresenter @Inject constructor() : BasePresenter<LoginView>() {

    @Inject
    lateinit var accountRepertory: AccountRepertory

    private var userName by PreferenceUtils(UserConstant.ACCOUNT_USERNAME, "")
    private var name by PreferenceUtils(UserConstant.ACCOUNT_NAME, "")
    private var passWord by PreferenceUtils(UserConstant.ACCOUNT_PASSWORD, "")
    private var accountId by PreferenceUtils(UserConstant.ACCOUNT_ID, 0)
    private var token by PreferenceUtils(UserConstant.ACCOUNT_TOKEN, "")
    private var userBean by PreferenceUtils(UserConstant.USER, "")

    fun login(userName: String, password: String) {
        accountRepertory.login(userName, password)
                .compose(ioMain(lifecycleProvider))
                .subscribe(object : RxSubscriber<LoginResult>(mView) {
                    override fun _onNext(t: LoginResult) {
                        mView.showLoading()
                        activity.doAsync {
                            if (t.LoginInfoDto != null) {
                                val userBean = t.LoginInfoDto!![0]
                                accountId = userBean.AccountID
                                name = userBean.Name
                                this@LoginPresenter.userName = userName
                                passWord = password
                                token = t.Token
                                userBean.token = t.Token
                                userBean.PassWord = password
                                this@LoginPresenter.userBean = Gson().toJson(userBean)
                                activity.runOnUiThread {
                                    mView.hideLoading()
                                    mView.loginSuccess()
                                }
                            } else {
                                activity.runOnUiThread {
                                    mView.hideLoading()
                                    mView.loginFailed(userName, password)
                                }
                            }
                        }
                    }

                    override fun _onError(toast: String) {
                        super._onError(toast)
                        mView.loginFailed(userName, password)
                    }

                })
    }

}