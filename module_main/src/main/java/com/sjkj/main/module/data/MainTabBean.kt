package com.sjkj.main.module.data

import android.graphics.drawable.Drawable

/**
 * @author by dingl on 2018/3/8.
 * @desc MainTabBean
 */
data class MainTabBean(
        val id: Int,
        val name: String,
        val icon: Drawable
)