package com.sjkj.main.module.data

import com.sjkj.lib_common.base.BaseJsonResult


/**
 * @author by dingl on 2018/3/16.
 * @desc UserCenterBean
 */
data class UserCenterBean(val GetUserInfoInfoDto: List<Data>?) : BaseJsonResult() {
    data class Data(
            val Address: String,
            val AuditionLength: Long,
            val Birthday: String,
            val CreateDate: String,
            val Email: String,
            val MyInvitationCode: String,
            val MyRefererUserName: String,
            val Name: String,
            val ParentEmail: String,
            val ParentPhone: String,
            val QQ: String,
            val School: String,
            val Sex: String,
            val Signature: String
    )
}