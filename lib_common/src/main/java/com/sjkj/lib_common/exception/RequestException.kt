package com.sjkj.lib_common.exception

import com.google.gson.JsonParseException
import org.json.JSONException
import retrofit2.HttpException
import java.net.ConnectException
import java.text.ParseException


/**
 * @author by dingl on 2017/9/30.
 * @desc RequestException
 */
class RequestFailedException(message: String) : Exception(message)

class RequestTokenOutOfDateException(message: String) : Throwable(message)

class RequestTokenNotExistException(message: String) : Exception(message)

class RequestNullDataException(message: String) : Exception(message)

object ERROR {
    const val UNKNOWN = 1000
    const val PARSE_ERROR = 1001
    const val NETWORD_ERROR = 1002
    const val HTTP_ERROR = 1003
}

class ApiException(throwable: Throwable, var code: Int) : Exception(throwable) {
    override var message: String? = null
}

class ServerException : RuntimeException() {
    var code: Int = 0
    override var message: String? = null
}

object ExceptionEngine {

    fun handleException(e: Throwable): ApiException {
        val ex: ApiException
        if (e is HttpException) {
            ex = ApiException(e, ERROR.HTTP_ERROR)
            ex.message = "网络错误"
            return ex
        } else if (e is ServerException) {
            ex = ApiException(e, e.code)
            ex.message = e.message
            return ex
        } else if (e is JsonParseException
                || e is JSONException
                || e is ParseException) {
            ex = ApiException(e, ERROR.PARSE_ERROR)
            ex.message = "解析错误"
            return ex
        } else if (e is ConnectException) {
            ex = ApiException(e, ERROR.NETWORD_ERROR)
            ex.message = "连接失败"
            return ex
        } else {
            ex = ApiException(e, ERROR.UNKNOWN)
            ex.message = "未知错误"
            return ex
        }
    }
}
