package com.sjkj.lib_common.http

import com.blankj.utilcode.util.Utils.getApp
import io.reactivex.Observable
import io.rx_cache2.DynamicKey
import io.rx_cache2.EvictDynamicKey
import io.rx_cache2.Reply
import io.rx_cache2.internal.RxCache
import io.victoralbertos.jolyglot.GsonSpeaker

/**
 * @author by dingl on 2018/3/29.
 * @desc CacheProviders
 */
fun getCacheProviders(): CacheProviders =
        RxCache.Builder()
                .useExpiredDataIfLoaderNotAvailable(true)
                .persistence(getApp().cacheDir, GsonSpeaker())
                .using(CacheProviders::class.java)

interface CacheProviders {

    fun <T> getDataWithCache(data: Observable<T>,
                             dynamicKey: DynamicKey,
                             evictDynamicKey: EvictDynamicKey = EvictDynamicKey(false)
    ): Observable<Reply<T>>

}