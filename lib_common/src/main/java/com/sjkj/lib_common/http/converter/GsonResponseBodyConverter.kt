package com.sjkj.lib_common.http.converter

import android.util.Log
import com.google.gson.Gson
import com.google.gson.TypeAdapter
import com.sjkj.lib_common.base.BaseJsonResult
import com.sjkj.lib_common.exception.RequestTokenNotExistException
import com.sjkj.lib_common.exception.RequestTokenOutOfDateException
import okhttp3.ResponseBody
import retrofit2.Converter
import java.io.IOException

@Suppress("ConstantConditionIf")
internal class GsonResponseBodyConverter<T>(private val gson: Gson, private val adapter: TypeAdapter<T>) : Converter<ResponseBody, T> {

    @Throws(IOException::class)
    override fun convert(value: ResponseBody): T {
        value.use {
            val read = adapter.read(gson.newJsonReader(value.charStream()))
            if (read is BaseJsonResult) {
                if (read.Code == -6 || read.Code == -7 || read.Code == -8 || read.Code == -5) {
                    Log.d("get_token", "token 无效，重新获取token")
                    throw RequestTokenOutOfDateException("-6")
                } else if (read.Code == -9) {
                    throw RequestTokenNotExistException(read.Message)
                }
            }
            return read
        }
    }
}
