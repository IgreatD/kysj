package com.sjkj.lib_common.http

import com.orhanobut.logger.Logger
import com.sjkj.lib_common.common.UserConstant
import com.sjkj.lib_common.utils.PreferenceUtils
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author by dingl on 2018/1/17.
 * @desc HttpLogInterceptor
 */

@Singleton
class HttpLog : HttpLoggingInterceptor.Logger {
    override fun log(message: String?) {
        Logger.d(message)
    }

    val mMessage = StringBuilder()

//    return HttpLoggingInterceptor.Logger { message ->
    //            var msg = ""
//            if (message.startsWith("--> POST")) {
//                mMessage.setLength(0)
//            }
//            if ((message.startsWith("{") && message.endsWith("}")) ||
//                    (message.startsWith("[") && message.endsWith("]"))) {
//                msg = JsonUtils.formatJson(JsonUtils.decodeUnicode(message))
//            }
//            mMessage.append(msg).append("\n")
//            if (message.startsWith("<-- END HTTP")) {
//                Logger.d(mMessage.toString())
//            }

}


@Singleton
class RequestInterceptor @Inject constructor() : Interceptor {

    private val token by PreferenceUtils(UserConstant.ACCOUNT_TOKEN, "")

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
                .newBuilder()
                .addHeader("Content_Type", "application/json")
                .addHeader("charset", "UTF-8")
                .addHeader("Token", token)
                .build()
        return chain.proceed(request)
    }
}
