package com.sjkj.lib_common.http

import com.sjkj.lib_common.common.Constant
import com.sjkj.lib_common.http.converter.GsonConverterFactory
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.lang.reflect.Type
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * @author by dingl on 2018/1/15.
 * @desc RetrofitFactory
 */
@Singleton
class RetrofitFactory {

    companion object {
        val instance by lazy { RetrofitFactory() }
    }

    fun getRetrofit(baseUrl: String): Retrofit {
        val nullOnEmptyConverterFactory = object : Converter.Factory() {

            fun converterFactory() = this

            override fun responseBodyConverter(type: Type, annotations: Array<out Annotation>, retrofit: Retrofit) = object : Converter<ResponseBody, Any?> {
                val nextResponseBodyConverter = retrofit.nextResponseBodyConverter<Any?>(converterFactory(), type, annotations)
                override fun convert(value: ResponseBody) = if (value.contentLength() != 0L) nextResponseBodyConverter.convert(value) else null
            }
        }

        return Retrofit.Builder()
                .client(initClient())
                .baseUrl(baseUrl)
                .addConverterFactory(nullOnEmptyConverterFactory)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    private fun initClient() =
            OkHttpClient.Builder()
                    .connectTimeout(Constant.CONNECT_TIMEOUT_IN_MS, TimeUnit.SECONDS)
                    .readTimeout(Constant.READ_TIMEOUT, TimeUnit.SECONDS)
                    .addInterceptor(RequestInterceptor())
                    .build()
}


