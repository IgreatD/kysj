package com.sjkj.lib_common.http

import com.sjkj.lib_common.common.HttpStore

/**
 * @author by dingl on 2018/3/8.
 * @desc HttpExt
 */

inline fun <reified T> createService(service: Class<T>): T = RetrofitFactory.instance.getRetrofit(HttpStore.Base_Url).create(service)

inline fun <reified T> createUploadService(service: Class<T>): T = RetrofitFactory.instance.getRetrofit(HttpStore.Base_Img_Url).create(service)