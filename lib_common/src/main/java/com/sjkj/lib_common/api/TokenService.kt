package com.sjkj.lib_common.api

import android.annotation.SuppressLint
import android.util.Log
import com.blankj.utilcode.util.EncryptUtils
import com.sjkj.lib_common.base.BaseJsonResult
import com.sjkj.lib_common.common.UserConstant
import com.sjkj.lib_common.exception.RequestTokenNotExistException
import com.sjkj.lib_common.http.createService
import com.sjkj.lib_common.utils.PreferenceUtils
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * @author by dingl on 2018/3/17.
 * @desc TokenApi
 */
interface TokenApi {
    @POST("CommonService.svc/GetUserToken")
    fun getToken(@Body tokenRequestBody: TokenRequestBody): Observable<TokenResult>
}

data class TokenResult(val Token: String) : BaseJsonResult()

data class TokenRequestBody(
        val UserName: String,
        val Password: String,
        val TimeSpan: String = (System.currentTimeMillis() / 1000).toString()
)

class TokenRepertory {

    private var token by PreferenceUtils(UserConstant.ACCOUNT_TOKEN, "")
    private val userName by PreferenceUtils(UserConstant.ACCOUNT_USERNAME, "")
    private val password by PreferenceUtils(UserConstant.ACCOUNT_PASSWORD, "")

    @SuppressLint("CheckResult")
    fun getToken(): Observable<*> {
        return createService(TokenApi::class.java)
                .getToken(TokenRequestBody(userName, EncryptUtils.encryptMD5ToString(password)))
                .flatMap {
                    if (it.Token.isNotEmpty()) {
                        Log.d("get_token", "重新获取到 token ${it.Token}")
                        token = it.Token
                        Observable.just(true)
                    } else
                        Observable.error(RequestTokenNotExistException("token 获取不到"))
                }
    }

}