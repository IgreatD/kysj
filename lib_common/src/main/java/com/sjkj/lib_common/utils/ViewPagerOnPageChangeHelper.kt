package com.sjkj.lib_common.utils

import android.support.v4.view.ViewPager

/**
 * @author by dingl on 2018/3/31.
 * @desc ViewPagerOnPageChangeHelper
 */
open class ViewPagerOnPageChangeHelper : ViewPager.OnPageChangeListener {
    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
    }
}