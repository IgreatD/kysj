package com.sjkj.lib_common.utils

import android.content.Context
import android.content.SharedPreferences
import com.sjkj.lib_common.common.Constant
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * @author by dingl on 2018/3/2.
 * @desc PreferenceUtils
 */
@Suppress("UNCHECKED_CAST")
class PreferenceUtils<T>(private val name: String, private val defaultValue: T) : ReadWriteProperty<Any?, T> {

    companion object {

        lateinit var preferences: SharedPreferences

        fun setContext(context: Context) {
            preferences = context.getSharedPreferences(
                    context.packageName + Constant.SHARED_NAME,
                    Context.MODE_PRIVATE
            )
        }

        fun clear() {
            preferences.edit().clear().apply()
        }
    }

    override fun getValue(thisRef: Any?, property: KProperty<*>): T = findPreference(name, defaultValue)

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        with(preferences.edit()) {
            when (value) {
                is Long -> putLong(name, value)
                is String -> putString(name, value)
                is Int -> putInt(name, value)
                is Boolean -> putBoolean(name, value)
                is Float -> putFloat(name, value)
                else -> throw IllegalArgumentException("This type can be saved into Preferences")
            }.apply()
        }
    }

    private fun <U> findPreference(name: String, default: U): U = with(preferences) {
        val res: Any = when (default) {
            is Long -> getLong(name, default)
            is String -> getString(name, default)
            is Int -> getInt(name, default)
            is Boolean -> getBoolean(name, default)
            is Float -> getFloat(name, default)
            else -> throw IllegalArgumentException("This type can be saved into Preferences")
        }
        res as U
    }
}