package com.sjkj.lib_common.utils

import android.app.Notification
import android.app.NotificationManager
import android.content.Context
import android.graphics.BitmapFactory
import android.support.v4.app.NotificationCompat
import com.sjkj.lib_common.R
import com.sjkj.lib_common.extensions.getString

/**
 * @author by dingl on 2018/3/15.
 * @desc NotificationUtils
 */
object NotificationUtils {

    private var nm: NotificationManager? = null

    private var builder: NotificationCompat.Builder? = null

    fun updateNotify(context: Context, channedid: String) {
        nm = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        builder = NotificationCompat.Builder(context, channedid)
                .setDefaults(Notification.DEFAULT_ALL)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("下载进度：0%")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setSmallIcon(R.drawable.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.drawable.ic_launcher))
                .setAutoCancel(true)
                .setProgress(100, 0, false)
                .setOngoing(true)
        nm?.notify(channedid, 0, builder?.build())
    }

    fun updateProgressNotify(percent: Int, channedid: String) {
        builder?.setProgress(100, percent, false)
        builder?.setContentText("下载进度：$percent%")
        builder?.setDefaults(Notification.FLAG_ONLY_ALERT_ONCE)
        nm?.notify(channedid, 0, builder?.build())
    }

    fun cancleNotify(channedid: String) {
        nm?.cancel(channedid, 0)
    }
}