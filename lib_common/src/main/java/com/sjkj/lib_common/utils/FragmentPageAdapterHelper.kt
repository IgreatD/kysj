package com.sjkj.lib_common.utils

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatActivity

/**
 * @author by dingl on 2018/3/21.
 * @desc FragmentPageAdapterHelper
 */
abstract class FragmentPageAdapterHelper(activity: AppCompatActivity,
                                         private val dataList: Array<String>)
    : FragmentPagerAdapter(activity.supportFragmentManager) {
    override fun getItem(position: Int): Fragment = getFragment(position)

    abstract fun getFragment(position: Int): Fragment

    override fun getCount(): Int = dataList.size

    override fun getPageTitle(position: Int): CharSequence? {
        return dataList[position]
    }

}

