package com.sjkj.lib_common.extensions

/**
 * @author by dingl on 2018/3/28.
 * @desc StringExt
 */
fun String.replace() =
        replace("\\r\\n", "\n")
