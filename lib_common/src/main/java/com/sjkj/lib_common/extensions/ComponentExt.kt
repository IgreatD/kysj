package com.sjkj.lib_common.extensions

import com.sjkj.lib_common.base.BaseRxActivity
import com.sjkj.lib_common.base.BaseRxFragment
import com.sjkj.lib_common.di.*
import org.jetbrains.anko.support.v4.act


/**
 * @author by dingl on 2018/3/8.
 * @desc ComponentExt
 */
fun BaseRxActivity.injectBaseComponet(): BaseComponent =
        DaggerBaseComponent.builder()
                .appModule(AppModule(application))
                .activityModule(ActivityModule(this))
                .lifecycleProviderModule(LifecycleProviderModule(this))
                .build()

fun BaseRxFragment.injectBaseComponet(): BaseComponent =
        DaggerBaseComponent.builder()
                .appModule(AppModule(act.application))
                .activityModule(ActivityModule(act))
                .lifecycleProviderModule(LifecycleProviderModule(this))
                .build()
