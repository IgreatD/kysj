package com.sjkj.lib_common.extensions

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.view.View
import com.blankj.utilcode.util.Utils.getApp
import com.orhanobut.logger.Logger

/**
 * @author by dingl on 2018/1/15.
 * @desc ContextExtensions
 */

fun getString(resId: Int): String = getApp().getString(resId)

fun getColorWithExtension(resId: Int) = ContextCompat.getColor(getApp(), resId)

fun getStringArray(resId: Int): Array<String> = getApp().resources.getStringArray(resId)

fun View.setVisible(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}

fun SwipeRefreshLayout.dismiss() {
    postDelayed({ isRefreshing = false }, 500)
}

fun Context.startCameraActivity() {
    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE_SECURE)
    if (packageManager.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY) != null) {
        try {
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            Logger.e("activity not found")
        }
    }
}
