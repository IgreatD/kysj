package com.sjkj.lib_common.data.repertory

import android.annotation.SuppressLint
import com.blankj.utilcode.util.Utils.getApp
import com.google.gson.Gson
import com.sjkj.lib_common.data.CitysBean
import com.sjkj.lib_common.data.api.OptionApi
import com.sjkj.lib_common.http.createService
import com.sjkj.lib_common.http.getCacheProviders
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.rx_cache2.DynamicKey
import javax.inject.Inject

/**
 * @author by dingl on 2018/3/30.
 * @desc OptionRepertory
 */
class OptionRepertory @Inject constructor() {

    fun getSubjectData() =
            getCacheProviders().getDataWithCache(
                    createService(OptionApi::class.java).getSubjectData(), DynamicKey("subject"))

    fun getQueryData() =
            getCacheProviders().getDataWithCache(
                    createService(OptionApi::class.java).getQueryData(), DynamicKey("query"))

    @SuppressLint("CheckResult")
    fun getLocationData() =
            getCacheProviders().getDataWithCache(
                    Observable.just("citys.json")
                            .flatMap {
                                Observable.just(
                                        Gson().fromJson(getApp().assets.open(it)
                                                .use {
                                                    it.bufferedReader()
                                                            .readLines()
                                                            .joinToString("")
                                                }, CitysBean::class.java))
                            }
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread()), DynamicKey("location"))

}