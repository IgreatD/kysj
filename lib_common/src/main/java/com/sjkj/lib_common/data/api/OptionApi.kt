package com.sjkj.lib_common.data.api

import com.sjkj.lib_common.data.QueryResult
import com.sjkj.lib_common.data.SubjectResult
import io.reactivex.Observable
import retrofit2.http.POST

/**
 * @author by dingl on 2018/3/30.
 * @desc OptionApi
 */

interface OptionApi {
    @POST("CommonService.svc/com_GetCourse")
    fun getSubjectData(): Observable<SubjectResult>

    @POST("CommonService.svc/GetQueryItem")
    fun getQueryData(): Observable<QueryResult>
}