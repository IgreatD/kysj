package com.sjkj.lib_common.data

import com.sjkj.lib_common.base.BaseJsonResult

/**
 * @author by dingl on 2018/3/27.
 * @desc CitysBean
 */
data class CitysBean(
        val Province: List<ProvinceBean>
) : BaseJsonResult()

data class ProvinceBean(
        val CityID: String,
        val ParentID: String,
        val CityName: String,
        val City: List<CityBean>
)

data class CityBean(
        val CityID: String,
        val ParentID: String,
        val CityName: String,
        val County: List<CountyBean>
)

data class CountyBean(
        val CityID: String,
        val ParentID: String,
        val CityName: String
)