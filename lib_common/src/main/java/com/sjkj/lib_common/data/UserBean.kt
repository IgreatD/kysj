package com.sjkj.lib_common.data

import android.content.Context
import com.alibaba.android.arouter.facade.annotation.Route
import com.alibaba.android.arouter.facade.service.SerializationService
import com.google.gson.Gson
import com.sjkj.lib_common.annotation.Poko
import com.sjkj.lib_common.base.BaseJsonResult
import java.lang.reflect.Type

/**
 * @author by dingl on 2018/3/26.
 * @desc UserBean
 */
data class LoginResult(val LoginInfoDto: List<UserBean>?, val Token: String) : BaseJsonResult()

@Route(path = "/service/json")
@Poko
data class UserBean(
        val Email: String,
        val QQ: String,
        var token: String,
        var PassWord: String,
        val AllowableSpeakNumber: Int,
        val AuditionLength: Long,
        val Balance: Float,
        val Cent: Int,
        val Center: Int,
        val City: String,
        val CityID: Int,
        val ClassWork: Int,
        val ComsumeAmount: Float,
        val Course: Int,
        val District: String,
        val DistrictID: Int,
        val EffectiveLength: Int,
        val Grade: String?,
        val GradeID: Int,
        var HeadImage: String,
        val IncomeAmount: Float,
        val IsPerfect: Int,
        val LoginTimes: Int,
        val MemberLevel: String,
        val MemberType: String?,
        val Message: String,
        val MyClass: Int,
        val MyInvitationCode: Int,
        val MyRefererUserName: String,
        val Name: String,
        val PrevLoginDate: String,
        val Province: String,
        val ProvinceID: Int,
        val Question: Int,
        val RechargeAmount: Float,
        val School: String,
        val SchoolID: Int,
        val Stage: String,
        val StageID: Int,
        val Status: Int,
        val SubAccountSid: String,
        val SubToken: String,
        val TutorFee: Double,
        val VoipAccount: String,
        val VoipPwd: String,
        val Wealth: String,
        val cityVersion: Int,
        val effect: Int,
        val exchangeRate: Int,
        var AccountID: Int,
        val UserName: String) : SerializationService {
    override fun <T : Any?> json2Object(input: String?, clazz: Class<T>?): T {
        return Gson().fromJson(input, clazz)
    }

    override fun init(context: Context?) {

    }

    override fun object2Json(instance: Any?): String {
        return Gson().toJson(instance)
    }

    override fun <T : Any?> parseObject(input: String?, clazz: Type?): T {
        return Gson().fromJson(input, clazz)
    }
}