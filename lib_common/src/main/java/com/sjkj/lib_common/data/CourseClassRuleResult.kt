package com.sjkj.lib_common.data

import com.sjkj.lib_common.base.BaseJsonResult

/**
 * @author by dingl on 2018/3/28.
 * @desc CourseClassRuleResult
 */
data class CourseClassRuleResult(val GetSysRulesInfoDto: List<CourseClassRuleBean>) : BaseJsonResult()

data class CourseClassRuleBean(
        val QuitRule: String,
        val TeachingRule: String,
        val TransferRule: String
)