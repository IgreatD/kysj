package com.sjkj.lib_common.data

import com.sjkj.lib_common.base.BaseJsonResult


/**
 * @author by dingl on 2018/3/23.
 * @desc SubjectBean
 */
data class SubjectResult(val GetCourseInfoDto: List<SubjectBean>) : BaseJsonResult()

data class SubjectBean(
        val CourseInfoID: Int,
        val Name: String
)

data class QueryResult(val GetQueryItemInfoDto: List<QueryBean>) : BaseJsonResult()

data class QueryBean(
        val QueryItemID: Int,
        val QueryItemName: String
)