package com.sjkj.lib_common.data

import com.sjkj.lib_common.base.BaseJsonResult
import com.sjkj.lib_common.common.HttpStore
import com.sjkj.lib_common.utils.SerializationServiceImpl
import java.text.SimpleDateFormat
import java.util.*

/**
 * @author by dingl on 2018/3/28.
 * @desc CourseClassBean
 */
data class CourseClassSquareResult(val ClassListInfoDto: List<CourseClassBean>) : BaseJsonResult()

data class CourseClassBean(
        val EndDate: String,
        val Name: String,
        private val CoverPath: String,
        var Knowledge: String,
        val Teacher: String,
        val CourseInfoName: String,
        val Province: String,
        val City: String,
        val District: String,
        val SignInNumber: Int,
        private val BeginDate: String?,
        val GoodEvaluateCount: Int,
        val EvaluateAvgCent: String,
        val LessonNumber: Int,
        val LessonPrice: Float,
        val ClassID: Int,
        val Describe: String
) : SerializationServiceImpl() {

    fun getSquareImg() = HttpStore.Base_Img_Url + CoverPath

    fun getClassBeginDate(): String = if (BeginDate.isNullOrEmpty())
        ""
    else
        try {
            val format = SimpleDateFormat("yyyy-MM-dd", Locale.SIMPLIFIED_CHINESE)
            format.format(format.parse(BeginDate))
        } catch (e: Exception) {
            BeginDate!!

        }
}