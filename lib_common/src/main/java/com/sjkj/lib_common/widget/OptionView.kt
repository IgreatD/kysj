package com.sjkj.lib_common.widget

import android.content.Context
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import com.blankj.utilcode.util.ScreenUtils
import com.sjkj.lib_common.R
import org.jetbrains.anko.sdk25.coroutines.onClick


/**
 * Created by dongjunkun on 2015/6/17.
 */
class OptionView : LinearLayout {

    private val tabMenuView by lazy { LinearLayout(context) }
    private val containerView by lazy { FrameLayout(context) }
    private val popupMenuViews by lazy { FrameLayout(context) }
    private val maskView by lazy { View(context) }
    private var currentPosition = -1

    private var dividerColor = -0x333334
    private var textSelectedColor = ContextCompat.getColor(context, R.color.colorPrimary)
    private var textUnselectedColor = ContextCompat.getColor(context, R.color.qmui_config_color_60_pure_black)
    private var maskColor = ContextCompat.getColor(context, R.color.qmui_config_color_gray_5)
    private var menuTextSize = 14

    private var menuSelectedIcon: Int = R.drawable.ic_expand_less
    private var menuUnselectedIcon: Int = R.drawable.ic_expand_black

    private var menuHeighPercent = 0.5f

    constructor(context: Context) : super(context, null)
    @JvmOverloads constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int = 0) : super(context, attrs, defStyleAttr) {

        orientation = LinearLayout.VERTICAL

        var menuBackgroundColor = ContextCompat.getColor(context, R.color.qmui_config_color_white)
        var underlineColor = -0x333334
        val a = context.obtainStyledAttributes(attrs, R.styleable.OptionView)
        underlineColor = a.getColor(R.styleable.OptionView_underlineColor, underlineColor)
        dividerColor = a.getColor(R.styleable.OptionView_dividerColor, dividerColor)
        textSelectedColor = a.getColor(R.styleable.OptionView_textSelectedColor, textSelectedColor)
        textUnselectedColor = a.getColor(R.styleable.OptionView_textUnselectedColor, textUnselectedColor)
        menuBackgroundColor = a.getColor(R.styleable.OptionView_menuBackgroundColor, menuBackgroundColor)
        maskColor = a.getColor(R.styleable.OptionView_maskColor, maskColor)
        menuTextSize = a.getDimensionPixelSize(R.styleable.OptionView_menuTextSize, menuTextSize)
        menuSelectedIcon = a.getResourceId(R.styleable.OptionView_menuSelectedIcon, menuSelectedIcon)
        menuUnselectedIcon = a.getResourceId(R.styleable.OptionView_menuUnselectedIcon, menuUnselectedIcon)
        menuHeighPercent = a.getFloat(R.styleable.OptionView_menuMenuHeightPercent, menuHeighPercent)
        a.recycle()

        val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        tabMenuView.orientation = LinearLayout.HORIZONTAL
        tabMenuView.setBackgroundColor(menuBackgroundColor)
        tabMenuView.layoutParams = params
        addView(tabMenuView, 0)

        val underLine = View(getContext())
        underLine.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dpTpPx(1.0f))
        underLine.setBackgroundColor(underlineColor)
        addView(underLine, 1)

        containerView.layoutParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)
        addView(containerView, 2)

    }

    fun setOptionView(tabTexts: Array<String>, popupViews: Array<View>, contentView: View) {

        if (tabTexts.size != popupViews.size) {
            throw IllegalArgumentException("params not match, tabTexts.size() should be equal popupViews.size()")
        }

        for (i in tabTexts.indices) {
            addTab(tabTexts, i)
        }
        containerView.addView(contentView, 0)
        containerView.setBackgroundColor(-0x000)
        maskView.layoutParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)
        maskView.setBackgroundColor(maskColor)
        maskView.onClick { closeMenu() }
        containerView.addView(maskView, 1)
        maskView.visibility = View.GONE
        if (containerView.getChildAt(2) != null) {
            containerView.removeViewAt(2)
        }

        val layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (ScreenUtils.getScreenHeight() * menuHeighPercent).toInt())
        popupMenuViews.layoutParams = layoutParams
        popupMenuViews.visibility = View.GONE
        containerView.addView(popupMenuViews, 2)

        for (i in popupViews.indices) {
            popupViews[i].layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            popupMenuViews.addView(popupViews[i], i)
        }

    }

    private fun addTab(tabTexts: Array<String>, i: Int) {
        val tab = TextView(context)
        tab.setSingleLine()
        tab.ellipsize = TextUtils.TruncateAt.END
        tab.gravity = Gravity.CENTER
        tab.setTextSize(TypedValue.COMPLEX_UNIT_SP, menuTextSize.toFloat())
        tab.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        tab.setTextColor(textUnselectedColor)
        tab.setCompoundDrawablesWithIntrinsicBounds(null, null, resources.getDrawable(menuUnselectedIcon, context.theme), null)
        tab.text = tabTexts[i]
        tab.setPadding(dpTpPx(10f), dpTpPx(5f), dpTpPx(5f), dpTpPx(10f))
        //添加点击事件
        tab.onClick { switchMenu(tab) }
        tabMenuView.addView(tab)
        //添加分割线
        if (i < tabTexts.size - 1) {
            val view = View(context)
            view.layoutParams = LinearLayout.LayoutParams(dpTpPx(0.5f), ViewGroup.LayoutParams.MATCH_PARENT)
            view.setBackgroundColor(dividerColor)
            tabMenuView.addView(view)
        }
    }

    private fun closeMenu() {
        if (currentPosition != -1) {
            (tabMenuView.getChildAt(currentPosition) as TextView).setTextColor(textUnselectedColor)
            (tabMenuView.getChildAt(currentPosition) as TextView).setCompoundDrawablesWithIntrinsicBounds(null, null,
                    resources.getDrawable(menuUnselectedIcon, context.theme), null)
            popupMenuViews.visibility = View.GONE
            popupMenuViews.animation = AnimationUtils.loadAnimation(context, R.anim.menu_out)
            maskView.visibility = View.GONE
            maskView.animation = AnimationUtils.loadAnimation(context, R.anim.mask_out)
            currentPosition = -1
        }

    }

    fun setOnMenuCheckListener(onMenuCheckListener: OnMenuCheckListener) {
        this.onMenuCheckListener = onMenuCheckListener
    }

    private var onMenuCheckListener: OnMenuCheckListener? = null

    interface OnMenuCheckListener {
        fun onMenuCheckListener(currentPosition: Int)
    }

    private fun switchMenu(target: View) {
        println(currentPosition)
        var i = 0
        while (i < tabMenuView.childCount) {
            if (target === tabMenuView.getChildAt(i)) {
                if (currentPosition == i) {
                    closeMenu()
                } else {
                    if (currentPosition == -1) {
                        popupMenuViews.visibility = View.VISIBLE
                        popupMenuViews.animation = AnimationUtils.loadAnimation(context, R.anim.menu_in)
                        maskView.visibility = View.VISIBLE
                        maskView.animation = AnimationUtils.loadAnimation(context, R.anim.mask_in)
                        popupMenuViews.getChildAt(i / 2).visibility = View.VISIBLE
                    } else {
                        popupMenuViews.getChildAt(i / 2).visibility = View.VISIBLE
                    }
                    currentPosition = i
                    (tabMenuView.getChildAt(i) as TextView).setTextColor(textSelectedColor)
                    (tabMenuView.getChildAt(i) as TextView).setCompoundDrawablesWithIntrinsicBounds(null, null,
                            resources.getDrawable(menuSelectedIcon, context.theme), null)
                    onMenuCheckListener?.onMenuCheckListener(currentPosition / 2)
                }
            } else {
                (tabMenuView.getChildAt(i) as TextView).setTextColor(textUnselectedColor)
                (tabMenuView.getChildAt(i) as TextView).setCompoundDrawablesWithIntrinsicBounds(null, null,
                        resources.getDrawable(menuUnselectedIcon, context.theme), null)
                popupMenuViews.getChildAt(i / 2).visibility = View.GONE
            }
            i += 2
        }
    }

    private fun dpTpPx(value: Float): Int {
        val dm = resources.displayMetrics
        return (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, dm) + 0.5).toInt()
    }
}