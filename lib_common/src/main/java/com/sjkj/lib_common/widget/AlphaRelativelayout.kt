package com.sjkj.lib_common.widget

import android.content.Context
import android.util.AttributeSet
import com.qmuiteam.qmui.alpha.QMUIAlphaViewHelper
import com.zhy.autolayout.AutoRelativeLayout

/**
 * 在 pressed 和 disabled 时改变 View 的透明度
 */
class AlphaRelativelayout : AutoRelativeLayout {

    private var mAlphaViewHelper: QMUIAlphaViewHelper = QMUIAlphaViewHelper(this)

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun setPressed(pressed: Boolean) {
        super.setPressed(pressed)
        mAlphaViewHelper.onPressedChanged(this, pressed)
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        mAlphaViewHelper.onEnabledChanged(this, enabled)
    }

}
