package com.sjkj.lib_common.di

import android.app.Activity
import android.app.Application
import com.trello.rxlifecycle2.LifecycleProvider
import dagger.Component
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * @author by dingl on 2018/3/8.
 * @desc Base
 */
@Singleton
@Component(modules = [
    (ActivityModule::class),
    (LifecycleProviderModule::class),
    (AppModule::class)])
interface BaseComponent {

    fun app(): Application

    fun activity(): Activity

    fun lifecycleProvider(): LifecycleProvider<*>
}

@Module
class AppModule(private val app: Application) {
    @Singleton
    @Provides
    fun provideApp(): Application = app
}

@Module
class ActivityModule(private val activity: Activity) {

    @Singleton
    @Provides
    fun provideActivity(): Activity = activity

}

@Module
class LifecycleProviderModule(private val lifecycleProvider: LifecycleProvider<*>) {
    @Singleton
    @Provides
    fun provideLifecycleProvider() = lifecycleProvider
}
