package com.sjkj.lib_common.di.scope

import javax.inject.Scope

/**
 * @author by dingl on 2018/1/18.
 * @desc PerRepertory
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerApplication

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerFragment

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerRepertory

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerUser
