package com.sjkj.lib_common.common

import com.sjkj.lib_common.BuildConfig

/**
 * @author by dingl on 2018/3/8.
 * @desc HttpStore
 */
object HttpStore {
    var Ping_Url = if (BuildConfig.DEBUG) "60.205.169.15" else "60.205.169.15"
    var Base_Url = "http://$Ping_Url:81/"
    var Base_Img_Url = "http://$Ping_Url:82/"
}