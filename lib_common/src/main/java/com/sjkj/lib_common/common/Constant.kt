package com.sjkj.lib_common.common

/**
 * @author by dingl on 2018/3/8.
 * @desc Constant
 */
object Constant {
    const val CONNECT_TIMEOUT_IN_MS: Long = 30L
    const val READ_TIMEOUT: Long = 10L
    const val SHARED_NAME = "shared_name"
    const val WELCOME_FIRST: String = "welcome_first"
    const val PAGE_SIZE = 15
    const val AUTHORITY = "com.sjkj.kysj.fileProvider"
    const val SPAN_COUNT = 3
}

