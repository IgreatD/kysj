package com.sjkj.lib_common.common

/**
 * @author by dingl on 2018/3/26.
 * @desc HotFixID
 */
object HotFixID {
    const val APP_ID = "24833802"
    const val APP_KEY = "b3efacf24a88f857728b56e0f14a217d"
    const val RES_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCyp47SdYqLvk3repCaPqKdteYrggrDqXYWJVdwKLLMqmqzjHnrXS15p9JcNXdhUZFsDIZi0FVxcF7yso85+SkIONDlJKh0Ib5UuUU6nFSz4sJ0ZEN5vDGyznW0p+g8Q0dXwgCJyq2fjckX2IjhcO/Y2JgYphUDdIO9uXVIzq7p/k5E25IC02Ula/6oVUfYd//VE/JBDjGh3MoSeUhxE/T7YDKu4lVR73wX3UraaPj77lp0y8Diq1mIhaO4ZjjNf8W+LlxchQFHmeunNhXYZHz0Z0l2knzBtqfZolRVUqGhxhG3/kyriUjYSsa+S3Cu+2IwG9L4HJA88zAtotfOGLMTAgMBAAECggEAExv9GOHduejXKNLpWLZOapQkVI8KW7dqnB/OZH7V8TJhS2dBOz2yOYsL4zUM2eoBpg8I6NTj7shYP5l93HOcQfbh0UDEi/d4BlPy5bwHC1678fsvhHvuzPNsf7BeFOQVY3uq1xJI1AI0ekp9CbAs14MDcc+B++agmNCXjPwelYSiFhySt/qb/flU8IhD0HUbFF+kLdYv8UEA7S3p3/o2SkHskyLLcQUuG7pML8+F+qwXV7cOiuImlldHBZS3kCGMVXiSfdntZsD/wY1QSi2VtXym76FnzFZTVHN38JEwRv5kIuGd4U7vcyZThyY10dR2v5PoYUVKg8S+QVkOBZ8a0QKBgQDtfKvJYqjmlBjGepNPy4joKMvmAIpJpaTQwakfg580UAWDar3XTR57ARoBrIeg7sigY9HmNDWLY+PQQqsmWaMiOxGB6OtD00+9Dxs7o72YRxjQ8GglVY5UfLq3HoVvre5HFIVnbEbY/vRiHJrFeX/zpxL6/oMk8LsZs8xMYvICTQKBgQDAlNEFvR0hIU8F3DPleOygRiWzF07jZ3pJSUJtOPmG4PTpXbqja9nfrVp6jJ8twUH9/8wfabSjQGRHHx3Z9ny44A7CHUDz19OfMw22EfaVGVE7Fp+EIadaDdkQr8kAR5f817QsyoOh80Y6Eb2iXqpf2mAgUCl1fANtGKYkoPB63wKBgQC8HD9aMgrFMVN6i4b58qdL9Vykn5LqCQnGzU8wBirp3eSwFrGuw4g8OEAoSUfZIqrJ2KdfJ8Wt1n6R7OzyYJgGOjGA7ETGIbAMedXBZM59uvseLP7ha6Zkp7Oh7swVDPkrVkbhB/QKV9CtkijJ+JZwIcu4AjdQ4FnjXvaZHtrBFQKBgAwSR/fgHs9Q0y8+2lHr/okdXIeZUmyWs+ecG/nUtG9r8m4eXfCYdhO7Pgyoe+HWV1igjB779UK2apK6tJhRBwhfow0v4D5orO8xuA5NmfNGnvzhMiaKJX9TluOg4SyPUEudCcI0/4NLSH+jVQu0e2F+wr/DxpWCrIROIzvwiqJpAoGBAJe/PEfWoGDhz19PYAj205nF5s9yLMRatURkGwT8PBCjRBqwpmXJbWYr5AR0NDIKzZIrbLV7au46As9VVzaoXgtvBfurhbik+NcgBNyy3cQ35ZO02D5lryCgTun3FC0CEGnzjwJ6SfEzj83mWrJBYcGVo9SAAdQbb7Nl3eHF+k+D"
}