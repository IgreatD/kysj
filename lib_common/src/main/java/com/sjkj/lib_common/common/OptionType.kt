package com.sjkj.lib_common.common

/**
 * @author by dingl on 2018/3/27.
 * @desc OptionType
 */
object OptionType {
    const val TYPE_COURSE = 0
    const val TYPE_KNOW = 1
    const val TYPE_LOCATION = 2
    const val TYPE_TIME = 3
    const val TYPE_SORT = 4
    const val TYPE_STATUS = 5
}