package com.sjkj.lib_common.common

/**
 * @author by dingl on 2018/3/9.
 * @desc UserConstant
 */
object UserConstant {

    const val ACCOUNT_ID = "account_id"
    const val ACCOUNT_USERNAME = "account_user_name"
    const val ACCOUNT_PASSWORD = "account_pass_word"
    const val ACCOUNT_TOKEN = "account_token"
    const val ACCOUNT_NAME = "account_name"
    const val ACCOUNT_STAGEID = "account_stageid"
    const val ACCOUNT_GRADEID = "account_gradeid"
    const val USER: String = "user"

}