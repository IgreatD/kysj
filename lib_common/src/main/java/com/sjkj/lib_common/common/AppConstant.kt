package com.sjkj.lib_common.common

import android.os.Environment

/**
 * @author by dingl on 2018/3/12.
 * @desc AppConstant
 */
object AppConstant {
    val DIR: String = Environment.getExternalStorageDirectory().absolutePath + "/kysj/"
}