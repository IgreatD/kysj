package com.sjkj.lib_common.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import com.blankj.utilcode.util.NetworkUtils
import com.sjkj.lib_common.common.HttpStore
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


/**
 * @author by dingl on 2018/3/29.
 * @desc NetWorkReceiver
 */
interface NetWorkListener {
    fun networkConnect()
}

class NetWorkReceiver : BroadcastReceiver() {

    private var mNetWorkListener: NetWorkListener? = null

    fun setNetworkConnectListener(netWorkListener: NetWorkListener) {
        mNetWorkListener = netWorkListener
    }

    override fun onReceive(context: Context, intent: Intent?) {
        intent ?: return
        if (ConnectivityManager.CONNECTIVITY_ACTION == intent.action) {
            doAsync {
                val availableByPing = NetworkUtils.isAvailableByPing(HttpStore.Ping_Url)
                uiThread {
                    if (availableByPing) {
                        mNetWorkListener?.networkConnect()
                    }
                }
            }
        }
    }
}