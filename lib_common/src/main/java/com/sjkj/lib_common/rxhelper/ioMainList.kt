package com.sjkj.lib_common.rxhelper

import com.sjkj.lib_common.base.BaseJsonResult
import com.sjkj.lib_common.base.BaseView
import com.sjkj.lib_common.exception.RequestFailedException
import com.sjkj.lib_common.exception.ServerException
import com.trello.rxlifecycle2.LifecycleProvider
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.rx_cache2.Reply
import java.io.IOException
import java.net.ConnectException
import java.net.SocketException

/**
 * @author by dingl on 2017/9/12.
 * *
 * @desc RxSchedulersHelper
 */
fun <T> Observable<T>.ioMain(lifecycleProvider: LifecycleProvider<*>? = null,
                             view: BaseView? = null,
                             onRequestSucess: ((T) -> Unit)) {
    ioMain(lifecycleProvider, view, onRequestSucess, {}, {})
}

fun <T> Observable<T>.ioMain(lifecycleProvider: LifecycleProvider<*>? = null,
                             view: BaseView? = null,
                             onRequestSucess: ((T) -> Unit),
                             onRequestFailed: ((Throwable) -> Unit)) {
    ioMain(lifecycleProvider, view, onRequestSucess, {}, onRequestFailed)
}

fun <T> Observable<T>.ioMain(lifecycleProvider: LifecycleProvider<*>? = null,
                             view: BaseView? = null,
                             onRequestSucess: ((T) -> Unit),
                             onRequestNullData: (() -> Unit),
                             onRequestFailed: ((Throwable) -> Unit)) {

    val dispos: DisposableObserver<T> = object : DisposableObserver<T>() {

        override fun onStart() {
            view?.showLoading()
        }

        override fun onComplete() {
            view?.hideLoading()
            if (isDisposed) {
                dispose()
            }
        }

        override fun onNext(t: T) {
            when (t) {
                null -> {
                    view?.showEmptyView()
                    onRequestNullData()
                }
                is Reply<*> -> {
                    when {
                        t.data == null -> view?.showLoadError()
                        t.data is BaseJsonResult -> {
                            val it = t.data as BaseJsonResult
                            when {
                                it.Code < 0 -> {
                                    onRequestFailed(RequestFailedException(it.Message))
                                    view?.showLoadError()
                                }
                                it.Code == 1 -> {
                                    view?.loadComplete()
                                    onRequestSucess(t)
                                }
                                it.Code == 2 -> {
                                    view?.loadEnd()
                                    view?.showEmptyView()
                                    onRequestNullData()
                                }
                                else -> {
                                    onRequestFailed(RequestFailedException(it.Message))
                                }
                            }
                        }
                        else -> view?.showLoadError()
                    }
                }
                is BaseJsonResult -> {
                    when {
                        t.Code < 0 -> {
                            onRequestFailed(RequestFailedException(t.Message))
                            view?.showLoadError()
                        }
                        t.Code == 1 -> {
                            onRequestSucess(t)
                        }
                        t.Code == 2 -> {
                            view?.loadEnd()
                            view?.showEmptyView()
                            onRequestNullData()
                        }
                        else -> {
                            view?.showLoadError()
                            onRequestFailed(RequestFailedException(t.Message))
                        }
                    }
                }
                else -> {
                    view?.showLoadError()
                }
            }
        }

        override fun onError(e: Throwable) {
            if (isDisposed) {
                dispose()
            }
            view?.hideLoading()
            view?.loadEnd()
            view?.showToast(e.message ?: "请求失败，请重试！")
            onRequestFailed(e)
            when (e) {
                is ConnectException, SocketException(), ServerException(), IOException() -> {
                    view?.showNetError()
                }
                else -> {
                    view?.showLoadError()
                }
            }
        }
    }
    retryWhen(CustomerRetryWhen())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .compose(lifecycleProvider?.bindToLifecycle())
            .subscribe(dispos)
}




