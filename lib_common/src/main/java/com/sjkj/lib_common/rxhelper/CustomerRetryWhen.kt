package com.sjkj.lib_common.rxhelper

import com.sjkj.lib_common.api.TokenRepertory
import com.sjkj.lib_common.exception.RequestTokenOutOfDateException
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.exceptions.CompositeException
import io.reactivex.functions.Function
import javax.inject.Singleton

/**
 * @author by dingl on 2017/11/1.
 */
@Singleton
class CustomerRetryWhen : Function<Observable<out Throwable>, ObservableSource<*>> {

    private val tokenRepertory by lazy { TokenRepertory() }

    override fun apply(t: Observable<out Throwable>): ObservableSource<*> {
        return t.flatMap { throwable ->
            when (throwable) {
                is CompositeException -> tokenRepertory.getToken()
                is RequestTokenOutOfDateException -> tokenRepertory.getToken()
                else -> Observable.error<Long>(throwable)
            }
        }
    }

}
