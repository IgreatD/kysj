package com.sjkj.lib_common.rxhelper

import com.orhanobut.logger.Logger
import com.sjkj.lib_common.base.BaseView
import com.sjkj.lib_common.exception.RequestFailedException
import com.sjkj.lib_common.exception.RequestNullDataException
import com.sjkj.lib_common.exception.RequestTokenOutOfDateException
import io.reactivex.observers.DisposableObserver
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

/**
 * @author by dingl on 2017/9/12.
 * *
 * @desc RxSubscriber
 */
abstract class RxSubscriber<T>(private val mBaseView: BaseView) : DisposableObserver<T>() {

    override fun onStart() {
        mBaseView.showLoading()
    }

    override fun onComplete() {
        mBaseView.hideLoading()
    }

    override fun onError(e: Throwable) {
        Logger.e(e.message)
        mBaseView.hideLoading()
        if (e.message?.contains("Parameter specified as non-null is null") == true) {
            _onEmpty()
            return
        }
        val msg: String
        when (e) {
            is RequestTokenOutOfDateException -> {
                msg = e.message.toString()
                _onError(msg)
                return
            }
            is RequestNullDataException -> {
                _onEmpty()
                return
            }
            is ConnectException -> {
                msg = "暂无网络，请连接网络后再试!"
                mBaseView.showNetError()
            }
            is RequestFailedException -> {
                msg = e.message as String
                mBaseView.showLoadError()
            }
            is UnknownHostException -> {
                msg = "暂无网络，请连接网络后再试!"
                mBaseView.showNetError()
            }
            is SocketTimeoutException -> {
                msg = "请求超时，请稍后重试..."
                mBaseView.showLoadError()
            }
            else -> {
                msg = "请求失败，请稍后重试..."
                mBaseView.showLoadError()
            }
        }
        _onError(msg)
    }

    open fun _onEmpty() {
        mBaseView.loadEnd()
        mBaseView.showEmptyView()
    }

    open fun _onError(toast: String) {
        mBaseView.showToast(toast)
        mBaseView.loadError()
    }

    override fun onNext(t: T) {
        mBaseView.loadComplete()
        _onNext(t)
    }

    abstract fun _onNext(t: T)

}
