package com.sjkj.lib_common.presenter

import com.sjkj.lib_common.base.BasePresenter
import com.sjkj.lib_common.base.BaseView
import com.sjkj.lib_common.common.OptionType
import com.sjkj.lib_common.data.CitysBean
import com.sjkj.lib_common.data.QueryBean
import com.sjkj.lib_common.data.SubjectBean
import com.sjkj.lib_common.data.repertory.OptionRepertory
import com.sjkj.lib_common.rxhelper.ioMain
import javax.inject.Inject

/**
 * @author by dingl on 2018/3/27.
 * @desc OptionPresenter
 */
interface OptionView : BaseView {
    fun getSubjectDataSuccess(subjectList: List<SubjectBean>)
    fun getDataFailed()
    fun getQueryDataSuccess(queryList: List<QueryBean>) {}
    fun getProvinceDataSuccess(citysBean: CitysBean) {}
}

class OptionPresenter @Inject constructor() : BasePresenter<OptionView>() {

    @Inject
    lateinit var optionRepertory: OptionRepertory

    fun getOptionData(optionType: List<Int>) {

        optionType.forEach {
            when (it) {
                OptionType.TYPE_COURSE -> optionRepertory.getSubjectData()
                        .ioMain(lifecycleProvider, mView, {
                            mView.getSubjectDataSuccess(it.data.GetCourseInfoDto)
                        }, {
                            mView.getDataFailed()
                        })
                OptionType.TYPE_LOCATION -> optionRepertory.getLocationData()
                        .ioMain(lifecycleProvider, mView, {
                            mView.getProvinceDataSuccess(it.data)
                        }, {
                            mView.getDataFailed()
                        })
                OptionType.TYPE_SORT -> optionRepertory.getQueryData()
                        .ioMain(lifecycleProvider, mView, {
                            mView.getQueryDataSuccess(it.data.GetQueryItemInfoDto)
                        }, {
                            mView.getDataFailed()
                        })
            }
        }
    }

}