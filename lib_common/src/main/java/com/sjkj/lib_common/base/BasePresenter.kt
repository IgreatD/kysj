package com.sjkj.lib_common.base

import android.app.Activity
import com.sjkj.lib_common.common.Constant
import com.trello.rxlifecycle2.LifecycleProvider
import javax.inject.Inject

/**
 * @author by dingl on 2018/1/15.
 * @desc BasePresenter
 */
open class BasePresenter<V : BaseView> {

    lateinit var mView: V

    @Inject
    lateinit var lifecycleProvider: LifecycleProvider<*>

    @Inject
    lateinit var activity: Activity

}

open class BaseRecyclePresenter<V : BaseRecycleView<*>> : BasePresenter<V>() {

    protected open var pageIndex = 1

    fun getCurrentPageIndex() = pageIndex

    fun setCurrentPageIndex(pageIndex: Int) {
        this.pageIndex = pageIndex
    }

    fun setSize(size: Int) {
        pageIndex++
        if (size < Constant.PAGE_SIZE) {
            mView.loadEnd()
        }
    }
}
