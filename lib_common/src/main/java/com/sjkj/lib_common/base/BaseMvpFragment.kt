package com.sjkj.lib_common.base

import android.os.Bundle
import android.view.View
import com.blankj.utilcode.util.ToastUtils
import com.qmuiteam.qmui.widget.dialog.QMUITipDialog
import com.sjkj.lib_common.R
import com.sjkj.lib_common.extensions.setVisible
import kotlinx.android.synthetic.main.base_fragment.*
import org.jetbrains.anko.support.v4.act
import javax.inject.Inject

/**
 * @author by dingl on 2018/3/8.
 * @desc BaseMvpFragment
 */
abstract class BaseMvpFragment<P : BasePresenter<*>> : BaseRxFragment(), BaseView {

    @Inject
    lateinit var mPresenter: P

    private val mLoingProgress by lazy { QMUITipDialog.Builder(act).setIconType(QMUITipDialog.Builder.ICON_TYPE_LOADING).create() }

    override fun getLayoutId() = R.layout.base_fragment

    override fun initView(savedInstanceState: Bundle?) {
        emptyView.setLoadingShowing(true)
        showViewVisible(true)
        container.addView(getContainerView())
    }

    abstract fun getContainerView(): View

    private fun showViewVisible(flag: Boolean) {
        emptyView.setVisible(flag)
        container.setVisible(!flag)
    }

    override fun showLoadError() {
        super.showLoadError()
        showViewVisible(true)
        emptyView.show(false, resources.getString(R.string.emptyView_mode_desc_fail_title), null, resources.getString(R.string.emptyView_mode_desc_retry), {
            requestDataFirst()
        })
    }

    override fun showNetError() {
        showViewVisible(true)
        emptyView.show(false, resources.getString(R.string.emptyView_mode_desc_fail_title), resources.getString(R.string.emptyView_mode_desc_fail_desc), resources.getString(R.string.emptyView_mode_desc_retry), {
            requestDataFirst()
        })
    }

    override fun showEmptyView() {
        showViewVisible(true)
        emptyView.show(resources.getString(R.string.null_data), null)
    }

    abstract fun requestDataFirst()

    override fun loadComplete() {
        showViewVisible(false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        injectComponent()
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onLazyInitView(savedInstanceState: Bundle?) {
        super.onLazyInitView(savedInstanceState)
        requestDataFirst()
    }

    abstract fun injectComponent()

    override fun showLoading() {
        /*if (mLoingProgress.isShowing.not()) {
            mLoingProgress.show()
        }*/
    }

    override fun hideLoading() {
        /*if (mLoingProgress.isShowing) {
            mLoingProgress.dismiss()
        }*/
    }

    override fun showToast(toast: String) {
        ToastUtils.showShort(toast)
    }

}