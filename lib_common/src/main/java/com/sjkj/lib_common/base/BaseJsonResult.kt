package com.sjkj.lib_common.base

/**
 * @author by dingl on 2018/3/8.
 * @desc BaseJsonResult
 */
open class BaseJsonResult {
    val Code: Int = -1
    val Message: String = ""
}
