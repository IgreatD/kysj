package com.sjkj.lib_common.base

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import com.alibaba.android.arouter.launcher.ARouter
import com.blankj.utilcode.util.AppUtils
import com.blankj.utilcode.util.Utils
import com.liulishuo.filedownloader.FileDownloader
import com.liulishuo.filedownloader.connection.FileDownloadUrlConnection
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.orhanobut.logger.PrettyFormatStrategy
import com.sjkj.lib_common.common.HotFixID
import com.sjkj.lib_common.utils.PreferenceUtils
import com.taobao.sophix.PatchStatus
import com.taobao.sophix.SophixManager
import org.litepal.LitePal
import java.net.Proxy


/**
 * @author by dingl on 2018/1/15.
 * @desc BaseApp
 */
open class BaseApplication : Application() {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)

        val appVersion = try {
            this.packageManager.getPackageInfo(this.packageName, 0).versionName
        } catch (e: Exception) {
            "1.0.0"
        }

        SophixManager.getInstance()
                .setContext(this)
                .setAppVersion(appVersion)
                .setAesKey(null)
                .setEnableDebug(true)
                .setSecretMetaData(HotFixID.APP_ID, HotFixID.APP_KEY, HotFixID.RES_KEY)
                .setPatchLoadStatusStub { mode, code, info, handlePatchVersion ->
                    // 补丁加载回调通知
                    when (code) {
                        PatchStatus.CODE_LOAD_SUCCESS -> {
                            // 表明补丁加载成功
                        }
                        PatchStatus.CODE_LOAD_RELAUNCH -> {
                            // 表明新补丁生效需要重启. 开发者可提示用户或者强制重启;
                            // 建议: 用户可以监听进入后台事件, 然后调用killProcessSafely自杀，以此加快应用补丁，详见1.3.2.3
                        }
                        else -> {
                            // 其它错误信息, 查看PatchStatus类说明
                        }
                    }
                }.initialize()

        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()

        initUtils()

        if (AppUtils.isAppDebug()) {
            ARouter.openDebug()
            ARouter.openLog()
        }
        ARouter.init(this)

        initLogger()
        initLitePal()

        initFileDownload()
        PreferenceUtils.setContext(applicationContext)

    }

    private fun initFileDownload() {
        FileDownloader.setupOnApplicationOnCreate(this)
                .connectionCreator(FileDownloadUrlConnection.Creator(FileDownloadUrlConnection.Configuration()
                        .connectTimeout(15000) // set connection timeout.
                        .readTimeout(15000) // set read timeout.
                        .proxy(Proxy.NO_PROXY) // set proxy
                ))
                .commit()
    }

    private fun initUtils() {
        Utils.init(this)
    }

    private fun initLitePal() {
        LitePal.initialize(this)
    }

    private fun initLogger() {
        val formatStrategy = PrettyFormatStrategy.newBuilder()
                .showThreadInfo(false)
                .methodCount(2)
                .methodOffset(2)
                .tag("sjkj_logger")
                .build()

        Logger.addLogAdapter(AndroidLogAdapter(formatStrategy))
    }

}
