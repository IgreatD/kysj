package com.sjkj.lib_common.base

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.blankj.utilcode.util.ToastUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.qmuiteam.qmui.widget.QMUIEmptyView
import com.qmuiteam.qmui.widget.dialog.QMUITipDialog
import com.sjkj.lib_common.R
import com.sjkj.lib_common.extensions.dismiss
import org.jetbrains.anko.support.v4.act
import org.jetbrains.anko.support.v4.onRefresh
import javax.inject.Inject

/**
 * @author by dingl on 2018/3/8.
 * @desc BaseListActivity
 */
abstract class BaseListFragment<A : BaseQuickAdapter<*, BaseViewHolder>, P : BasePresenter<*>>
    : BaseRxFragment(), BaseQuickAdapter.RequestLoadMoreListener, BaseView {

    protected open lateinit var mRefresh: SwipeRefreshLayout

    protected open lateinit var mRecyclerView: RecyclerView

    protected open lateinit var mAdapter: A

    protected open lateinit var mView: View

    @Inject
    lateinit var mPresenter: P

    private val mEmptyView by lazy { QMUIEmptyView(context) }

    private val mLoingProgress by lazy { QMUITipDialog.Builder(act).setIconType(QMUITipDialog.Builder.ICON_TYPE_LOADING).create() }

    override fun showLoadError() {
        mRefresh.dismiss()
        mAdapter.loadMoreFail()
        mEmptyView.show(false, resources.getString(R.string.emptyView_mode_desc_fail_title), null, resources.getString(R.string.emptyView_mode_desc_retry), {
            showEmptyLoadingView()
            requestData()
        })
    }

    override fun showNetError() {
        mRefresh.dismiss()
        mAdapter.loadMoreFail()
        mEmptyView.show(false, resources.getString(R.string.emptyView_mode_desc_fail_title), resources.getString(R.string.emptyView_mode_desc_fail_desc), resources.getString(R.string.emptyView_mode_desc_retry), {
            showEmptyLoadingView()
            requestData()
        })
    }

    override fun networkConnect() {

    }

    override fun loadEnd() {
        mRefresh.dismiss()
        mAdapter.loadMoreEnd()
    }

    override fun loadError() {
        mRefresh.dismiss()
        mAdapter.loadMoreFail()
    }

    override fun loadComplete() {
        mRefresh.dismiss()
        mAdapter.loadMoreComplete()
    }

    override fun showEmptyView() {
        mEmptyView.show(resources.getString(R.string.null_data), null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        injectComponent()
        super.onViewCreated(view, savedInstanceState)
        mView = view
        mAdapter = getContentAdapter()
        initRecycleAndRefresh()
        checkObject()
        setupAdapter()
        setupRefresh()
        setupRecyclerView()
    }

    abstract fun injectComponent()

    abstract fun initRecycleAndRefresh()

    override fun onLazyInitView(savedInstanceState: Bundle?) {
        super.onLazyInitView(savedInstanceState)
        requestData()
    }

    private fun setupRecyclerView() {
        with(mRecyclerView) {
            adapter = mAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }

    private fun setupRefresh() {
        with(mRefresh) {
            setColorSchemeResources(R.color.colorPrimary)
            onRefresh {
                requestData()
            }
        }
    }

    private fun requestData() {
        mRefresh.isRefreshing = true
        requestDataFirst()
    }

    abstract fun requestDataFirst()

    private fun checkObject() {
        checkNotNull(mRefresh) { "please set SwipeRefreshLayout first in xml" }
        checkNotNull(mRecyclerView) { "please set RecyclerView first in xml" }
        checkNotNull(mAdapter) { "please set BaseQuickAdapter first in xml" }
    }

    private fun setupAdapter() {
        with(mAdapter) {
            bindToRecyclerView(mRecyclerView)
            disableLoadMoreIfNotFullPage()
            setHeaderAndEmpty(true)
            showEmptyLoadingView()
            emptyView = mEmptyView
            setOnLoadMoreListener(this@BaseListFragment, mRecyclerView)
        }
    }

    fun showEmptyLoadingView() {
        mEmptyView.show(true)
    }

    abstract fun getContentAdapter(): A

    override fun onLoadMoreRequested() {
        requestDataMore()
    }

    protected open fun requestDataMore() {}

    override fun showLoading() {
        /*if (mLoingProgress.isShowing.not()) {
            mLoingProgress.show()
        }*/
    }

    override fun hideLoading() {
        /* if (mLoingProgress.isShowing) {
             mLoingProgress.dismiss()
         }*/
    }

    override fun showToast(toast: String) {
        ToastUtils.showShort(toast)
    }


}
