package com.sjkj.lib_common.base

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.blankj.utilcode.util.ToastUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.qmuiteam.qmui.widget.QMUIEmptyView
import com.sjkj.lib_common.R
import com.sjkj.lib_common.extensions.dismiss
import org.jetbrains.anko.support.v4.onRefresh

/**
 * @author by dingl on 2018/3/8.
 * @desc BaseListActivity
 */
abstract class BaseListActivity<A : BaseQuickAdapter<*, BaseViewHolder>, P : BasePresenter<*>> :
        BaseMvpActivity<P>(), BaseQuickAdapter.RequestLoadMoreListener {

    protected open lateinit var mRefresh: SwipeRefreshLayout

    protected open lateinit var mRecyclerView: RecyclerView

    protected open lateinit var mAdapter: A

    private val mEmptyView by lazy { QMUIEmptyView(this) }

    override fun showToast(toast: String) {
        ToastUtils.showShort(toast)
    }

    override fun showLoading() {
    }

    override fun hideLoading() {
    }

    override fun showLoadError() {
        mRefresh.dismiss()
        mAdapter.loadMoreFail()
        mEmptyView.show(false, resources.getString(R.string.emptyView_mode_desc_fail_title), null, resources.getString(R.string.emptyView_mode_desc_retry), {
            requestDataFirst()
        })
    }

    override fun showNetError() {
        mRefresh.dismiss()
        mAdapter.loadMoreFail()
        mEmptyView.show(false, resources.getString(R.string.emptyView_mode_desc_fail_title), resources.getString(R.string.emptyView_mode_desc_fail_desc), resources.getString(R.string.emptyView_mode_desc_retry), {
            requestDataFirst()
        })
    }

    override fun loadEnd() {
        mRefresh.dismiss()
        mAdapter.loadMoreEnd()
    }

    override fun loadError() {
        mRefresh.dismiss()
        mAdapter.loadMoreFail()
    }

    override fun loadComplete() {
        mRefresh.dismiss()
        mAdapter.loadMoreComplete()
    }

    override fun showEmptyView() {
        mEmptyView.show(resources.getString(R.string.null_data), null)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAdapter = getContentAdapter()
        checkObject()
        setupAdapter()
        setupRefresh()
        setupRecyclerView()
    }

    override fun onEnterAnimationComplete() {
        super.onEnterAnimationComplete()
        requestDataFirst()
    }

    private fun setupRecyclerView() {
        with(mRecyclerView) {
            adapter = mAdapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }
    }

    private fun setupRefresh() {
        with(mRefresh) {
            setColorSchemeResources(R.color.colorPrimary)
            onRefresh {
                isRefreshing = true
                requestDataFirst()
            }
        }
    }

    abstract fun requestDataFirst()

    private fun checkObject() {
        checkNotNull(mRefresh) { "please set SwipeRefreshLayout first in xml" }
        checkNotNull(mRecyclerView) { "please set RecyclerView first in xml" }
        checkNotNull(mAdapter) { "please set BaseQuickAdapter first in xml" }
    }

    private fun setupAdapter() {
        with(mAdapter) {
            bindToRecyclerView(mRecyclerView)
            setHeaderAndEmpty(true)
            mEmptyView.setLoadingShowing(true)
            emptyView = mEmptyView
            setOnLoadMoreListener(this@BaseListActivity, mRecyclerView)
        }
    }

    abstract fun getContentAdapter(): A

    override fun onLoadMoreRequested() {
        requestDataMore()
    }

    protected open fun requestDataMore() {}


}
