package com.sjkj.lib_common.base

import android.os.Bundle
import com.blankj.utilcode.util.ToastUtils
import com.qmuiteam.qmui.widget.dialog.QMUITipDialog
import org.jetbrains.anko.act
import javax.inject.Inject

/**
 * @author by dingl on 2018/3/8.
 * @desc BaseMvpActivity
 */
abstract class BaseMvpActivity<P : BasePresenter<*>> : BaseRxActivity(), BaseView {

    @Inject
    lateinit var mPresenter: P

    private val mLoingProgress by lazy { QMUITipDialog.Builder(act).setIconType(QMUITipDialog.Builder.ICON_TYPE_LOADING).create() }

    override fun onCreate(savedInstanceState: Bundle?) {
        injectComponent()
        super.onCreate(savedInstanceState)
    }

    abstract fun injectComponent()

    override fun showLoading() {
        if (mLoingProgress.isShowing.not()) {
            mLoingProgress.show()
        }
    }

    override fun hideLoading() {
        if (mLoingProgress.isShowing) {
            mLoingProgress.dismiss()
        }
    }

    override fun showToast(toast: String) {
        ToastUtils.showShort(toast)
    }

}