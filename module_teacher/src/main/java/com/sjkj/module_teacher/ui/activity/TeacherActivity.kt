package com.sjkj.module_teacher.ui.activity

import android.os.Bundle
import com.sjkj.lib_common.base.BaseRxActivity
import com.sjkj.module_teacher.R
import kotlinx.android.synthetic.main.activity_teacher.*
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 * @author by dingl on 2018/4/2.
 * @desc TeacherActivity
 */
class TeacherActivity : BaseRxActivity() {

    override fun getLayoutId() = R.layout.activity_teacher

    override fun initView(savedInstanceState: Bundle?) {
        topbar.addLeftBackImageButton().onClick { finishAfterTransition() }
        collapsing_topbar_layout.title = getString(R.string.app_name)
    }

}