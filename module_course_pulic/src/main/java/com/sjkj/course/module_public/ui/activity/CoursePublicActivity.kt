package com.sjkj.course.module_public.ui.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import com.alibaba.android.arouter.facade.annotation.Route
import com.qmuiteam.qmui.widget.QMUITabSegment
import com.sjkj.course.module_public.ui.Fragment.CoursePublicSubscribeFragment
import com.sjkj.lib_common.extensions.getStringArray
import com.sjkj.lib_common.utils.FragmentPageAdapterHelper
import com.sjkj.module_course_pulic.R
import kotlinx.android.synthetic.main.activity_course_public.*
import me.yokeyword.fragmentation.SupportActivity
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 * @author by dingl on 2018/3/30.
 * @desc CoursePublicActivity
 */
@Route(path = "/course/public")
class CoursePublicActivity : SupportActivity() {

    private val mTabDatas by lazy { getStringArray(R.array.class_public_tab) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_course_public)
        setupTopBar()
        setupData()
        setupView()
    }

    private fun setupTopBar() {
        topbar.setTitle(R.string.title_activity_course_public)
        topbar.addLeftBackImageButton().onClick { finish() }
    }

    private fun setupData() {
        checkNotNull(mTabDatas) { "the mTabDatas is null" }
    }

    private fun setupView() {
        tabLayout.apply {
            setHasIndicator(true)
            mode = QMUITabSegment.MODE_FIXED
            mTabDatas.forEach {
                addTab(QMUITabSegment.Tab(it))
            }
        }.setupWithViewPager(viewPager.apply {
            offscreenPageLimit = mTabDatas.size
            adapter = object : FragmentPageAdapterHelper(this@CoursePublicActivity, mTabDatas) {
                override fun getFragment(position: Int): Fragment {
                    return when (position) {
                        0 -> CoursePublicSubscribeFragment.newInstance()
                        1 -> CoursePublicSubscribeFragment.newInstance()
                        2 -> CoursePublicSubscribeFragment.newInstance()
                        else -> throw IllegalArgumentException("position at $position is wrong")
                    }
                }
            }
        })
    }

}