package com.sjkj.course.module_public.presenter

import com.sjkj.lib_common.base.BaseRecyclePresenter
import com.sjkj.lib_common.base.BaseRecycleView
import com.sjkj.lib_common.data.CourseClassBean
import javax.inject.Inject

/**
 * @author by dingl on 2018/3/30.
 * @desc CoursePublicPresenter
 */
class CoursePublicPresenter @Inject constructor() : BaseRecyclePresenter<BaseRecycleView<CourseClassBean>>() {

    fun getCoursePublicSubscribeData() {

    }

    fun getCoursePublicSubscribeMoreData() {

    }

}