package com.sjkj.course.module_public.di

import com.sjkj.course.module_public.ui.Fragment.CoursePublicSubscribeFragment
import com.sjkj.lib_common.di.BaseComponent
import com.sjkj.lib_common.di.scope.PerUser
import dagger.Component

/**
 * @author by dingl on 2018/3/8.
 * @desc Component
 */
@PerUser
@Component(dependencies = [(BaseComponent::class)])
interface CoursePublicComponent {

    fun inject(coursePublicSubscribeFragment: CoursePublicSubscribeFragment)

}

