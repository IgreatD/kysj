package com.sjkj.course.module_public.ui.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.sjkj.lib_common.data.CourseClassBean
import com.sjkj.module_course_pulic.R

/**
 * @author by dingl on 2018/3/30.
 * @desc CoursePublicAdapter
 */
class CoursePublicAdapter(private val itemClick: (CourseClassBean) -> Unit) : BaseQuickAdapter<CourseClassBean, BaseViewHolder>(R.layout.adapter_option) {
    override fun convert(helper: BaseViewHolder?, item: CourseClassBean?) {

    }
}