package com.sjkj.module_search.ui.activity

import android.app.SearchManager
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.support.annotation.TransitionRes
import android.text.InputType
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.TextUtils
import android.text.style.StyleSpan
import android.transition.Transition
import android.transition.TransitionInflater
import android.transition.TransitionManager
import android.util.SparseArray
import android.view.View
import android.view.ViewStub
import android.view.inputmethod.EditorInfo
import android.widget.SearchView
import android.widget.TextView
import com.alibaba.android.arouter.facade.annotation.Route
import com.qmuiteam.qmui.util.QMUIKeyboardHelper
import com.sjkj.lib_common.base.BaseRxActivity
import com.sjkj.lib_common.utils.ImeUtils
import com.sjkj.lib_common.widget.ElasticDragDismissFrameLayout
import com.sjkj.module_search.R
import kotlinx.android.synthetic.main.activity_search.*
import org.jetbrains.anko.sdk25.coroutines.onClick


/**
 * @author by dingl on 2018/3/5.
 * @desc SearchActivity
 */
@Route(path = "/search/search")
class SearchActivity : BaseRxActivity() {

    private val transitions = SparseArray<Transition>()

    private var noResults: TextView? = null

    override fun getLayoutId() = R.layout.activity_search

    override fun initView(savedInstanceState: Bundle?) {
        searchback.onClick {
            searchback.background = null
            finishAfterTransition()
        }
        setupSearchView()
        root.addListener(
                object : ElasticDragDismissFrameLayout.SystemChromeFader(this) {
                    override fun onDragDismissed() {
                        if (root.translationY > 0) {
                            window.returnTransition = TransitionInflater.from(this@SearchActivity)
                                    .inflateTransition(R.transition.about_return_downward)
                        }
                        finishAfterTransition()
                    }
                })
        scrim.onClick {
            searchback.background = null
            finishAfterTransition()
        }
    }

    private fun setupSearchView() {
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        with(search_view) {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            queryHint = getString(R.string.search_hint)
            inputType = InputType.TYPE_TEXT_FLAG_CAP_WORDS
            imeOptions = imeOptions or EditorInfo.IME_ACTION_SEARCH or
                    EditorInfo.IME_FLAG_NO_EXTRACT_UI or EditorInfo.IME_FLAG_NO_FULLSCREEN

            val id = resources.getIdentifier("android:id/search_src_text", null, null)

            val textView = findViewById<TextView>(id)

            textView.setTextColor(Color.WHITE)
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    searchFor(query)
                    return true
                }

                override fun onQueryTextChange(query: String): Boolean {
                    if (TextUtils.isEmpty(query)) {
                        clearResults()
                    }
                    return true
                }
            })
        }
    }

    private fun searchFor(query: String) {
        clearResults()
        QMUIKeyboardHelper.hideKeyboard(search_view)
        search_view.clearFocus()
        loading_progress.visibility = View.VISIBLE
    }

    private fun clearResults() {
        TransitionManager.beginDelayedTransition(root, getTransition(R.transition.auto))
        loading_progress.visibility = View.GONE
        setNoResultsVisibility(View.GONE)
    }

    private fun getTransition(@TransitionRes transitionId: Int): Transition? {
        var transition: Transition? = transitions.get(transitionId)
        if (transition == null) {
            transition = TransitionInflater.from(this).inflateTransition(transitionId)
            transitions.put(transitionId, transition)
        }
        return transition
    }

    private fun setNoResultsVisibility(visibility: Int) {
        if (visibility == View.VISIBLE) {
            if (noResults == null) {
                noResults = (findViewById<View>(R.id.stub_no_search_results) as ViewStub).inflate() as TextView
                noResults?.setOnClickListener({
                    search_view.setQuery("", false)
                    search_view.requestFocus()
                    ImeUtils.showIme(search_view)
                })
            }
            val message = String.format(getString(R.string.no_search_results), search_view.query.toString())
            val ssb = SpannableStringBuilder(message)
            ssb.setSpan(StyleSpan(Typeface.ITALIC),
                    message.indexOf('“') + 1,
                    message.length - 1,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            noResults?.text = ssb
        }
        noResults?.visibility = visibility
    }

}