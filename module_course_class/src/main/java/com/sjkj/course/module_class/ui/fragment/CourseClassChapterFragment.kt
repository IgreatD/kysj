package com.sjkj.course.module_class.ui.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.ViewGroup
import android.widget.TextView
import com.google.gson.Gson
import com.sjkj.course.R
import com.sjkj.course.module_class.data.CourseClassChapterBean
import com.sjkj.course.module_class.di.DaggerCourseComponent
import com.sjkj.course.module_class.presenter.CourseClassChapterPresenter
import com.sjkj.course.module_class.presenter.CourseClassChapterView
import com.sjkj.course.module_class.ui.adapter.CourseClassChapterAdapter
import com.sjkj.lib_common.base.BaseListFragment
import com.sjkj.lib_common.data.CourseClassBean
import com.sjkj.lib_common.extensions.injectBaseComponet
import com.sjkj.lib_common.widget.SwitchMultiButton
import kotlinx.android.synthetic.main.fragment_course_class_chapter.*
import org.jetbrains.anko.find

/**
 * @author by dingl on 2018/3/28.
 * @desc CourseClassChapterFragment
 */
class CourseClassChapterFragment
    : BaseListFragment<CourseClassChapterAdapter, CourseClassChapterPresenter>(),
        CourseClassChapterView {

    private var status = "1,2,5"

    private lateinit var courseClassBean: CourseClassBean

    companion object {
        fun newInstance(courseClassBeanJson: String): CourseClassChapterFragment {
            val fragment = CourseClassChapterFragment()
            val bundle = Bundle()
            bundle.putString("courseClassBeanJson", courseClassBeanJson)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_course_class_chapter

    override fun initView(savedInstanceState: Bundle?) {
        setupArguments()
        switch_button.setOnSwitchListener(object : SwitchMultiButton.OnSwitchListener {
            override fun onSwitch(position: Int, tabText: String) {
                status = if (position == 0) {
                    "1,2,5"
                } else
                    "3,4"
                mAdapter.setNewData(null)
                showEmptyLoadingView()
                requestDataFirst()
            }
        })
        setupHeadView()
    }

    @SuppressLint("SetTextI18n")
    private fun setupHeadView() {
        val headView = layoutInflater.inflate(R.layout.head_course_class_chapter, mView.parent as ViewGroup, false)
        mAdapter.addHeaderView(headView)
        with(courseClassBean) {
            headView.find<TextView>(R.id.squareName).text = "班级名称：$Name"
            headView.find<TextView>(R.id.squareTeacher).text = "主讲：$Teacher"
            headView.find<TextView>(R.id.squareCourseInfoName).text = "科目：$CourseInfoName"
            headView.find<TextView>(R.id.squareLocation).text = "地区：$Province-$City-$District"
            headView.find<TextView>(R.id.squareBeginDate).text = "开班时间：${getClassBeginDate()}"
            headView.find<TextView>(R.id.squareStatus).text = "班级状态："
            headView.find<TextView>(R.id.squareKnow).text = "知识点：$Knowledge"
            headView.find<TextView>(R.id.squareEndDate).text = "结束时间：$EndDate"
            headView.find<TextView>(R.id.squareLessonNumber).text = "共 $LessonNumber 课时"
            headView.find<TextView>(R.id.squareLessonPrice).text = "$LessonPrice 鱼丸/课时"
        }
    }

    private fun setupArguments() {
        val courseClassBeanJson = arguments?.getString("courseClassBeanJson")
        if (courseClassBeanJson != null) {
            courseClassBean = Gson().fromJson(courseClassBeanJson, CourseClassBean::class.java)
        } else
            pop()
    }

    override fun injectComponent() {
        DaggerCourseComponent.builder()
                .baseComponent(injectBaseComponet())
                .build()
                .inject(this)
        mPresenter.mView = this
    }

    override fun initRecycleAndRefresh() {
        mRefresh = mView.find(R.id.refresh)
        mRecyclerView = mView.find(R.id.recyclerView)
    }

    override fun requestDataFirst() {
        mPresenter.getCourseClassChapterData(courseClassBean.ClassID, status)
    }

    override fun requestDataMore() {
        mPresenter.getCourseClassChapterMoreData(courseClassBean.ClassID, status)
    }

    override fun getContentAdapter() = CourseClassChapterAdapter()

    override fun setNewData(t: List<CourseClassChapterBean>) {
        mAdapter.setNewData(t)
    }

    override fun setMoreData(t: List<CourseClassChapterBean>) {
        mAdapter.addData(t)
    }
}