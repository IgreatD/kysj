package com.sjkj.course.module_class.domain.api

import com.sjkj.course.module_class.data.CourseClassChapterResult
import com.sjkj.course.module_class.domain.RequestBody.CourseClassChapterRequestBody
import com.sjkj.course.module_class.domain.RequestBody.CourseClassSquareRequetBody
import com.sjkj.lib_common.data.CourseClassRuleResult
import com.sjkj.lib_common.data.CourseClassSquareResult
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * @author by dingl on 2018/3/21.
 * @desc ClassCourseApi
 */
interface ClassCourseApi {

    @POST("MyClassService.svc/com_MyClassList")
    fun getClassCourseData(@Body courseClassSquareRequetBody: CourseClassSquareRequetBody): Observable<CourseClassSquareResult>

    @POST("MyClassService.svc/com_ClassList")
    fun getCourseClassSquareData(@Body courseClassSquareRequetBody: CourseClassSquareRequetBody): Observable<CourseClassSquareResult>

    @POST("CommonService.svc/GetSysRules")
    fun getCourseRuleData(): Observable<CourseClassRuleResult>

    @POST("MyClassService.svc/GetClassLessons")
    fun getCourseClassChapterData(@Body courseClassChapterRequestBody: CourseClassChapterRequestBody)
            : Observable<CourseClassChapterResult>

}
