package com.sjkj.course.module_class.ui.fragment

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListView
import com.sjkj.course.R
import com.sjkj.course.module_class.di.DaggerCourseComponent
import com.sjkj.course.module_class.presenter.CourseClassSquarePresenter
import com.sjkj.course.module_class.ui.adapter.CourseClassHaveInAdapter
import com.sjkj.lib_common.base.BaseListFragment
import com.sjkj.lib_common.base.BaseRecycleView
import com.sjkj.lib_common.common.OptionType
import com.sjkj.lib_common.data.CourseClassBean
import com.sjkj.lib_common.data.SubjectBean
import com.sjkj.lib_common.extensions.injectBaseComponet
import com.sjkj.lib_common.presenter.OptionPresenter
import com.sjkj.lib_common.presenter.OptionView
import kotlinx.android.synthetic.main.fragment_course_class.*
import org.jetbrains.anko.find
import org.jetbrains.anko.support.v4.toast
import javax.inject.Inject

/**
 * @author by dingl on 2018/3/28.
 * @desc CourseClassHaveInFragment
 */
class CourseClassHaveInFragment :
        BaseListFragment<CourseClassHaveInAdapter, CourseClassSquarePresenter>(),
        OptionView,
        BaseRecycleView<CourseClassBean> {

    override fun setNewData(t: List<CourseClassBean>) {
        mAdapter.setNewData(t)
    }

    override fun setMoreData(t: List<CourseClassBean>) {
        mAdapter.addData(t)
    }

    private val subjectRecycleView by lazy {
        val subjectRecycleView = ListView(context)
        subjectRecycleView
    }

    override fun getSubjectDataSuccess(subjectList: List<SubjectBean>) {
        val adapter = ArrayAdapter<String>(context, android.R.layout.simple_list_item_1)
        adapter.addAll(subjectList.map { it.Name })
        subjectRecycleView.adapter = adapter
    }

    override fun getDataFailed() {
    }

    companion object {
        fun newInstance(): CourseClassHaveInFragment {
            val fragment = CourseClassHaveInFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun getContentAdapter() = CourseClassHaveInAdapter {

    }

    override fun getLayoutId() = R.layout.fragment_course_class

    private lateinit var contentView: View

    @Inject
    lateinit var optionPresenter: OptionPresenter

    override fun injectComponent() {
        DaggerCourseComponent.builder()
                .baseComponent(injectBaseComponet())
                .build()
                .inject(this)
        mPresenter.mView = this
        optionPresenter.mView = this
    }

    override fun initView(savedInstanceState: Bundle?) {
        optionContainer.setOptionView(
                arrayOf("科目"),
                arrayOf(subjectRecycleView),
                contentView)
        optionContainer.setOnMenuCheckListener(object : com.sjkj.lib_common.widget.OptionView.OnMenuCheckListener {
            override fun onMenuCheckListener(currentPosition: Int) {
                toast(currentPosition.toString())
            }
        })
    }

    private fun getOptionData() {
        val optionTypeList = ArrayList<Int>()
        optionTypeList.add(OptionType.TYPE_COURSE)
        optionPresenter.getOptionData(optionTypeList)
    }

    override fun initRecycleAndRefresh() {
        contentView = layoutInflater.inflate(R.layout.base_recycler_view, null)
        mRecyclerView = contentView.find(R.id.recyclerView)
        mRefresh = contentView.find(R.id.refresh)
    }

    override fun requestDataFirst() {
        getOptionData()
        mPresenter.getCourseClassHaveInData()
    }

    override fun requestDataMore() {
        mPresenter.getCourseClassHaveInMoreData()
    }

}