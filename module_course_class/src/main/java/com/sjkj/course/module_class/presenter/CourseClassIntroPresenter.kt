package com.sjkj.course.module_class.presenter

import com.sjkj.lib_common.data.CourseClassRuleBean
import com.sjkj.course.module_class.domain.Repetroty.ClassCourseRepertory
import com.sjkj.lib_common.base.BasePresenter
import com.sjkj.lib_common.base.BaseView
import com.sjkj.lib_common.rxhelper.ioMain
import javax.inject.Inject

/**
 * @author by dingl on 2018/3/28.
 * @desc CourseClassIntroPresenter
 */
interface CourseClassIntroView : BaseView {
    fun getCourseClassRuleSuccess(classRuleList: List<CourseClassRuleBean>)
    fun getCourseClassRuleFailed()
}

class CourseClassIntroPresenter @Inject constructor() : BasePresenter<CourseClassIntroView>() {

    @Inject
    lateinit var courseRepertory: ClassCourseRepertory

    fun getCourseClassRuleData() {
        courseRepertory.getCourseClassRuleData()
                .ioMain(lifecycleProvider, mView, {
                    mView.getCourseClassRuleSuccess(it.data.GetSysRulesInfoDto)
                }, {
                    mView.getCourseClassRuleFailed()
                })
    }

}