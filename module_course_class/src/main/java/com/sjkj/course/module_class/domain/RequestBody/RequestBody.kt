package com.sjkj.course.module_class.domain.RequestBody

import com.sjkj.lib_common.common.Constant
import com.sjkj.lib_common.common.UserConstant
import com.sjkj.lib_common.utils.PreferenceUtils

/**
 * @author by dingl on 2018/3/19.
 * @desc RequestBody
 */

private val gradeID by PreferenceUtils(UserConstant.ACCOUNT_GRADEID, 0)

private val stageID by PreferenceUtils(UserConstant.ACCOUNT_STAGEID, 1)

data class CourseClassSquareRequetBody(
        val QueryText: String = "",
        val StageID: Int = stageID,
        val GradeID: Int = gradeID,
        val CourseInfoID: Int,
        val QueryItemID: Int = 0,
        val KnowledgeIDs: String = "",
        val ProvinceID: Int = 0,
        val CityID: Int = 0,
        val DistrictID: Int = 0,
        val PageIndex: Int,
        val PageSize: Int = Constant.PAGE_SIZE
)

data class CourseClassChapterRequestBody(
        val Status: String,
        val ClassID: Int?,
        val PageIndex: Int,
        val PageSize: Int = Constant.PAGE_SIZE
)