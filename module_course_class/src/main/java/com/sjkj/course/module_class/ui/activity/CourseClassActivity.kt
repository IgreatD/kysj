package com.sjkj.course.module_class.ui.activity

import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.app.Fragment
import android.support.v4.util.Pair
import com.alibaba.android.arouter.facade.annotation.Route
import com.alibaba.android.arouter.launcher.ARouter
import com.qmuiteam.qmui.alpha.QMUIAlphaImageButton
import com.qmuiteam.qmui.widget.QMUITabSegment
import com.sjkj.course.R
import com.sjkj.course.module_class.ui.fragment.CourseClassHaveInFragment
import com.sjkj.course.module_class.ui.fragment.CourseClassSquareFragment
import com.sjkj.lib_common.base.BaseRxActivity
import com.sjkj.lib_common.extensions.getStringArray
import com.sjkj.lib_common.extensions.setVisible
import com.sjkj.lib_common.utils.FragmentPageAdapterHelper
import com.sjkj.lib_common.utils.TransitionHelper
import com.sjkj.lib_common.utils.ViewPagerOnPageChangeHelper
import kotlinx.android.synthetic.main.activity_course_class.*
import org.jetbrains.anko.act
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 * @author by dingl on 2018/3/28.
 * @desc CourseClassActivity
 */
@Route(path = "/course/class")
class CourseClassActivity : BaseRxActivity() {

    override fun initView(savedInstanceState: Bundle?) {
        setupTopBar()
        setupData()
        setupView()
    }

    override fun getLayoutId() = R.layout.activity_course_class

    private val mTabDatas by lazy { getStringArray(R.array.class_course_tab) }

    private lateinit var searchButton: QMUIAlphaImageButton

    private fun setupTopBar() {
        topbar.setTitle(R.string.title_activity_course_class)
        topbar.addLeftBackImageButton().onClick { finish() }
        searchButton = topbar.addRightImageButton(R.drawable.ic_search, R.id.id_search).apply {
            setVisible(false)
            setOnClickListener {
                val otherPair = Pair(it, getString(R.string.transition_search_back))
                val pairs = TransitionHelper.instance.createSafeTransitionPair(act, false, otherPair)
                val options = ActivityOptionsCompat.makeSceneTransitionAnimation(act, *pairs)
                ARouter.getInstance()
                        .build("/search/search")
                        .withOptionsCompat(options)
                        .navigation(this@CourseClassActivity)
            }
        }
    }

    private fun setupData() {
        checkNotNull(mTabDatas) { "the mTabDatas is null" }
    }

    private fun setupView() {
        tabLayout.apply {
            setHasIndicator(true)
            mode = QMUITabSegment.MODE_FIXED
            mTabDatas.forEach {
                addTab(QMUITabSegment.Tab(it))
            }
        }.setupWithViewPager(viewPager.apply {
            adapter = object : FragmentPageAdapterHelper(this@CourseClassActivity, mTabDatas) {
                override fun getFragment(position: Int): Fragment {
                    return when (position) {
                        0 -> CourseClassHaveInFragment.newInstance()
                        1 -> CourseClassSquareFragment.newInstance()
                        else -> throw IllegalArgumentException("position at $position is wrong")
                    }
                }
            }
            addOnPageChangeListener(object : ViewPagerOnPageChangeHelper() {
                override fun onPageSelected(position: Int) {
                    when (position) {
                        0 -> searchButton.setVisible(false)
                        1 -> searchButton.setVisible(true)
                    }
                }
            })
        })
    }

}