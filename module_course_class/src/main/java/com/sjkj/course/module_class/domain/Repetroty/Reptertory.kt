package com.sjkj.course.module_class.domain.Repetroty

import com.sjkj.course.module_class.domain.RequestBody.CourseClassChapterRequestBody
import com.sjkj.course.module_class.domain.RequestBody.CourseClassSquareRequetBody
import com.sjkj.course.module_class.domain.api.ClassCourseApi
import com.sjkj.lib_common.http.createService
import com.sjkj.lib_common.http.getCacheProviders
import io.rx_cache2.DynamicKey
import io.rx_cache2.EvictDynamicKey
import javax.inject.Inject

/**
 * @author by dingl on 2018/3/16.
 * @desc Reptertory
 *
 */

class ClassCourseRepertory @Inject constructor() {

    fun getCourseClassRuleData() =
            getCacheProviders().getDataWithCache(
                    createService(ClassCourseApi::class.java).getCourseRuleData(), DynamicKey("class_rule"))

    fun getClassCourseHaveInData(courseInfoID: Int, pageInex: Int) =
            getCacheProviders().getDataWithCache(
                    createService(ClassCourseApi::class.java).getClassCourseData(
                            CourseClassSquareRequetBody(CourseInfoID = courseInfoID, PageIndex = pageInex)
                    ), DynamicKey("havein_$pageInex"))

    fun getCourseClassSquareData(courseInfoID: Int, pageInex: Int) =
            getCacheProviders().getDataWithCache(
                    createService(ClassCourseApi::class.java).getCourseClassSquareData(
                            CourseClassSquareRequetBody(CourseInfoID = courseInfoID, PageIndex = pageInex)
                    ), DynamicKey("square_$pageInex"), EvictDynamicKey(true))

    fun getCourseClassChapterData(classId: Int?, status: String, pageInex: Int) =
            getCacheProviders().getDataWithCache(
                    createService(ClassCourseApi::class.java).getCourseClassChapterData(
                            CourseClassChapterRequestBody(status, classId, pageInex)
                    ), DynamicKey("course_class_chapter_$classId$status$pageInex"))
}

