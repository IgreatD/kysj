package com.sjkj.course.module_class.ui.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.sjkj.course.R
import com.sjkj.lib_common.data.CourseClassBean
import com.sjkj.lib_common.utils.GlideApp
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 * @author by dingl on 2018/3/28.
 * @desc CourseClassHaveInAdapter
 */
class CourseClassHaveInAdapter(private val itemClick: (CourseClassBean) -> Unit) : BaseQuickAdapter<CourseClassBean, BaseViewHolder>(R.layout.adapter_course_class_have_in) {

    override fun convert(helper: BaseViewHolder, item: CourseClassBean?) {
        item ?: return
        with(item) {
            if (Knowledge.isEmpty()) {
                Knowledge = "暂无知识点"
            }
            GlideApp.with(mContext)
                    .load(getSquareImg())
                    .placeholder(R.drawable.teadet_introduction_defaultimg1)
                    .error(R.drawable.teadet_introduction_defaultimg1)
                    .into(helper.getView(R.id.squareImg))
            helper.setText(R.id.squareName, Name)
                    .setText(R.id.squareKnow, "知识点：$Knowledge")
                    .setText(R.id.squareCourseInfoName, CourseInfoName)
                    .setText(R.id.squareTeacher, Teacher)
                    .setText(R.id.squareLocation, "$Province-$City-$District")
                    .setText(R.id.squareSignInNumber, "${SignInNumber}人报名")
                    .setText(R.id.squareBeginDate, "开班时间：${getClassBeginDate()}")
                    .setText(R.id.squareLessonNumber, "共${LessonNumber}个课时")
                    .setText(R.id.squareGoodEvaluateCount, "好评数：$GoodEvaluateCount")
                    .setText(R.id.squareLessonPrice, "${LessonPrice}鱼丸/课时")
                    .itemView.onClick { itemClick(this@with) }
        }
    }
}