package com.sjkj.course.module_class.ui.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.sjkj.course.R
import com.sjkj.course.module_class.data.CourseClassChapterBean

/**
 * @author by dingl on 2018/4/2.
 * @desc CourseClassChapterAdapter
 */
class CourseClassChapterAdapter : BaseQuickAdapter<CourseClassChapterBean, BaseViewHolder>(R.layout.adapter_course_class_chapter) {

    override fun convert(helper: BaseViewHolder, item: CourseClassChapterBean?) {
        item ?: return
        with(item) {
            if (LessonDescription == null) {
                LessonDescription = "暂无课时描述"
            }
            helper.setText(R.id.chapterTitle, LessonTitle)
                    .setText(R.id.chapterSource, "预习素材数：$PreResourceCount")
                    .setText(R.id.chapterCheck, "预习检测数量：$PreQuestionCount")
                    .setText(R.id.chapterStar, " $EvaluateCount 人评价")
                    .setText(R.id.chapterDes, "课时描述：$LessonDescription")
                    .setText(R.id.chapterClassTime, "第 $Serial 课时")
                    .setText(R.id.chapterNum, "上课人数：$ListenNums")
                    .setText(R.id.chapterTeacher, if (IsTeacherReady == 1) "老师已签到" else "老师未签到")
                    .setText(R.id.chapterTime,
                            when (Status) {
                                1 -> "未上课"
                                2 ->
                                    when (IsTeacherReady) {
                                        0 -> "(已上课未完成)"
                                        1 -> "(正在上课)"
                                        else -> "(未知)"
                                    }
                                3 -> "(已上课)"
                                4 -> "(已上课)"
                                5 -> "(超时未上课)"
                                else -> "(未知)"
                            })
        }
    }
}