package com.sjkj.course.module_class.ui.fragment

import android.os.Bundle
import com.sjkj.course.R
import com.sjkj.lib_common.base.BaseRxFragment

/**
 * @author by dingl on 2018/3/28.
 * @desc CourseClassChapterFragment
 */
class CourseClassStarFragment : BaseRxFragment() {

    companion object {
        fun newInstance(): CourseClassStarFragment {
            val fragment = CourseClassStarFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun getLayoutId()= R.layout.fragment_course_class_star

    override fun initView(savedInstanceState: Bundle?) {
    }
}