package com.sjkj.course.module_class.di

import com.sjkj.course.module_class.ui.fragment.CourseClassChapterFragment
import com.sjkj.course.module_class.ui.fragment.CourseClassHaveInFragment
import com.sjkj.course.module_class.ui.fragment.CourseClassIntroFragment
import com.sjkj.course.module_class.ui.fragment.CourseClassSquareFragment
import com.sjkj.lib_common.di.BaseComponent
import com.sjkj.lib_common.di.scope.PerUser
import dagger.Component

/**
 * @author by dingl on 2018/3/8.
 * @desc Component
 */
@PerUser
@Component(dependencies = [(BaseComponent::class)])
interface CourseComponent {

    fun inject(courseClassHaveInFragment: CourseClassHaveInFragment)
    fun inject(courseClassSquareFragment: CourseClassSquareFragment)
    fun inject(courseClassIntroFragment: CourseClassIntroFragment)
    fun inject(courseClassChapterFragment: CourseClassChapterFragment)

}

