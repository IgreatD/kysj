package com.sjkj.course.module_class.ui.fragment

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import com.sjkj.course.R
import com.sjkj.course.module_class.di.DaggerCourseComponent
import com.sjkj.course.module_class.presenter.CourseClassIntroPresenter
import com.sjkj.course.module_class.presenter.CourseClassIntroView
import com.sjkj.course.module_class.ui.activity.CourseClassDetailActivity.Companion.COVERPATH
import com.sjkj.course.module_class.ui.activity.CourseClassDetailActivity.Companion.DESCRIBE
import com.sjkj.lib_common.base.BaseMvpFragment
import com.sjkj.lib_common.data.CourseClassRuleBean
import com.sjkj.lib_common.extensions.injectBaseComponet
import com.sjkj.lib_common.extensions.replace
import kotlinx.android.synthetic.main.fragment_course_class_intro.*

/**
 * @author by dingl on 2018/3/28.
 * @desc CourseClassChapterFragment
 */
class CourseClassIntroFragment : BaseMvpFragment<CourseClassIntroPresenter>(), CourseClassIntroView {

    override fun getContainerView(): View =
            layoutInflater.inflate(R.layout.fragment_course_class_intro, view?.parent as ViewGroup, false)

    override fun requestDataFirst() {
        mPresenter.getCourseClassRuleData()
    }

    companion object {
        fun newInstance(classID: Int, describe: String, coverPath: String): CourseClassIntroFragment {
            val fragment = CourseClassIntroFragment()
            val bundle = Bundle()
            bundle.putInt("", classID)
            bundle.putString(DESCRIBE, describe)
            bundle.putString(COVERPATH, coverPath)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        var describe = arguments?.getString(DESCRIBE)
        if (describe.isNullOrEmpty()) {
            describe = "没有班级描述"
        }
        introDescribe.text = describe
    }

    override fun injectComponent() {
        DaggerCourseComponent.builder()
                .baseComponent(injectBaseComponet())
                .build()
                .inject(this)
        mPresenter.mView = this
    }

    override fun getCourseClassRuleSuccess(classRuleList: List<CourseClassRuleBean>) {
        classRuleList[0].takeIf {
            introTransferRule.text = it.TransferRule.replace()
            introQuitRule.text = it.QuitRule.replace()
            introTeachingRule.text = it.TeachingRule.replace()
            false
        }
    }

    override fun getCourseClassRuleFailed() {

    }
}