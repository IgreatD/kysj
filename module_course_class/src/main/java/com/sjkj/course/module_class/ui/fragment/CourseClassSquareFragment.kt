package com.sjkj.course.module_class.ui.fragment

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListView
import com.alibaba.android.arouter.launcher.ARouter
import com.sjkj.course.R
import com.sjkj.course.module_class.di.DaggerCourseComponent
import com.sjkj.course.module_class.presenter.CourseClassSquarePresenter
import com.sjkj.course.module_class.ui.activity.CourseClassDetailActivity
import com.sjkj.course.module_class.ui.adapter.CourseClassHaveInAdapter
import com.sjkj.lib_common.base.BaseListFragment
import com.sjkj.lib_common.base.BaseRecycleView
import com.sjkj.lib_common.common.OptionType
import com.sjkj.lib_common.data.CitysBean
import com.sjkj.lib_common.data.CourseClassBean
import com.sjkj.lib_common.data.QueryBean
import com.sjkj.lib_common.data.SubjectBean
import com.sjkj.lib_common.extensions.injectBaseComponet
import com.sjkj.lib_common.presenter.OptionPresenter
import com.sjkj.lib_common.presenter.OptionView
import kotlinx.android.synthetic.main.fragment_course_class.*
import org.jetbrains.anko.find
import org.jetbrains.anko.support.v4.toast
import javax.inject.Inject

/**
 * @author by dingl on 2018/3/28.
 * @desc CourseClassHaveInFragment
 */
class CourseClassSquareFragment :
        BaseListFragment<CourseClassHaveInAdapter, CourseClassSquarePresenter>(),
        OptionView,
        BaseRecycleView<CourseClassBean> {

    override fun setNewData(t: List<CourseClassBean>) {
        mAdapter.setNewData(t)
    }

    override fun setMoreData(t: List<CourseClassBean>) {
        mAdapter.addData(t)
    }

    private val cityRecyceView by lazy {
        val cityRecyceView = ListView(context)
        cityRecyceView
    }

    private val subjectRecycleView by lazy {
        val subjectRecycleView = ListView(context)
        subjectRecycleView
    }

    private val knowRecycleView by lazy {
        val knowRecycleView = ListView(context)
        knowRecycleView
    }

    private val queryRecycleView by lazy {
        val queryRecycleView = ListView(context)
        queryRecycleView
    }

    override fun getSubjectDataSuccess(subjectList: List<SubjectBean>) {
        val adapter = ArrayAdapter<String>(context, android.R.layout.simple_list_item_1)
        adapter.addAll(subjectList.map { it.Name })
        subjectRecycleView.adapter = adapter
    }

    override fun getDataFailed() {
    }

    override fun getQueryDataSuccess(queryList: List<QueryBean>) {
        val adapter = ArrayAdapter<String>(context, android.R.layout.simple_list_item_1)
        adapter.addAll(queryList.map { it.QueryItemName })
        queryRecycleView.adapter = adapter
    }

    override fun getProvinceDataSuccess(citysBean: CitysBean) {
        val adapter = ArrayAdapter<String>(context, android.R.layout.simple_list_item_1)
        adapter.addAll(citysBean.Province.map { it.CityName })
        cityRecyceView.adapter = adapter
    }

    companion object {
        fun newInstance(): CourseClassSquareFragment {
            val fragment = CourseClassSquareFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun getContentAdapter() = CourseClassHaveInAdapter {
        ARouter.getInstance().build("/course/class/detail")
                .withObject(CourseClassDetailActivity.CLASS_COURSE, it)
                .navigation()
    }

    override fun getLayoutId() = R.layout.fragment_course_class

    private lateinit var contentView: View

    @Inject
    lateinit var optionPresenter: OptionPresenter

    override fun injectComponent() {
        DaggerCourseComponent.builder()
                .baseComponent(injectBaseComponet())
                .build()
                .inject(this)
        mPresenter.mView = this
        optionPresenter.mView = this
    }

    override fun initView(savedInstanceState: Bundle?) {
        getOptionData()
        optionContainer.setOptionView(
                arrayOf("科目", "智能选项", "知识点", "地区"),
                arrayOf(subjectRecycleView, queryRecycleView, knowRecycleView, cityRecyceView),
                contentView)
        optionContainer.setOnMenuCheckListener(object : com.sjkj.lib_common.widget.OptionView.OnMenuCheckListener {
            override fun onMenuCheckListener(currentPosition: Int) {
                toast(currentPosition.toString())
            }
        })
    }

    private fun getOptionData() {
        val optionTypeList = ArrayList<Int>()
        optionTypeList.add(OptionType.TYPE_COURSE)
        optionTypeList.add(OptionType.TYPE_SORT)
        optionTypeList.add(OptionType.TYPE_LOCATION)
        optionPresenter.getOptionData(optionTypeList)
    }

    override fun initRecycleAndRefresh() {
        contentView = layoutInflater.inflate(R.layout.base_recycler_view, null)
        mRecyclerView = contentView.find(R.id.recyclerView)
        mRefresh = contentView.find(R.id.refresh)
    }

    override fun requestDataFirst() {
        mPresenter.getCourseClassSquareData()
    }

    override fun requestDataMore() {
        mPresenter.getCourseClassSquareMoreData()
    }

}