package com.sjkj.course.module_class.presenter

import com.sjkj.course.module_class.domain.Repetroty.ClassCourseRepertory
import com.sjkj.lib_common.base.BaseRecyclePresenter
import com.sjkj.lib_common.base.BaseRecycleView
import com.sjkj.lib_common.data.CourseClassBean
import com.sjkj.lib_common.rxhelper.ioMain
import javax.inject.Inject

/**
 * @author by dingl on 2018/3/28.
 * @desc CourseClassSquarePresenter
 */
class CourseClassSquarePresenter @Inject constructor() : BaseRecyclePresenter<BaseRecycleView<CourseClassBean>>() {

    @Inject
    lateinit var classCourseRepertory: ClassCourseRepertory

    fun getCourseClassHaveInData() {
        pageIndex = 1
        classCourseRepertory.getClassCourseHaveInData(0, pageIndex)
                .ioMain(lifecycleProvider, mView, {
                    setSize(it.data.ClassListInfoDto.size)
                    mView.setNewData(it.data.ClassListInfoDto)
                })
    }

    fun getCourseClassHaveInMoreData() {
        classCourseRepertory.getClassCourseHaveInData(0, pageIndex)
                .ioMain(lifecycleProvider, mView, {
                    setSize(it.data.ClassListInfoDto.size)
                    mView.setMoreData(it.data.ClassListInfoDto)
                })
    }

    fun getCourseClassSquareData() {
        pageIndex = 0
        classCourseRepertory.getCourseClassSquareData(0, pageIndex)
                .ioMain(lifecycleProvider, mView, {
                    setSize(it.data.ClassListInfoDto.size)
                    mView.setNewData(it.data.ClassListInfoDto)
                })
    }

    fun getCourseClassSquareMoreData() {
        classCourseRepertory.getCourseClassSquareData(0, pageIndex)
                .ioMain(lifecycleProvider, mView, {
                    setSize(it.data.ClassListInfoDto.size)
                    mView.setMoreData(it.data.ClassListInfoDto)
                })
    }

}