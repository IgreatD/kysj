package com.sjkj.course.module_class.data

import com.sjkj.lib_common.base.BaseJsonResult

/**
 * @author by dingl on 2018/4/2.
 * @desc CourseClassChapterResult
 */
data class CourseClassChapterResult(val ClassLessonInfoDto: List<CourseClassChapterBean>) : BaseJsonResult()

data class CourseClassChapterBean(
        val IsStudentReady:Int,
        val LessonTitle: String,
        val Status: Int,
        val IsTeacherReady: Int,
        val PreResourceCount: Int,
        val PreQuestionCount: Int,
        val TurtorVoiceDuration: String,
        val BeginDate: String,
        val EndDate: String,
        val Duration: String,
        val EvaluateCount: Int,
        val Serial: Int,
        var LessonDescription: String?,
        val ListenNums: Int
)