package com.sjkj.course.module_class.presenter

import com.sjkj.course.module_class.data.CourseClassChapterBean
import com.sjkj.course.module_class.domain.Repetroty.ClassCourseRepertory
import com.sjkj.lib_common.base.BaseRecyclePresenter
import com.sjkj.lib_common.base.BaseRecycleView
import com.sjkj.lib_common.rxhelper.ioMain
import javax.inject.Inject

/**
 * @author by dingl on 2018/4/2.
 * @desc CourseClassChapterPresenter
 */
interface CourseClassChapterView : BaseRecycleView<CourseClassChapterBean>

class CourseClassChapterPresenter @Inject constructor() : BaseRecyclePresenter<CourseClassChapterView>() {

    @Inject
    lateinit var classCourseRepertory: ClassCourseRepertory

    fun getCourseClassChapterData(classId: Int?, status: String) {
        pageIndex = 0
        classCourseRepertory.getCourseClassChapterData(classId, status, pageIndex)
                .ioMain(lifecycleProvider, mView, {
                    setSize(it.data.ClassLessonInfoDto.size)
                    mView.setNewData(it.data.ClassLessonInfoDto)
                })
    }

    fun getCourseClassChapterMoreData(classId: Int?, status: String) {
        classCourseRepertory.getCourseClassChapterData(classId, status, pageIndex)
                .ioMain(lifecycleProvider, mView, {
                    setSize(it.data.ClassLessonInfoDto.size)
                    mView.setMoreData(it.data.ClassLessonInfoDto)
                })
    }


}