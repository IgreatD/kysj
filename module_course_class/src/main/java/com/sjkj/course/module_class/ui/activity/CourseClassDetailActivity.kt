package com.sjkj.course.module_class.ui.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import com.alibaba.android.arouter.facade.annotation.Route
import com.qmuiteam.qmui.widget.QMUITabSegment
import com.sjkj.course.R
import com.sjkj.course.module_class.ui.fragment.CourseClassChapterFragment
import com.sjkj.course.module_class.ui.fragment.CourseClassIntroFragment
import com.sjkj.course.module_class.ui.fragment.CourseClassStarFragment
import com.sjkj.lib_common.base.BaseRxActivity
import com.sjkj.lib_common.extensions.getStringArray
import com.sjkj.lib_common.utils.FragmentPageAdapterHelper
import kotlinx.android.synthetic.main.activity_course_class.*
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 * @author by dingl on 2018/3/28.
 * @desc CourseClassActivity
 */
@Route(path = "/course/class/detail")
class CourseClassDetailActivity : BaseRxActivity() {

    override fun getLayoutId() = R.layout.activity_course_class

    override fun initView(savedInstanceState: Bundle?) {
        setupExtras()
        setupTopBar()
        setupData()
        setupView()
    }

    companion object {
        const val CLASS_COURSE = "class_course"
        const val DESCRIBE = "describe"
        const val COVERPATH = "coverpath"
    }

    private val mTabDatas by lazy { getStringArray(R.array.class_course_detail_tab) }

    private lateinit var courseClassBeanJson: String

    private fun setupExtras() {
        try {
            courseClassBeanJson = intent.extras.getSerializable(CLASS_COURSE) as String
        } catch (e: Exception) {
            e.printStackTrace()
            finishAfterTransition()
        }
    }

    private fun setupTopBar() {
        topbar.setTitle(R.string.title_activity_course_class_detail)
        topbar.addLeftBackImageButton().onClick { finish() }
    }

    private fun setupData() {
        checkNotNull(mTabDatas) { "the mTabDatas is null" }
    }

    private fun setupView() {
        tabLayout.apply {
            setHasIndicator(true)
            mode = QMUITabSegment.MODE_FIXED
            mTabDatas.forEach {
                addTab(QMUITabSegment.Tab(it))
            }
        }.setupWithViewPager(viewPager.apply {
            offscreenPageLimit = mTabDatas.size
            adapter = object : FragmentPageAdapterHelper(this@CourseClassDetailActivity, mTabDatas) {
                override fun getFragment(position: Int): Fragment {
                    return when (position) {
                        0 -> CourseClassChapterFragment.newInstance(courseClassBeanJson)
                        1 -> CourseClassIntroFragment.newInstance(
                                1,
                                "",
                                "")
                        2 -> CourseClassStarFragment.newInstance()
                        else -> throw IllegalArgumentException("position at $position is wrong")
                    }
                }
            }
        })
    }

}