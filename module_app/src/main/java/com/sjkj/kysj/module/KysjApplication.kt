package com.sjkj.kysj.module

import com.sjkj.lib_common.base.BaseApplication

/**
 * @author by dingl on 2018/3/28.
 * @desc KysjApplication
 */
class KysjApplication : BaseApplication()